#pragma once

#include "Expression.hpp"

class SymbolTable
{
    int current_offset;
    
public:
    vector<string> internal_string_table;

    vector<TypeMap> internal_type_table_stack;
    vector<VarMap> internal_variable_table_stack;

    SymbolTable();

    VarMap Create_Predefined_Variables();
    TypeMap Create_Predefined_Types();

    int New_Offset(Type); // For assignments
    string Get_Variable_Offest(string identifier);

    void Enter_Scope();
    void Exit_Scope();
    
    string Add_String(string);

    Expression* Get_Variable(string);
    void Add_Variable(Expression *, string);

    Type* Get_Type(string);
    void Add_SimpleType(Type*, string);

    void Add_Const_Variable(Expression*, string);

    void Add_Variables_IdentList(vector<string> * list, Type* type);

    void Assign_Variable(Expression*,Expression*);

    void Dump_Strings();
    
    Type * Make_Array_Type(Expression * first, Expression * last, Type * base_type);
};

class RegisterPool
{
    stack<string> internal_pool; // s0 - s7, t0 - t9
public:
    RegisterPool();

    string request();
    void free(Expression *);
    void free(string);
};

extern RegisterPool register_pool;
extern SymbolTable symbol_table;