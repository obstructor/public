%{
#include <iostream>
#include <fstream>
#include "Compiler.hpp"

extern int yylex();
extern int linecount;
void yyerror(const char*);
%}

%union
{
  char * str_val;
  int int_val;
  char char_val;
  Expression * expr;
  Type * type;
  vector<string> * ident_list;
}

%error-verbose
%token ARRAYSY 
%token ASSIGNSY 
%token BEGINSY 
%token CHARCONSTSY 
%token CHRSY 
%token COLONSY 
%token COMMASY 
%token CONSTSY 
%token DIVSY 
%token DOSY 
%token DOTSY 
%token DOWNTOSY 
%token ELSEIFSY 
%token ELSESY 
%token ENDSY 
%token EQSY 
%token FORSY 
%token FORWARDSY 
%token FUNCTIONSY 
%token GTESY 
%token GTSY 
%token IDENTSY
%token IFSY 
%token INTSY 
%token LBRACKETSY 
%token LPARENSY 
%token LTESY 
%token LTSY 
%token MINUSSY 
%token MODSY 
%token MULTSY
%token NOTSY 
%token OFSY
%token ORDSY 
%token PLUSSY 
%token PREDSY 
%token PROCEDURESY 
%token RBRACKETSY
%token READSY 
%token RECORDSY 
%token REFSY 
%token REPEATSY 
%token RETURNSY 
%token RPARENSY 
%token SCOLONSY 
%token STOPSY 
%token STRINGSY 
%token SUCCSY
%token THENSY 
%token TOSY 
%token TYPESY 
%token UNTILSY 
%token VARSY 
%token WHILESY 
%token WRITESY

%left ANDSY ORSY
%right NOTSY
%nonassoc EQSY LTESY GTESY GTSY LTSY NEQSY 
%left PLUSSY MINUSSY 
%left DIVSY MODSY MULTSY
%right UMINUSSY 

%type <expr>Expression

%type <char_val> CHARCONSTSY

%type <int_val> Arguments 

%type <int_val> Assignment
%type <int_val> Block 
%type <int_val> Body  
%type <int_val> ElseClause 
%type <int_val> ElseIfHead 
%type <int_val> ElseIfList 

%type <int_val> FSignature 

%type <type> FieldDecl 
%type <type> FieldDecls

%type <int_val> ForHead 
%type <int_val> ForStatement 
%type <int_val> FormalParameter
%type <int_val> FormalParameters  
%type <int_val> FunctionCall 
%type <int_val> INTSY 

%type <ident_list> IdentList 

%type <int_val> OptVar 
%type <int_val> IfHead 
%type <int_val> IfStatement 

%type <expr> LValue 

%type <int_val> OptArguments 
%type <int_val> OptFormalParameters  
%type <int_val> PSignature 
%type <int_val> ProcedureCall
%type <int_val> ReadArgs
%type <int_val> ReadStatement 
%type <int_val> RepeatStatement 
%type <int_val> ReturnStatement

%type <type> SimpleType 

%type <int_val> Statement 
%type <int_val> StatementList 
%type <int_val> StopStatement 
%type <int_val> ThenPart 
%type <int_val> ToHead 

%type <type> Type 
%type <type> ArrayType 
%type <type> RecordType

%type <int_val> WhileHead 
%type <int_val> WhileStatement 
%type <int_val> WriteArgs 
%type <int_val> WriteStatement  

%type <str_val> IDENTSY 
%type <str_val> STRINGSY 

%%
Program : ProgramHead Block DOTSY {Footer_Code();}
	;

ProgramHead : OptConstDecls OptTypeDecls OptVarDecls PFDecls {cout << "read_main:\n";/*Maybe needs its own function?*/}
            ;

OptConstDecls : CONSTSY ConstDecls
                |
                ;

ConstDecls : ConstDecls ConstDecl
                | ConstDecl
                ;

ConstDecl : IDENTSY EQSY Expression SCOLONSY {symbol_table.Add_Const_Variable($3,$1);}
	;

PFDecls : PFDecls ProcedureDecl
        | PFDecls FunctionDecl
        |
        ;

ProcedureDecl : PSignature SCOLONSY FORWARDSY SCOLONSY {}
              | PSignature SCOLONSY Body SCOLONSY {}
	        ;

PSignature : PROCEDURESY IDENTSY LPARENSY OptFormalParameters RPARENSY {}
           ;

FunctionDecl : FSignature SCOLONSY FORWARDSY SCOLONSY {}
                | FSignature SCOLONSY Body SCOLONSY {}
                ;

FSignature : FUNCTIONSY IDENTSY LPARENSY OptFormalParameters RPARENSY COLONSY Type {}
           ;

OptFormalParameters : FormalParameters {}
                    | {}
                    ;

FormalParameters : FormalParameters SCOLONSY FormalParameter {}
                 | FormalParameter {}
                 ;

FormalParameter : OptVar IdentList COLONSY Type {}
                ;

OptVar : VARSY {}
       | REFSY {}
       | {}
       ;


Body : OptConstDecls OptTypeDecls OptVarDecls Block {/*Return code? only exists with function calls*/}
     ;

Block : BEGINSY StatementList ENDSY {symbol_table.Exit_Scope();}
      ;

StatementList : StatementList SCOLONSY Statement {}
              | Statement {}
              ;

OptTypeDecls : TYPESY TypeDecls
             |
             ;

TypeDecls    : TypeDecls TypeDecl
             | TypeDecl
             ;

TypeDecl : IDENTSY EQSY Type SCOLONSY {symbol_table.Add_SimpleType($3,$1);}
         ;

Type : SimpleType {$$ = $1;}
     | RecordType {$$ = $1;}
     | ArrayType {$$ = $1;}
     ;

SimpleType : IDENTSY {$$ = symbol_table.Get_Type($1);}
           ;

RecordType : RECORDSY FieldDecls ENDSY {$$ = FieldDecl_to_Record($2);}
           ;

FieldDecls : FieldDecls FieldDecl {$$ = Combine_FieldDecl($1,$2);}
           | {$$ = new Type();}
           ;

FieldDecl : IdentList COLONSY Type SCOLONSY {$$ = Make_FieldDecl($1,$3);}
          ;

IdentList : IdentList COMMASY IDENTSY {$$ = Add_Identifier_tolist($3,$1);}
          | IDENTSY {$$ = Make_Identlist($1);}
          ;

ArrayType : ARRAYSY LBRACKETSY Expression COLONSY Expression RBRACKETSY OFSY Type {$$ = symbol_table.Make_Array_Type($3,$5,$8);}
          ;

OptVarDecls : VARSY VarDecls
            |
            ;

VarDecls    : VarDecls VarDecl
            | VarDecl
            ;

VarDecl : IdentList COLONSY Type SCOLONSY {symbol_table.Add_Variables_IdentList($1,$3);}
        ;

Statement : Assignment {/*TODO*/}
          | IfStatement {}
          | WhileStatement {}
          | RepeatStatement {}
          | ForStatement {}
          | StopStatement {/*TODO*/}
          | ReturnStatement {}
          | ReadStatement {/*TODO*/}
          | WriteStatement {/*TODO*/}
          | ProcedureCall {}
          | {}
          ;

Assignment : LValue ASSIGNSY Expression {symbol_table.Assign_Variable($1,$3);}
           ;

IfStatement : IfHead ThenPart ElseIfList ElseClause ENDSY {}
            ;

IfHead : IFSY Expression {}
       ;

ThenPart : THENSY StatementList {}
         ;

ElseIfList : ElseIfList ElseIfHead ThenPart {}
           |{}
           ;

ElseIfHead : ELSEIFSY Expression {}
           ;

ElseClause : ELSESY StatementList {}
           | {}
           ;

WhileStatement : WhileHead DOSY StatementList ENDSY {}
               ;

WhileHead : WHILESY Expression {}
          ;

RepeatStatement : REPEATSY StatementList UNTILSY Expression {}

ForStatement : ForHead ToHead DOSY StatementList ENDSY{}
             ;

ForHead : FORSY IDENTSY ASSIGNSY Expression {}
        ;

ToHead : TOSY Expression {}
       | DOWNTOSY Expression {}
       ;

StopStatement : STOPSY {Stop_Program();}
              ;

ReturnStatement : RETURNSY Expression {}
                | RETURNSY {}
                ;


ReadStatement : READSY LPARENSY ReadArgs RPARENSY {/*TODO*/}
              ;

ReadArgs : ReadArgs COMMASY LValue {Read_Lvalue($3);}
         | LValue                  {Read_Lvalue($1);}
         ;

WriteStatement : WRITESY LPARENSY WriteArgs RPARENSY {/*TODO*/}
               ;

WriteArgs : WriteArgs COMMASY Expression {Write_Lvalue($3);}
          | Expression                   {Write_Lvalue($1);}
          ;

ProcedureCall : IDENTSY LPARENSY OptArguments RPARENSY {}
              ;
OptArguments : Arguments {}
             |           {}
             ;
Arguments : Arguments COMMASY Expression {}
          | Expression                   {}
          ;

Expression : CHARCONSTSY                         {$$ = Make_Expression($1);}
           | CHRSY LPARENSY Expression RPARENSY  {$$ = unary_math(UNOP_CHR, $3);}
           | Expression ANDSY Expression         {$$ = binary_math(OP_AND,$1,$3);}
           | Expression DIVSY Expression         {$$ = binary_math(OP_DIV,$1,$3);}
           | Expression EQSY Expression          {$$ = binary_math(OP_EQ,$1,$3);}
           | Expression GTESY Expression         {$$ = binary_math(OP_GTE,$1,$3);}
           | Expression GTSY Expression          {$$ = binary_math(OP_GT,$1,$3);}
           | Expression LTESY Expression         {$$ = binary_math(OP_LTE,$1,$3);}
           | Expression LTSY Expression          {$$ = binary_math(OP_LT,$1,$3);}
           | Expression MINUSSY Expression       {$$ = binary_math(OP_SUB,$1,$3);}
           | Expression MODSY Expression         {$$ = binary_math(OP_MOD,$1,$3);}
           | Expression MULTSY Expression        {$$ = binary_math(OP_MUL,$1,$3);}
           | Expression NEQSY Expression         {$$ = binary_math(OP_NEQ,$1,$3);}
           | Expression ORSY Expression          {$$ = binary_math(OP_OR,$1,$3);}
           | Expression PLUSSY Expression        {$$ = binary_math(OP_ADD,$1,$3);}
           | FunctionCall                        {}
           | INTSY                               {$$ = Make_Expression($1);}
           | LPARENSY Expression RPARENSY        {$$ = $2;}
           | LValue                              {$$ = $1;}
           | MINUSSY Expression %prec UMINUSSY   {$$ = unary_math(UNOP_MINUS,$2);}
           | NOTSY Expression                    {$$ = unary_math(UNOP_NEGATE,$2);}
           | ORDSY LPARENSY Expression RPARENSY  {$$ = unary_math(UNOP_ORD, $3);}
           | PREDSY LPARENSY Expression RPARENSY {$$ = unary_math(UNOP_PRED, $3);}
           | STRINGSY                            {$$ = Make_Expression($1);}
           | SUCCSY LPARENSY Expression RPARENSY {$$ = unary_math(UNOP_SUCC, $3);}
           ;

FunctionCall : IDENTSY LPARENSY OptArguments RPARENSY {}
             ;

LValue : LValue DOTSY IDENTSY {$$ = Lvalue_Dot_Lvalue($1,string($3));}
       | LValue LBRACKETSY Expression RBRACKETSY {$$ = Lvalue_From_Brackets($1,$3);}
       | IDENTSY {$$ = symbol_table.Get_Variable(string($1));}
       ;

%%

void yyerror(const char* msg)
{
  std::cerr << linecount << " " << msg;
}
