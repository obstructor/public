#pragma once

#include <stack>
#include <map>
#include <vector>
#include <stack>
#include <iostream>

using std::vector; using std::map; using std::stack; using std::cout; using std::string;

struct Type; struct Expression;

typedef map<string,Type> TypeMap;
typedef map<string,Expression> VarMap;

enum ExpressionType
{
    REFERENCE,
    VALUE,
    NULLTYPE
};

struct Expression
{
    ExpressionType value_or_reference = VALUE;
    bool is_constant = true;
    string mips_reg = "";
    int value = -1;
    int offset = -1;
    string string_literal_address = "";
    Type *type = NULL;
};

Expression* Make_Expression(char*);
Expression* Make_Expression(int);
Expression* Make_Expression(char);


enum TypeType
{
    SIMPLE_TYPE,
    RECORD_TYPE,
    ARRAY_TYPE,
    NULL_TYPE
};

struct Type
{
    TypeType typetype = SIMPLE_TYPE;
    int size = 4; //default size is 4, record types and arrays are larger.
    Type * type;

    VarMap members;
    Type * base_type;
    int array_begin;
    int array_end;
};

extern Type internal_char;
extern Type internal_int;
extern Type internal_boolean;
extern Type internal_string;