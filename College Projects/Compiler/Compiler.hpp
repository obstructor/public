#pragma once

#include "SymbolTable.hpp"

enum MathOperator
{
    OP_ADD,
    OP_SUB,
    OP_MUL,
    OP_DIV,
    OP_MOD,
    OP_AND,
    OP_OR,
    OP_GT,
    OP_LT,
    OP_LTE,
    OP_GTE,
    OP_EQ,
    OP_NEQ,
    UNOP_MINUS,
    UNOP_NEGATE,
    UNOP_PRED,
    UNOP_SUCC,
    UNOP_ORD,
    UNOP_CHR
};

Expression * binary_math(MathOperator, Expression* left, Expression * right);
Expression * unary_math(MathOperator, Expression *);

vector<string> * Make_Identlist(char*);
vector<string> * Add_Identifier_tolist(char*, vector<string> *);
Type * Combine_FieldDecl(Type * left, Type * right);
Type * Make_FieldDecl(vector<string> * identlist, Type * type);
Type * FieldDecl_to_Record(Type * field_decl);

Expression * Lvalue_Dot_Lvalue(Expression * parent, string child_name);
Expression * Lvalue_From_Brackets(Expression * parent, Expression * position);

void Footer_Code();
void Read_Lvalue(Expression*);
void Write_Lvalue(Expression*);
void Stop_Program();