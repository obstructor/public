#include "Compiler.hpp"
#include "globals.hpp"

void emit_mips_opening()
{
    cout << ".globl main\n";
    cout << ".text\n";
    cout << "main: la $gp, GA\n";
    cout << "j read_main\n";
}

SymbolTable::SymbolTable()
{
    internal_char.size = 4;
    internal_char.type = &internal_char;

    internal_int.size = 4;
    internal_int.type = &internal_int;

    internal_boolean.size = 4;
    internal_boolean.type = &internal_boolean;

    internal_string.size = 0;
    internal_string.type = &internal_string;

    current_offset =  0;
    internal_type_table_stack.push_back(Create_Predefined_Types());
    internal_variable_table_stack.push_back(Create_Predefined_Variables());

    emit_mips_opening();

    // enter global scope
    Enter_Scope();
}

TypeMap SymbolTable::Create_Predefined_Types()
{
    TypeMap predefined_layer;
    Type current_type;

    current_type.type = &internal_char;
    current_type.size = 4;
    predefined_layer["character"] = current_type;
    predefined_layer["CHARACTER"] = current_type;
    predefined_layer["CHAR"] = current_type;
    predefined_layer["char"] = current_type;
    predefined_layer["CHR"] = current_type;
    predefined_layer["chr"] = current_type;

    current_type.type = &internal_int;
    current_type.size = 4;
    predefined_layer["integer"] = current_type;
    predefined_layer["INTEGER"] = current_type;
    predefined_layer["int"] = current_type;
    predefined_layer["INT"] = current_type;

    current_type.type = &internal_boolean;
    current_type.size = 4;
    predefined_layer["boolean"] = current_type;
    predefined_layer["BOOLEAN"] = current_type;
    predefined_layer["bool"] = current_type;
    predefined_layer["BOOL"] = current_type;

    return predefined_layer;
}

VarMap SymbolTable::Create_Predefined_Variables()
{
    VarMap predefined_layer;
    Expression current_expression;

    current_expression.value = 0;
    current_expression.type = &internal_boolean;
    current_expression.is_constant = true;
    
    predefined_layer["False"] = current_expression;
    predefined_layer["false"] = current_expression;
    predefined_layer["FALSE"] = current_expression;


    current_expression.value = 1;
    current_expression.type = &internal_boolean;
    current_expression.is_constant = true;

    predefined_layer["True"] = current_expression;
    predefined_layer["true"] = current_expression;
    predefined_layer["TRUE"] = current_expression;

    return predefined_layer;
}

int SymbolTable::New_Offset(Type variable)
{
    int offset = current_offset;
    current_offset += variable.size;
    return offset;
}

void SymbolTable::Enter_Scope()
{
    TypeMap type_map; VarMap var_map;
    internal_type_table_stack.push_back(type_map);
    internal_variable_table_stack.push_back(var_map);
}

void SymbolTable::Exit_Scope()
{
    internal_type_table_stack.pop_back();
    internal_variable_table_stack.pop_back();
}

string SymbolTable::Add_String(string name)
{
    string position_label = string("STR") + std::to_string(internal_string_table.size());
    internal_string_table.push_back(name);
    return position_label;
}

Expression * SymbolTable::Get_Variable(string identifier)
{
    for(int i = internal_variable_table_stack.size() - 1; i >= 0;--i)
    {
        if (internal_variable_table_stack[i].find(identifier) != internal_variable_table_stack[i].end())
        {
            return &internal_variable_table_stack[i][identifier];
        }
    }
    cout << "\n## Did not find variable " << identifier << " in symbol table\n";
    throw "Did not find variable in symbol table";
}

void SymbolTable::Add_Variable(Expression* value,string identifier)
{
    if(!(value->is_constant))
    {
        value->offset = New_Offset(*value->type);
    }
    internal_variable_table_stack.back()[identifier] = *value;
}

Type* SymbolTable::Get_Type(string identifier)
{
    for(int i = internal_type_table_stack.size() - 1; i >= 0;--i)
    {
        if (internal_type_table_stack[i].find(identifier) != internal_type_table_stack[i].end())
        {
            return &internal_type_table_stack[i][identifier];
        }

    }
    cout << "\n## Did not find type " << identifier << " in symbol table\n";
    throw "Did not find type in symbol table";
}

void SymbolTable::Add_SimpleType(Type* type, string identifier)
{
    internal_type_table_stack.back()[identifier] = *type;
}

void SymbolTable::Add_Const_Variable(Expression* variable, string identifier)
{
    variable->is_constant = true;

    Add_Variable(variable, identifier);
}

void SymbolTable::Add_Variables_IdentList(vector<string> * ident_list, Type* type)
{
    Expression * ident_expr = new Expression();
    ident_expr->type = type->type;
    ident_expr->is_constant = false;

    for(int i = 0; i < ident_list->size();++i)
    {
        Add_Variable(ident_expr, (*ident_list)[i]);
    }
}

void Assign_Variable_Object(Expression * variable, Expression* value)
{
    string copy_reg = register_pool.request();
    string value_reg = "";
    for(int i = 0; i < variable->type->size;i += 4)
    {
        if(value->value_or_reference == REFERENCE)
        {
            cout << "lw " << copy_reg << ", " << i << "(" << value->mips_reg << ")\n";
        }
        else
        {
            cout << "lw " << copy_reg << ", " << value->offset + i << "($gp)\n";
        }
        if(variable->value_or_reference == REFERENCE)
        {
            cout << "sw " << copy_reg << ", " << i << "(" << variable->mips_reg << ")\n";
        }
        else
        {
            cout << "sw " << copy_reg << ", " << variable->offset + i << "($gp)\n";
        }
    }
    register_pool.free(copy_reg);
    register_pool.free(variable->mips_reg);
    register_pool.free(value->mips_reg);
}

void SymbolTable::Assign_Variable(Expression * variable, Expression* value)
{
    
    if(variable->type == value->type)
    {
        if(variable->type->typetype == RECORD_TYPE || variable->type->typetype == ARRAY_TYPE)
        {
            Assign_Variable_Object(variable, value);
        }
        else
        {
            if(value->value_or_reference == REFERENCE)
            {
                cout << "lw " << value->mips_reg << ", 0(" << value->mips_reg << ")\n";
                value->value_or_reference = VALUE;
            }
            if(value->is_constant)
            {
                value->mips_reg = register_pool.request();
                cout << "li " << value->mips_reg << ", " << value->value <<"\n";
            }
            else if(value->mips_reg.empty())
            {
                value->mips_reg = register_pool.request();
                cout << "lw " << value->mips_reg << ", " << value->offset << "($gp)\n";
            }

            if(variable->value_or_reference == REFERENCE)
            {
                cout << "sw " << value->mips_reg << ", 0(" << variable->mips_reg << ")\n";
                value->value_or_reference = VALUE;
            }
            else
            {
                cout << "sw " << value->mips_reg << ", " << variable->offset << "($gp)\n";
            }
            register_pool.free(variable);
            register_pool.free(value);
        }
    }
}

void SymbolTable::Dump_Strings()
{
    for(int i = 0;i < internal_string_table.size();++i)
    {
        cout << "STR" << i << ": .asciiz " << internal_string_table[i].c_str() << "\n";
    }
}

Type * SymbolTable::Make_Array_Type(Expression * begin, Expression * end, Type * base)
{
    Type* new_array_type = new Type();
    new_array_type->base_type = base->type;
    new_array_type->type = new_array_type;
    new_array_type->typetype = ARRAY_TYPE;

    if(begin->is_constant && end->is_constant)
    {
        new_array_type->array_begin = begin->value;
        new_array_type->array_end = end->value;
    }
    else
    {
        cout << "\n##Array bounds non-constants\n";
        throw "Crash me";
    } 

    new_array_type->size = base->size * (end->value - begin->value + 1);

    return new_array_type;
}