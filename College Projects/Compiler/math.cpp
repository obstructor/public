#include "Compiler.hpp"

 using std::cout;

Expression * binary_math(MathOperator instruction, Expression * left, Expression * right)
{
    Expression * result;
    if(left->type == right->type)
        {
            result = new Expression();
            result->type = left->type;

            if(left->is_constant && right->is_constant)
            {
                cout << "## 2 constant math\n";
                result->is_constant = true;
                bool result_bool;

                switch(instruction)
                {
                    case OP_ADD:
                        result->value = left->value + right->value;
                    break;
                    case OP_SUB:
                        result->value = left->value - right->value;
                    break;
                    case OP_MUL:
                        result->value = left->value * right->value;
                    break;
                    case OP_DIV:
                        result->value = left->value / right->value;
                    break;
                    case OP_MOD:
                        result->value = left->value % right->value;
                    break;
                    case OP_AND:
                        result_bool = left->value & right->value;
                        result->value = (result_bool)? 1 : 0;
                    break;
                    case OP_OR:
                        result_bool = left->value | right->value;
                        result->value = (result_bool)? 1 : 0;
                    break;
                    case OP_GT:
                        result_bool = left->value > right->value;
                        result->value = (result_bool)? 1 : 0;
                    break;
                    case OP_LT:
                        result_bool = left->value < right->value;
                        result->value = (result_bool)? 1 : 0;
                    break;
                    case OP_LTE:
                        result_bool = left->value <= right->value;
                        result->value = (result_bool)? 1 : 0;
                    break;
                    case OP_GTE:
                        result_bool = left->value >= right->value;
                        result->value = (result_bool)? 1 : 0;
                    break;
                    case OP_EQ:
                        result_bool = left->value == right->value;
                        result->value = (result_bool)? 1 : 0;
                    break;
                    case OP_NEQ:
                        result_bool = left->value != right->value;
                        result->value = (result_bool)? 1 : 0;
                    break;
                }
            }
            else if(right->is_constant)
            {
                cout << "## right is constant math\n";
                result->is_constant = false;
                if(left->mips_reg.empty())
                {
                    left->mips_reg = register_pool.request();
                    cout << "lw " << left->mips_reg << ", " << left->offset << "($gp)\n";
                }
                string temp_reg;
                switch(instruction)
                {
                    case OP_ADD:
                        cout << "addi " << left->mips_reg << ", " <<left->mips_reg << ", " << right->value << "\n";
                    break;
                    case OP_SUB:
                        cout << "addi " << left->mips_reg << ", " << left->mips_reg << ", " << (right->value * - 1) << "\n";
                    break;
                    case OP_MUL:
                        temp_reg = register_pool.request();
                        cout << "li " << temp_reg << ", " << right->value << "\n";
                        cout << "mult " << left->mips_reg << ", " << temp_reg << "\n";
                        cout << "mflo " << left->mips_reg << "\n";
                        register_pool.free(temp_reg);
                    break;
                    case OP_DIV:
                        temp_reg = register_pool.request();
                        cout << "li " << temp_reg << ", " << right->value << "\n";
                        cout << "div " << left->mips_reg << ", " << temp_reg << "\n";
                        cout << "mflo " << left->mips_reg << "\n";
                        register_pool.free(temp_reg);
                    break;
                    case OP_MOD:
                        temp_reg = register_pool.request();
                        cout << "li " << temp_reg << ", " << right->value << "\n";
                        cout << "div " << left->mips_reg << ", " << temp_reg << "\n";
                        cout << "mfhi " << left->mips_reg << "\n";
                        register_pool.free(temp_reg);
                    break;
                    case OP_AND:
                        cout << "andi " << left->mips_reg << ", " << left->mips_reg << ", " << right->value << "\n";
                    break;
                    case OP_OR:
                        cout << "ori " << left->mips_reg << ", " << left->mips_reg << ", " << right->value << "\n";
                    break;
                    case OP_GT:
                        temp_reg = register_pool.request();
                        cout << "li " << temp_reg << ", " << right->value << "\n";
                        cout << "sgt " << left->mips_reg << ", " << left->mips_reg << ", " << temp_reg << "\n";
                        register_pool.free(temp_reg);
                    break;
                    case OP_LT:
                        temp_reg = register_pool.request();
                        cout << "li " << temp_reg << ", " << right->value << "\n";
                        cout << "slt " << left->mips_reg << ", " << left->mips_reg << ", " << temp_reg << "\n";
                        register_pool.free(temp_reg);
                    break;
                    case OP_LTE:
                        temp_reg = register_pool.request();
                        cout << "li " << temp_reg << ", " << right->value << "\n";
                        cout << "sle " << left->mips_reg << ", " << left->mips_reg << ", " << temp_reg << "\n";
                        register_pool.free(temp_reg);
                    break;
                    case OP_GTE:
                        temp_reg = register_pool.request();
                        cout << "li " << temp_reg << ", " << right->value << "\n";
                        cout << "sge " << left->mips_reg << ", " << left->mips_reg << ", " << temp_reg << "\n";
                        register_pool.free(temp_reg);
                    break;
                    case OP_EQ:
                        temp_reg = register_pool.request();
                        cout << "li " << temp_reg << ", " << right->value << "\n";
                        cout << "seq " << left->mips_reg << ", " << left->mips_reg << ", " << temp_reg << "\n";
                        register_pool.free(temp_reg);
                    break;
                    case OP_NEQ:
                        temp_reg = register_pool.request();
                        cout << "li " << temp_reg << ", " << right->value << "\n";
                        cout << "sne " << left->mips_reg << ", " << left->mips_reg << ", " << temp_reg << "\n";
                        register_pool.free(temp_reg);
                    break;
                }
                result->mips_reg = left->mips_reg; left->mips_reg.clear();
            }
            else if(left->is_constant)
            {
                cout << "## left is constant math\n";

                result->is_constant = false;
                if(right->mips_reg.empty())
                {
                    right->mips_reg = register_pool.request();
                    cout << "lw " << right->mips_reg << ", " << right->offset << "($gp)\n";
                }
                string temp_reg;
                switch(instruction)
                {
                    case OP_ADD:
                        cout << "addi " << right->mips_reg << ", " << right->mips_reg << ", " << left->value << "\n";
                    break;
                    case OP_SUB:
                        temp_reg = register_pool.request();
                        cout << "li " << temp_reg << ", " << left->value << "\n";
                        cout << "sub " << right->mips_reg << ", " << temp_reg << "\n";
                        register_pool.free(temp_reg);
                    break;
                    case OP_MUL:
                        temp_reg = register_pool.request();
                        cout << "li " << temp_reg << ", " << left->value << "\n";
                        cout << "mult " << right->mips_reg << ", " << temp_reg << "\n";
                        cout << "mflo " << right->mips_reg << "\n";
                        register_pool.free(temp_reg);
                    break;
                    case OP_DIV:
                        temp_reg = register_pool.request();
                        cout << "li " << temp_reg << ", " << left->value << "\n";
                        cout << "div " << right->mips_reg << ", " << temp_reg << "\n";
                        cout << "mflo " << right->mips_reg << "\n";
                        register_pool.free(temp_reg);
                    break;
                    case OP_MOD:
                        temp_reg = register_pool.request();
                        cout << "li " << temp_reg << ", " << left->value << "\n";
                        cout << "div " << right->mips_reg << ", " << temp_reg << "\n";
                        cout << "mfhi " << right->mips_reg << "\n";
                        register_pool.free(temp_reg);
                    break;
                    case OP_AND:
                        cout << "andi " << right->mips_reg << ", " << right->mips_reg << ", " << left->value << "\n";
                    break;
                    case OP_OR:
                        cout << "ori " << right->mips_reg << ", " << right->mips_reg << ", " << left->value << "\n";
                    break;
                    case OP_GT:
                        temp_reg = register_pool.request();
                        cout << "li " << temp_reg << ", " << left->value << "\n";
                        cout << "sgt " << right->mips_reg << ", " << temp_reg << "\n";
                        register_pool.free(temp_reg);
                    break;
                    case OP_LT:
                        temp_reg = register_pool.request();
                        cout << "li " << temp_reg << ", " << left->value << "\n";
                        cout << "slt " << right->mips_reg << ", " << temp_reg << "\n";
                        register_pool.free(temp_reg);
                    break;
                    case OP_LTE:
                        temp_reg = register_pool.request();
                        cout << "li " << temp_reg << ", " << left->value << "\n";
                        cout << "sle " << right->mips_reg << ", " << temp_reg << "\n";
                        register_pool.free(temp_reg);
                    break;
                    case OP_GTE:
                        temp_reg = register_pool.request();
                        cout << "li " << temp_reg << ", " << left->value << "\n";
                        cout << "sge " << right->mips_reg << ", " << temp_reg << "\n";
                        register_pool.free(temp_reg);
                    break;
                    case OP_EQ:
                        temp_reg = register_pool.request();
                        cout << "li " << temp_reg << ", " << left->value << "\n";
                        cout << "seq " << right->mips_reg << ", " << temp_reg << "\n";
                        register_pool.free(temp_reg);
                    break;
                    case OP_NEQ:
                        temp_reg = register_pool.request();
                        cout << "li " << temp_reg << ", " << left->value << "\n";
                        cout << "sne " << right->mips_reg << ", " << temp_reg << "\n";
                        register_pool.free(temp_reg);
                    break;
                }
                result->mips_reg = right->mips_reg; right->mips_reg.clear();
            }
            else
            {
                cout << "## 2 variable math\n";
                result->is_constant = false;
                if(left->mips_reg.empty())
                {
                    left->mips_reg = register_pool.request();
                    cout << "lw " << left->mips_reg << ", " << left->offset << "($gp)\n";
                }
                if(right->mips_reg.empty())
                {
                    right->mips_reg = register_pool.request();
                    cout << "lw " << right->mips_reg << ", " << right->offset << "($gp)\n";
                }

                switch(instruction)
                {
                    case OP_ADD:
                        cout << "add " << left->mips_reg << ", " << left->mips_reg << ", " << right->mips_reg << "\n";
                    break;
                    case OP_SUB:
                        cout << "sub " << left->mips_reg << ", " << left->mips_reg << ", " << right->mips_reg << "\n";
                    break;
                    case OP_MUL:
                        cout << "mult " << left->mips_reg << ", " << right->mips_reg << "\n";
                        cout << "mflo " << left->mips_reg << "\n";
                    break;
                    case OP_DIV:
                        cout << "div " << left->mips_reg << ", " << right->mips_reg << "\n";
                        cout << "mflo " << left->mips_reg << "\n";
                    break;
                    case OP_MOD:
                        cout << "div " << left->mips_reg << ", " << right->mips_reg << "\n";
                        cout << "mfhi " << left->mips_reg << "\n";
                    break;
                    case OP_AND:
                        cout << "and " << left->mips_reg << ", " << left->mips_reg << ", " << right->mips_reg << "\n";
                    break;
                    case OP_OR:
                        cout << "or " << left->mips_reg << ", " << left->mips_reg << ", " << right->mips_reg << "\n";
                    break;
                    case OP_GT:
                        cout << "sgt " << left->mips_reg << ", " << left->mips_reg << ", " << right->mips_reg << "\n";
                    break;
                    case OP_LT:
                        cout << "slt " << left->mips_reg << ", " << left->mips_reg << ", " << right->mips_reg << "\n";
                    break;
                    case OP_LTE:
                        cout << "sle " << left->mips_reg << ", " << left->mips_reg << ", " << right->mips_reg << "\n";
                    break;
                    case OP_GTE:
                        cout << "sge " << left->mips_reg << ", " << left->mips_reg << ", " << right->mips_reg << "\n";
                    break;
                    case OP_EQ:
                        cout << "seq " << left->mips_reg << ", " << left->mips_reg << ", " << right->mips_reg << "\n";
                    break;
                    case OP_NEQ:
                        cout << "sne " << left->mips_reg << ", " << left->mips_reg << ", " << right->mips_reg << "\n";
                    break;
                }
                result->mips_reg = left->mips_reg; left->mips_reg.clear(); 
                register_pool.free(right);
            }

            return result;
        }
        else 
        {
            cout << "\n##Unmatched type in binary math expression\n";
            throw "Unmatched type in binary math expression";
        }
}

Expression * unary_math(MathOperator instruction, Expression * right)
{
    Expression * result = new Expression();
    result->type = right->type;

    if(right->is_constant)
    {
        result->is_constant = true;

        switch(instruction)
        {
            case UNOP_MINUS:
                result->value = right->value * -1;
            break;
            case UNOP_NEGATE:
                result->value = (right->value == 0)? 1 : 0;
            break;
            case UNOP_PRED:
                if(right->type = &internal_boolean)
                    result->value = (right->value == 0)? 1 : 0;
                else
                    result->value = right->value - 1;
            break;
            case UNOP_SUCC:
                if(right->type = &internal_boolean)
                    result->value = (right->value == 0)? 1 : 0;
                else
                    result->value = right->value + 1;
            break;
            case UNOP_ORD:
                result->value = right->value;
                result->type = &internal_int;
            break;
            case UNOP_CHR:
                result->value = right->value;
                result->type = &internal_char;
            break;
            default:
                cout << "\n##Unary operation unknown\n";
                throw "Unary operation unknown";
            break;
        }
    }
    else
    {
        result->is_constant = false;
        if(right->mips_reg.empty())
        {
            right->mips_reg = register_pool.request();
            cout << "lw " << right->mips_reg << ", " << right->offset << "($gp)\n";
        }

        switch(instruction)
        {
            case UNOP_MINUS:
               cout << "sub " << right->mips_reg << ", $0, " << right->mips_reg << "\n";
            break;
            case UNOP_NEGATE:
                cout << "neg " << right->mips_reg << ", " << right->mips_reg << "\n";
            break;
            case UNOP_PRED:
                if(right->type = &internal_boolean)
                    cout << "neg " << right->mips_reg << ", " << right->mips_reg << "\n";
                else
                    cout << "addi " << right->mips_reg << ", " << right->mips_reg << ", -1\n";
            break;
            case UNOP_SUCC:
                if(right->type = &internal_boolean)
                    cout << "neg " << right->mips_reg << ", " << right->mips_reg << "\n";
                else
                    cout << "addi " << right->mips_reg << ", " << right->mips_reg << ", 1\n";
            break;
            case UNOP_ORD:
                result->type = &internal_int;
            break;
            case UNOP_CHR:
                result->type = &internal_char;
            break;
            default:
                cout << "\n##Unary operation unknown\n";
                throw "Unary operation unknown";
            break;
        }
        result->mips_reg = right->mips_reg; right->mips_reg.clear();
    }
    return result;
}