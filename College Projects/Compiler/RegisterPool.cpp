#include "Compiler.hpp"

RegisterPool::RegisterPool()
{
    for (int i = 0; i < 8;++i)
    {
        internal_pool.push(string("$s") + std::to_string(i));
    }
    for (int i = 0; i < 10;++i)
    {
        internal_pool.push(string("$t") + std::to_string(i));
    }
}

string RegisterPool::request()
{
    string reg = internal_pool.top();
    internal_pool.pop();
    return reg;
}

void RegisterPool::free(Expression *free_me)
{
    if(!free_me->mips_reg.empty())
    {
        internal_pool.push(free_me->mips_reg);
        free_me->mips_reg.clear();
    }
}

void RegisterPool::free(string free_me)
{
    if(!free_me.empty())
    {
        internal_pool.push(free_me);
    }
}