#include "Compiler.hpp"


vector<string> * Make_Identlist(char* identifier)
{
    vector<string> * list = new vector<string>();
    list->push_back(string(identifier));
    return list;
}

vector<string> * Add_Identifier_tolist(char* identifier, vector<string> * list)
{
    list->push_back(string(identifier));
    return list;
}

Type * FieldDecl_to_Record(Type * field_decl)
{
    cout << "\n## Create record from fielddecls\n";
    int offset = 0;
    field_decl->size = 0;
    field_decl->type = field_decl;
    field_decl->typetype = RECORD_TYPE;
    for(auto i = field_decl->members.begin(); i != field_decl->members.end();++i)
    {
        i->second.offset = offset;
        offset += i->second.type->size;
        field_decl->size += i->second.type->size;
    }

    return field_decl;
}

Type * Combine_FieldDecl(Type * left, Type * right)
{
    cout << "\n## combine fielddecls\n";
    left->members.insert(right->members.begin(), right->members.end());
    return left;
}

Type * Make_FieldDecl(vector<string> * ident_list, Type * type)
{
    cout << "\n## Create Fielddecl\n";
    Type * record_type = new Type();
    for(int i = 0; i < ident_list->size(); ++i)
    {
        cout << "\n## Load fielddecl from ident list and type\n";
        Expression variable;
        variable.is_constant = false;
        variable.type = type->type;
        record_type->members[ident_list->at(i)] = variable;
    }
    return record_type;
}

Expression * Lvalue_Dot_Lvalue(Expression * parent, string child_name)
{
    if(parent->type->typetype != RECORD_TYPE)
    {
        cout << "\n## lvalue.lvalue on non-record type\n";
        throw "Hard Crash man";
    }

    Expression * result = new Expression();
    Expression child = parent->type->members[child_name];
    result->is_constant = false;
    result->type = child.type;

    if(parent->value_or_reference == VALUE)
    { 
        result->value_or_reference = VALUE;
        result->offset = parent->offset + child.offset;
    }
    else
    {
        result->value_or_reference = REFERENCE;
        cout << "addi " << parent->mips_reg << ", " << parent->mips_reg << ", " << child.offset << "\n";
        result->mips_reg = parent->mips_reg;
        parent->mips_reg.clear();
    }

    return result;
}

Expression * Lvalue_From_Brackets(Expression * parent, Expression * position)
{
    Expression * result = new Expression();
    if(parent->type->typetype != ARRAY_TYPE)
    {
        cout << "\n## lvalue[] on non-array type\n";
        throw "Hard Crash man";
    }

    result->is_constant = false;
    result->type = parent->type->base_type;

    if(position->is_constant)
    {
        if(parent->value_or_reference == VALUE)
        {
            cout << "\n## array[constant] value array\n";
            result->value_or_reference = VALUE;
            result->offset = parent->offset 
                + ((position->value - parent->type->array_begin) 
                * parent->type->base_type->size);

        }
        else
        {
            cout << "\n## array[constant] reference array\n";
            result->value_or_reference = REFERENCE;
            cout << "addi " << parent->mips_reg << ", " << parent->mips_reg << ", " << position->value << "\n";
            result->mips_reg = parent->mips_reg;
            parent->mips_reg.clear();
        }
    }
    else
    {
        result->value_or_reference = REFERENCE;
        if(position->mips_reg.empty())
        {
            position->mips_reg = register_pool.request();
            cout << "lw " << position->mips_reg << ", " << position->offset << "($gp)\n";
        }

        if(parent->value_or_reference == VALUE)
        {
            cout << "\n## array[i] value array" << position->mips_reg << "\n";
            cout << "subi " << position->mips_reg << ", " << position->mips_reg << ", " << parent->type->array_begin << "\n";
            cout << "mul " << position->mips_reg << ", " << position->mips_reg << ", " << parent->type->base_type->size << "\n"; 
            cout << "addi " << position->mips_reg << ", " << position->mips_reg << ", " << parent->offset << "\n";
            cout << "add " << position->mips_reg << ", " << position->mips_reg << ", $gp" << "\n";

        }
        else
        {
            cout << "\n## array[i] reference array\n";
            cout << "subi " << position->mips_reg << ", " << position->mips_reg << ", " << parent->type->array_begin << "\n";
            cout << "mul " << position->mips_reg << ", " << position->mips_reg << ", " << parent->type->base_type->size << "\n"; 
            cout << "addi " << position->mips_reg << ", " << position->mips_reg << ", " << parent->mips_reg << "\n";

        }
        result->mips_reg = position->mips_reg;
        position->mips_reg.clear();
    }
    return result;
}

void Footer_Code()
{
    // data section and global area.
    cout << ".data\n";
    symbol_table.Dump_Strings();
    cout << ".align 2 \nGA:\n";
}

void Read_Lvalue(Expression * value)
{
    if(!(value->mips_reg.empty()))
    {
        register_pool.free(value);
    }

    if(value->type == &internal_char)
    {
        cout << "## Read char\n";
        cout << "li $v0, 12\n";
        cout << "syscall\n";
        cout << "sw $v0, " << value->offset << "($gp)" <<"\n";
    }
    if(value->type == &internal_int)
    {
        cout << "## Read int\n";
        cout << "li $v0, 5\n";
        cout << "syscall\n";
        cout << "sw $v0, " << value->offset << "($gp)" <<"\n";
    }
}

void Write_Lvalue(Expression * value)
{
    if(value->type == &internal_string)
    {
        cout << "## Write string\n";
        cout << "li $v0, 4\n";
        cout << "la $a0, " << value->string_literal_address << "\n";
        cout << "syscall\n";
        return;
    }

    if(value->is_constant)
    {
        cout << "## Write constant\n";
        cout << "li $a0, " << value->value << "\n";
    }
    else if(value->mips_reg.empty())
    {
        cout << "lw " << "$a0" << ", " << value->offset << "($gp)\n";
    }
    else
    {
        cout << "move " << "$a0" << ", " << value->mips_reg << "\n";
        register_pool.free(value);
    }
    if(value->type == &internal_char)
    {
        cout << "## Write char\n";
        cout << "li $v0, 11\n";
        cout << "syscall\n";
    }
    else if(value->type == &internal_boolean)
    {
        cout << "## Write bool\n";
        cout << "li $v0, 1\n";
        cout << "syscall\n";
    }
    else if(value->type == &internal_int)
    {
        cout << "## Write int\n";
        cout << "li $v0, 1\n";
        cout << "syscall\n";
    }
    else
    {
        cout << "##Invalid type in write statement\n";
    }
}

void Stop_Program()
{
    cout << "## stop program\n";
    cout << "li $v0, 10\n";
    cout << "syscall\n";
}