#include "Compiler.hpp"

Expression * Make_Expression(char* source)
{
    Expression * new_expression = new Expression();
    new_expression->string_literal_address = symbol_table.Add_String(string(source));
    new_expression->is_constant = true;
    new_expression->type = &internal_string;
    new_expression->mips_reg = "";

    return new_expression;
}

Expression * Make_Expression(int source)
{
    Expression * new_expression = new Expression();
    new_expression->type = &internal_int;
    new_expression->is_constant = true;
    new_expression->value = source;
    new_expression->mips_reg = "";
       
    return new_expression;
}

Expression * Make_Expression(char source)
{

    Expression * new_expression = new Expression();
    new_expression->type = &internal_char;
    new_expression->is_constant = true;
    new_expression->value = source;
    new_expression->mips_reg = "";
       
    return new_expression;
}