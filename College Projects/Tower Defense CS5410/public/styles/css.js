CSTD.Stats = (function(){

    var that ={};

    that.ground1 = function()
    {
            var image = document.getElementById("stats");
            image.src = "styles/Pictures/TowerStats/1-1.png";
            var item = document.getElementById('upgrade');
            item.style.visibility = 'visible';
            var item2 = document.getElementById('sell');
            item2.style.visibility = 'visible';
    }
    
    that.ground2 = function()
    {
            var image = document.getElementById("stats");
            image.src = "styles/Pictures/TowerStats/2-1.png";
            var item = document.getElementById('upgrade');
            item.style.visibility = 'visible';
            var item2 = document.getElementById('sell');
            item2.style.visibility = 'visible';
    }
    
    that.mixed = function()
    {
            var image = document.getElementById("stats");
            image.src = "styles/Pictures/TowerStats/3-1.png";
            var item = document.getElementById('upgrade');
            item.style.visibility = 'visible';
            var item2 = document.getElementById('sell');
            item2.style.visibility = 'visible';
    }
    
    that.air = function()
    {
            var image = document.getElementById("stats");
            image.src = "styles/Pictures/TowerStats/4-1.png";
            var item = document.getElementById('upgrade');
            item.style.visibility = 'visible';
            var item2 = document.getElementById('sell');
            item2.style.visibility = 'visible';
    }
    
    that.ground12 = function()
    {
            var image = document.getElementById("stats");
            image.src = "styles/Pictures/TowerStats/1-2.png";
            var item = document.getElementById('upgrade');
            item.style.visibility = 'visible';
            var item2 = document.getElementById('sell');
            item2.style.visibility = 'visible';
    }
    
    that.ground22 = function()
    {
            var image = document.getElementById("stats");
            image.src = "styles/Pictures/TowerStats/2-2.png";
            var item = document.getElementById('upgrade');
            item.style.visibility = 'visible';
            var item2 = document.getElementById('sell');
            item2.style.visibility = 'visible';
    }
    
    that.mixed2 = function()
    {
            var image = document.getElementById("stats");
            image.src = "styles/Pictures/TowerStats/3-2.png";
            var item = document.getElementById('upgrade');
            item.style.visibility = 'visible';
            var item2 = document.getElementById('sell');
            item2.style.visibility = 'visible';
    }
    
    that.air2 = function()
    {
            var image = document.getElementById("stats");
            image.src = "styles/Pictures/TowerStats/4-2.png";
            var item = document.getElementById('upgrade');
            item.style.visibility = 'visible';
            var item2 = document.getElementById('sell');
            item2.style.visibility = 'visible';
    }
    
    that.ground13 = function()
    {
            var image = document.getElementById("stats");
            image.src = "styles/Pictures/TowerStats/1-3.png";
            var item = document.getElementById('upgrade');
            item.style.visibility = 'visible';
            var item2 = document.getElementById('sell');
            item2.style.visibility = 'visible';
    }
    
    that.ground23 = function()
    {
            var image = document.getElementById("stats");
            image.src = "styles/Pictures/TowerStats/2-3.png";
            var item = document.getElementById('upgrade');
            item.style.visibility = 'visible';
            var item2 = document.getElementById('sell');
            item2.style.visibility = 'visible';
    }
    
    that.mixed3 = function()
    {
            var image = document.getElementById("stats");
            image.src = "styles/Pictures/TowerStats/3-3.png";
            var item = document.getElementById('upgrade');
            item.style.visibility = 'visible';
            var item2 = document.getElementById('sell');
            item2.style.visibility = 'visible';
    }
    
    that.air3 = function()
    {
            var image = document.getElementById("stats");
            image.src = "styles/Pictures/TowerStats/4-3.png";
            var item = document.getElementById('upgrade');
            item.style.visibility = 'visible';
            var item2 = document.getElementById('sell');
            item2.style.visibility = 'visible';
            
    }

    that.clear = function()
    {
            var image = document.getElementById("stats");
            image.src = "styles/Pictures/TowerStats/default.png";
            var item = document.getElementById('upgrade');
            item.style.visibility = 'hidden';
            var item2 = document.getElementById('sell');
            item2.style.visibility = 'hidden';
    }
    
    that.background1 = function()
    {
            var image = document.getElementById("canvas-main");
            image.style.backgroundImage = "url('styles/Pictures/Grid3.png')";
    }
        
    that.background2 = function()
    {
            var image = document.getElementById("canvas-main");
            image.style.backgroundImage = "url('styles/Pictures/Grid.png')";
    }

    return that;
    
}());
