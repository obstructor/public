CSTD.SoundManager = (function(){

	var that = {
		blehcount: 0,
		boomcount: 0,
		freezecount: 0,
		horraycount: 0,
		missilecount: 0,
		pewcount: 0,
		sellcount: 0,
		towercount: 0,
		upgradecount: 0
	};
	
	var bleh = [];
	var boom = [];
	var freeze = [];
	var horray = [];
	var missile = [];
	var pew = [];
	var sell = [];
	var tower = [];
	var upgrade = [];
	
	that.Initialize = function(){
		for(var i = 0; i < 10; i++){
			bleh[i] = new Audio();
			bleh[i].src = "styles/sounds/bleh/bleh" + i + ".wav";
			boom[i] = new Audio();
			boom[i].src = "styles/sounds//boom/boom" + i + ".wav";
			freeze[i] = new Audio();;
			freeze[i].src = "styles/sounds/freeze/freeze" + i + ".wav";
			horray[i] = new Audio();;
			horray[i].src = "styles/sounds/horray/horray" + i + ".wav";
			missile[i] = new Audio();;
			missile[i].src = "styles/sounds/missile/missile" + i + ".wav";
			pew[i] = new Audio();;
			pew[i].src = "styles/sounds/pew/pew" + i + ".wav";
			pew[i].volume = .3;
			sell[i] = new Audio();;
			sell[i].src = "styles/sounds/sell/sell" + i + ".wav";
			tower[i] = new Audio();;
			tower[i].src = "styles/sounds/tower/tower" + i + ".wav";
			upgrade[i] = new Audio();;
			upgrade[i].src = "styles/sounds/upgrade/upgrade" + i + ".wav";
		}
	}

	that.bleh = function(){
		if(CSTD.SoundManager.blehcount === 0){
            bleh[0].play();
            CSTD.SoundManager.blehcount++;
        }
        else if(CSTD.SoundManager.blehcount === 1){
            bleh[1].play();
            CSTD.SoundManager.blehcount++;
        }
        else if(CSTD.SoundManager.blehcount === 2){
            bleh[2].play();
            CSTD.SoundManager.blehcount++;
        }
        else if(CSTD.SoundManager.blehcount === 3){
            bleh[3].play();
            CSTD.SoundManager.blehcount++;
        }
        else if(CSTD.SoundManager.blehcount === 4){
            bleh[4].play();
            CSTD.SoundManager.blehcount++;
        }
        else if(CSTD.SoundManager.blehcount === 5){
            bleh[5].play();
            CSTD.SoundManager.blehcount++;
        }
        else if(CSTD.SoundManager.blehcount === 6){
            bleh[6].play();
            CSTD.SoundManager.blehcount++;
        } 
        else if(CSTD.SoundManager.blehcount === 7){
            bleh[7].play();
            CSTD.SoundManager.blehcount++;
        }
        else if(CSTD.SoundManager.blehcount === 8){
            bleh[8].play();
            CSTD.SoundManager.blehcount++;
        }
        else if(CSTD.SoundManager.blehcount === 9){
            bleh[9].play();
            CSTD.SoundManager.blehcount = 0;
        }
	}
	
	that.boom = function(){
		if(CSTD.SoundManager.boomcount === 0){
            boom[0].play();
            CSTD.SoundManager.boomcount++;
        }
        else if(CSTD.SoundManager.boomcount === 1){
            boom[1].play();
            CSTD.SoundManager.boomcount++;
        }
        else if(CSTD.SoundManager.boomcount === 2){
            boom[2].play();
            CSTD.SoundManager.boomcount++;
        }
        else if(CSTD.SoundManager.boomcount === 3){
            boom[3].play();
            CSTD.SoundManager.boomcount++;
        }
        else if(CSTD.SoundManager.boomcount === 4){
            boom[4].play();
            CSTD.SoundManager.boomcount++;
        }
        else if(CSTD.SoundManager.boomcount === 5){
            boom[5].play();
            CSTD.SoundManager.boomcount++;
        }
        else if(CSTD.SoundManager.boomcount === 6){
            boom[6].play();
            CSTD.SoundManager.boomcount++;
        } 
        else if(CSTD.SoundManager.boomcount === 7){
            boom[7].play();
            CSTD.SoundManager.boomcount++;
        }
        else if(CSTD.SoundManager.boomcount === 8){
            boom[8].play();
            CSTD.SoundManager.boomcount++;
        }
        else if(CSTD.SoundManager.boomcount === 9){
            boom[9].play();
            CSTD.SoundManager.boomcount = 0;
        }
	}
	
	that.freeze = function(){
		if(CSTD.SoundManager.freezecount === 0){
            freeze[0].play();
            CSTD.SoundManager.freezecount++;
        }
        else if(CSTD.SoundManager.freezecount === 1){
            freeze[1].play();
            CSTD.SoundManager.freezecount++;
        }
        else if(CSTD.SoundManager.freezecount === 2){
            freeze[2].play();
            CSTD.SoundManager.freezecount++;
        }
        else if(CSTD.SoundManager.freezecount === 3){
            freeze[3].play();
            CSTD.SoundManager.freezecount++;
        }
        else if(CSTD.SoundManager.freezecount === 4){
            freeze[4].play();
            CSTD.SoundManager.freezecount++;
        }
        else if(CSTD.SoundManager.freezecount === 5){
            freeze[5].play();
            CSTD.SoundManager.freezecount++;
        }
        else if(CSTD.SoundManager.freezecount === 6){
            freeze[6].play();
            CSTD.SoundManager.freezecount++;
        } 
        else if(CSTD.SoundManager.freezecount === 7){
            freeze[7].play();
            CSTD.SoundManager.freezecount++;
        }
        else if(CSTD.SoundManager.freezecount === 8){
            freeze[8].play();
            CSTD.SoundManager.freezecount++;
        }
        else if(CSTD.SoundManager.freezecount === 9){
            freeze[9].play();
            CSTD.SoundManager.freezecount = 0;
        }
	}
	
	that.horray = function(){
		if(CSTD.SoundManager.horraycount === 0){
            horray[0].play();
            CSTD.SoundManager.horraycount++;
        }
        else if(CSTD.SoundManager.horraycount === 1){
            horray[1].play();
            CSTD.SoundManager.horraycount++;
        }
        else if(CSTD.SoundManager.horraycount === 2){
            horray[2].play();
            CSTD.SoundManager.horraycount = 0;
        }
        else if(CSTD.SoundManager.horraycount === 3){
            horray[3].play();
            CSTD.SoundManager.horraycount++;
        }
        else if(CSTD.SoundManager.horraycount === 4){
            horray[4].play();
            CSTD.SoundManager.horraycount++;
        }
        else if(CSTD.SoundManager.horraycount === 5){
            horray[5].play();
            CSTD.SoundManager.horraycount++;
        }
        else if(CSTD.SoundManager.horraycount === 6){
            horray[6].play();
            CSTD.SoundManager.horraycount++;
        } 
        else if(CSTD.SoundManager.horraycount === 7){
            horray[7].play();
            CSTD.SoundManager.horraycount++;
        }
        else if(CSTD.SoundManager.horraycount === 8){
            horray[8].play();
            CSTD.SoundManager.horraycount++;
        }
        else if(CSTD.SoundManager.horraycount === 9){
            horray[9].play();
            CSTD.SoundManager.horraycount = 0;
        }
	}
	
	that.missile = function(){
		if(CSTD.SoundManager.missilecount === 0){
            missile[0].play();
            CSTD.SoundManager.missilecount++;
        }
        else if(CSTD.SoundManager.missilecount === 1){
            missile[1].play();
            CSTD.SoundManager.missilecount++;
        }
        else if(CSTD.SoundManager.missilecount === 2){
            missile[2].play();
            CSTD.SoundManager.missilecount++;
        }
        else if(CSTD.SoundManager.missilecount === 3){
            missile[3].play();
            CSTD.SoundManager.missilecount++;
        }
        else if(CSTD.SoundManager.missilecount === 4){
            missile[4].play();
            CSTD.SoundManager.missilecount++;
        }
        else if(CSTD.SoundManager.missilecount === 5){
            missile[5].play();
            CSTD.SoundManager.missilecount++;
        }
        else if(CSTD.SoundManager.missilecount === 6){
            missile[6].play();
            CSTD.SoundManager.missilecount++;
        } 
        else if(CSTD.SoundManager.missilecount === 7){
            missile[7].play();
            CSTD.SoundManager.missilecount++;
        }
        else if(CSTD.SoundManager.missilecount === 8){
            missile[8].play();
            CSTD.SoundManager.missilecount++;
        }
        else if(CSTD.SoundManager.missilecount === 9){
            missile[9].play();
            CSTD.SoundManager.missilecount = 0;
        }
	}
	
	that.pew = function(){
		if(CSTD.SoundManager.pewcount === 0){
            pew[0].play();
            CSTD.SoundManager.pewcount++;
        }
        else if(CSTD.SoundManager.pewcount === 1){
            pew[1].play();
            CSTD.SoundManager.pewcount++;
        }
        else if(CSTD.SoundManager.pewcount === 2){
            pew[2].play();
            CSTD.SoundManager.pewcount++;
        }
        else if(CSTD.SoundManager.pewcount === 3){
            pew[3].play();
            CSTD.SoundManager.pewcount++;
        }
        else if(CSTD.SoundManager.pewcount === 4){
            pew[4].play();
            CSTD.SoundManager.pewcount++;
        }
        else if(CSTD.SoundManager.pewcount === 5){
            pew[5].play();
            CSTD.SoundManager.pewcount++;
        }
        else if(CSTD.SoundManager.pewcount === 6){
            pew[6].play();
            CSTD.SoundManager.pewcount++;
        } 
        else if(CSTD.SoundManager.pewcount === 7){
            pew[7].play();
            CSTD.SoundManager.pewcount++;
        }
        else if(CSTD.SoundManager.pewcount === 8){
            pew[8].play();
            CSTD.SoundManager.pewcount++;
        }
        else if(CSTD.SoundManager.pewcount === 9){
            pew[9].play();
            CSTD.SoundManager.pewcount = 0;
        }
	}
	
	that.sell = function(){
		if(CSTD.SoundManager.sellcount === 0){
            sell[0].play();
            CSTD.SoundManager.sellcount++;
        }
        else if(CSTD.SoundManager.sellcount === 1){
            sell[1].play();
            CSTD.SoundManager.sellcount++;
        }
        else if(CSTD.SoundManager.sellcount === 2){
            sell[2].play();
            CSTD.SoundManager.sellcount++;
        }
        else if(CSTD.SoundManager.sellcount === 3){
            sell[3].play();
            CSTD.SoundManager.sellcount++;
        }
        else if(CSTD.SoundManager.sellcount === 4){
            sell[4].play();
            CSTD.SoundManager.sellcount++;
        }
        else if(CSTD.SoundManager.sellcount === 5){
            sell[5].play();
            CSTD.SoundManager.sellcount++;
        }
        else if(CSTD.SoundManager.sellcount === 6){
            sell[6].play();
            CSTD.SoundManager.sellcount++;
        } 
        else if(CSTD.SoundManager.sellcount === 7){
            sell[7].play();
            CSTD.SoundManager.sellcount++;
        }
        else if(CSTD.SoundManager.sellcount === 8){
            sell[8].play();
            CSTD.SoundManager.sellcount++;
        }
        else if(CSTD.SoundManager.sellcount === 9){
            sell[9].play();
            CSTD.SoundManager.sellcount = 0;
        }
	}
	
		that.tower = function(){
		if(CSTD.SoundManager.towercount === 0){
            tower[0].play();
            CSTD.SoundManager.towercount++;
        }
        else if(CSTD.SoundManager.towercount === 1){
            tower[1].play();
            CSTD.SoundManager.towercount++;
        }
        else if(CSTD.SoundManager.towercount === 2){
            tower[2].play();
            CSTD.SoundManager.towercount++;
        }
        else if(CSTD.SoundManager.towercount === 3){
            tower[3].play();
            CSTD.SoundManager.towercount++;
        }
        else if(CSTD.SoundManager.towercount === 4){
            tower[4].play();
            CSTD.SoundManager.towercount++;
        }
        else if(CSTD.SoundManager.towercount === 5){
            tower[5].play();
            CSTD.SoundManager.towercount++;
        }
        else if(CSTD.SoundManager.towercount === 6){
            tower[6].play();
            CSTD.SoundManager.towercount++;
        } 
        else if(CSTD.SoundManager.towercount === 7){
            tower[7].play();
            CSTD.SoundManager.towercount++;
        }
        else if(CSTD.SoundManager.towercount === 8){
            tower[8].play();
            CSTD.SoundManager.towercount++;
        }
        else if(CSTD.SoundManager.towercount === 9){
            tower[9].play();
            CSTD.SoundManager.towercount = 0;
        }
	}
	
		that.upgrade = function(){
		if(CSTD.SoundManager.upgradecount === 0){
            upgrade[0].play();
            CSTD.SoundManager.upgradecount++;
        }
        else if(CSTD.SoundManager.upgradecount === 1){
            upgrade[1].play();
            CSTD.SoundManager.upgradecount++;
        }
        else if(CSTD.SoundManager.upgradecount === 2){
            upgrade[2].play();
            CSTD.SoundManager.upgradecount++;
        }
        else if(CSTD.SoundManager.upgradecount === 3){
            upgrade[3].play();
            CSTD.SoundManager.upgradecount++;
        }
        else if(CSTD.SoundManager.upgradecount === 4){
            upgrade[4].play();
            CSTD.SoundManager.upgradecount++;
        }
        else if(CSTD.SoundManager.upgradecount === 5){
            upgrade[5].play();
            CSTD.SoundManager.upgradecount++;
        }
        else if(CSTD.SoundManager.upgradecount === 6){
            upgrade[6].play();
            CSTD.SoundManager.upgradecount++;
        } 
        else if(CSTD.SoundManager.upgradecount === 7){
            upgrade[7].play();
            CSTD.SoundManager.upgradecount++;
        }
        else if(CSTD.SoundManager.upgradecount === 8){
            upgrade[8].play();
            CSTD.SoundManager.upgradecount++;
        }
        else if(CSTD.SoundManager.upgradecount === 9){
            upgrade[9].play();
            CSTD.SoundManager.upgradecount = 0;
        }
	}

    return that;

}())