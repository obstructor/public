 CSTD.GridManager = (function(){
    
    var that = {};
	that.flag = [false,false,false,false];
    
    that.isSolid = function(coord){
		if(coord.x < CSTD.constants.gridwidth && coord.y < CSTD.constants.gridheight && coord.x >= 0 && coord.y >= 0){
			return CSTD.grid[coord.x][coord.y].solid;
		}
		else return true;
	};
	
	that.isNotSolid = function(coord){
		if(coord.x < CSTD.constants.gridwidth && coord.y < CSTD.constants.gridheight && coord.x >= 0 && coord.y >= 0){
			if(CSTD.grid[coord.x][coord.y].solid === false)
			return true;
		}
		else return false;
	}
	
	that.Solid = function(coord){
		if(coord.x < CSTD.constants.gridwidth && coord.y < CSTD.constants.gridheight ){
			CSTD.grid[coord.x][coord.y].solid = true;
		}
		else return;
	};
	
	that.notSolid = function(coord){
		if(coord.x < CSTD.constants.gridwidth && coord.y < CSTD.constants.gridheight ){
			CSTD.grid[coord.x][coord.y].solid = false;
		}
		else return;
	};
	
	that.DtoE = function(coord, layer){
		if(coord.x >= 0 && coord.x < (CSTD.constants.gridwidth) && coord.y >= 0 && coord.y < (CSTD.constants.gridheight)){
			return CSTD.grid[Math.trunc(coord.x)][Math.trunc(coord.y)].DtoE[layer];
		}
		else return 999;
	}
	
	function Coordinate(coord)
	{
		var that = {};
		that.x = coord.x;
		that.y = coord.y;
		that.distance = coord.distance;
		return that;
	}
	
	function BreadthDtoE(coordinate, layer)
	{
		CSTD.GridManager.flag[layer] = false;
		var bread = [];
		var coord = {};
			coord.distance = 0;
			coord.x = coordinate.x; coord.y = coordinate.y;
		bread.push(Coordinate(coord));
		
		for(var i = 0; i < bread.length; ++i){
			
			CSTD.grid[bread[i].x][bread[i].y].DtoE[layer] = bread[i].distance;
			coord.x = bread[i].x; coord.y = bread[i].y;
			coord.distance = bread[i].distance + 1;
			coord.x = bread[i].x + 1; coord.y = bread[i].y;
			
			if((CSTD.GridManager.isNotSolid(coord)))
			{
				if(CSTD.grid[coord.x][coord.y].DtoE[layer]> bread[i].distance){
					if(CSTD.grid[coord.x][coord.y].visited[layer] === false){
						CSTD.grid[coord.x][coord.y].visited[layer] =true;
						bread.push(Coordinate(coord));
					}
				}
			}
			
			
			coord.x = bread[i].x - 1; coord.y = bread[i].y;
			
		if((CSTD.GridManager.isNotSolid(coord)))
			{
				if(CSTD.grid[coord.x][coord.y].DtoE[layer]> bread[i].distance){
					if(CSTD.grid[coord.x][coord.y].visited[layer] === false){
						CSTD.grid[coord.x][coord.y].visited[layer] =true;
						bread.push(Coordinate(coord));
					}
				}
			}
			
			coord.x = bread[i].x; coord.y = bread[i].y + 1;
			
			if((CSTD.GridManager.isNotSolid(coord)))
			{
				if(CSTD.grid[coord.x][coord.y].DtoE[layer]> bread[i].distance){
					if(CSTD.grid[coord.x][coord.y].visited[layer] === false){
						CSTD.grid[coord.x][coord.y].visited[layer] =true;
						bread.push(Coordinate(coord));
					}
				}
			}
			coord.x = bread[i].x; coord.y = bread[i].y - 1;
			
			if((CSTD.GridManager.isNotSolid(coord)))
			{
				if(CSTD.grid[coord.x][coord.y].DtoE[layer]> bread[i].distance){
					if(CSTD.grid[coord.x][coord.y].visited[layer] === false){
						CSTD.grid[coord.x][coord.y].visited[layer] =true;
						bread.push(Coordinate(coord));
					}
				}
			}
			
			switch(layer){
				case 0:
				if(bread[i].x === 0 && bread[i].y === Math.trunc(CSTD.constants.gridheight / 2)){
					CSTD.GridManager.flag[0] = true;
					}	
				break;
				case 1:
				if(bread[i].x === CSTD.constants.gridwidth - 1 && bread[i].y === Math.trunc(CSTD.constants.gridheight / 2)){
					CSTD.GridManager.flag[1] = true;
					}
				break;
				case 2:
				if(bread[i].x === Math.trunc(CSTD.constants.gridwidth / 2) && bread[i].y === Math.trunc(CSTD.constants.gridheight - 1)){
					CSTD.GridManager.flag[2] = true;
					}
				break;
				case 3:
				if(bread[i].x === Math.trunc(CSTD.constants.gridwidth / 2) && bread[i].y === 0){
					CSTD.GridManager.flag[3] = true;
					}
				break;
			}
		};
	};
	
	that.UpdateGrid = function(){
		CSTD.grid.forEach(function(value,index,array){
			value.forEach(function(lue,dex,ray){
				lue.DtoE = [999,999,999,999];
				lue.visited = [false,false,false,false];
			});
		});
		
		var coordinate = {
			x: CSTD.constants.gridwidth - 1,
			y: Math.trunc(CSTD.constants.gridheight / 2),
			distance : 0
		}
		
		BreadthDtoE(coordinate,0);
		coordinate.x = 0;
		
		if(CSTD.difficulty === 1 || CSTD.difficulty ===2){
			BreadthDtoE(coordinate,1);
			coordinate.x = Math.trunc(CSTD.constants.gridwidth / 2);
			coordinate.y = 0;
		}
		if(CSTD.difficulty === 2){
			BreadthDtoE(coordinate,2);
			coordinate.y = CSTD.constants.gridheight - 1;
		
			BreadthDtoE(coordinate,3);
		}
		
		if(CSTD.difficulty === 1){
			return CSTD.GridManager.flag[0] && CSTD.GridManager.flag[1];
		}
		if(CSTD.difficulty === 2){
			return CSTD.GridManager.flag[0] && CSTD.GridManager.flag[1] && CSTD.GridManager.flag[2] && CSTD.GridManager.flag[3];
		}
		return CSTD.GridManager.flag[0];
	}
    

	
     that.GenerateSquare = function(k, i){
        var square = {
            tower : {
				Update : function(){return;}
			},
            DtoE : [999,999,999,999],
            solid : false,
            towersolid : false,
            selected : false,
			visited : [false,false,false,false]
        };

        return square;
    }
    
    function GenerateColumn(k){
        var temp = [];
        for(var i =0;i < CSTD.constants.gridheight;++i){
            temp.push(CSTD.GridManager.GenerateSquare(k,i));
        }
		return temp;
    }
    
    function GenerateGrid(){
		CSTD.grid = [];
        for(var i = 0; i < CSTD.constants.gridwidth;++i){
            CSTD.grid.push(GenerateColumn(i))
        }

    }
    
    function SetBoundary(){
        for(var i =0; i < CSTD.constants.gridwidth; i++){
            CSTD.grid[i][0].solid = true;
            CSTD.grid[i][14].solid = true;
        }
        for(var i =1; i < CSTD.constants.gridheight; i++){
            CSTD.grid[0][i].solid = true;
            CSTD.grid[19][i].solid = true;
        }
		if(CSTD.difficulty === 2){
			CSTD.grid[Math.trunc(CSTD.constants.gridwidth / 2)][Math.trunc(CSTD.constants.gridheight) - 1].solid = false;
			CSTD.grid[Math.trunc(CSTD.constants.gridwidth / 2)][0].solid = false;
		}
		
		CSTD.grid[0][Math.trunc(CSTD.constants.gridheight / 2)].solid = false;
		CSTD.grid[CSTD.constants.gridwidth - 1][Math.trunc(CSTD.constants.gridheight / 2)].solid = false;
		
    
    }
	
     that.Initialize = function(){
        GenerateGrid();
		that.UpdateGrid();
        SetBoundary();
    };
    
    
    return that;
    
    
}());



CSTD.GridManager.Initialize();










