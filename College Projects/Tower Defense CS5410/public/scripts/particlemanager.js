CSTD.ParticleManager = (function(){
	var that = {};
	that.emitters = [];
	
	function GenerateRenderInformation(particle){
		var temp = {};
		temp.x = particle.position.x;
		temp.y = particle.position.y;
		return temp;
		}

	function GenerateTowerEmitter(coord){
		var emitter = {
			particles : [],
			position : {
				x: coord.x * 40,
				y: coord.y * 40
			}
		}
		for(var i = 0; i < 10; ++i){
			var part = {
				x : coord.x * 40 + 20,
				y : coord.y * 40 + 20,
				vx : Math.random() - .5,
				vy : Math.random() - .5,
				lifespan : 1000,
				type : 0,
				life : 0,
				speed : .1,
				Update : function(timer){
					this.life += timer;
					this.x = this.x + this.vx * this.speed * timer;
					this.y = this.y + this.vy * this.speed * timer;	
					if(this.life > this.lifespan){
						return true;
					}
					else return false;
				}
			}
			emitter.particles.push(part);
		}
		emitter.Update = function(){
			if(emitter.particles.length < 1){
				return true;
			}
			for(var i = 0; i < emitter.particles.length;++i){
				if(emitter.particles[i].Update(CSTD.timer)){
					emitter.particles.splice(i,1);
					}
			}
			return false;
		}
		return emitter;
	}
	
	function GenerateDeathEmitter(source){
		var emitter = {
			particles : [],
			position : {
				x: source.position.x,
				y: source.position.y
			},
			lifespan : 500,
			life : 0,
			Update : function(timer){
				this.life += timer;
				this.particles[0].Update(timer);
				if(this.life > this.lifespan){
					return true;
				}
				return false;
			}
		}
		emitter.particles.push({
			x: source.position.x,
			y: source.position.y,
			vy : .1,
			lifespan : 1000,
			life : 0,
			value : source.score,
			type : 1,
			Update: function(timer){
				this.life += timer;
				this.y -= this.vy * timer;
			}
		})
		return emitter;
	}
	
	function GenerateBombTrailEmitter(source){
		var emitter = {
			particles : [],
			position : {
				x: source.position.x,
				y: source.position.y
			},
			lifespan : 250,
			life : 0,
			Update : function(timer){
				this.life += timer;
				this.particles[0].Update(timer);
				if(this.life > this.lifespan){
					return true;
				}
				return false;
			}
		}
		emitter.particles.push({
			x: source.position.x,
			y: source.position.y,
			vy : .1,
			lifespan : 1000,
			life : 0,
			value : source.score,
			type : 2,
			Update: function(timer){
				return;
			}
		})
		return emitter;
	}
	
	function GenerateBombSplodeEmitter(coord){
		var emitter = {
			particles : [],
			position : {
				x: coord.x,
				y: coord.y
			},
			Update : function(){
				for(var i = 0; i < this.particles.length;++i){
					if(this.particles[i].Update(CSTD.timer)){
						this.particles.splice(i,1);
					}
					if(this.particles.length < 1){
						return true;
					}
					return false;
				}
			}
		}
		for(var i = 0; i < 10; ++i){
			var part = {
				x : coord.x,
				y : coord.y,
				vx : Math.random() - .5,
				vy : Math.random() - .5,
				lifespan : 5000,
				life : 0,
				type : 0,
				speed : .1,
				Update : function(timer){
					part.life += timer;
					if(part.life > part.lifespan){
						return true;
					}
					this.x = this.x + this.vx * this.speed * timer;
					this.y = this.y + this.vy * this.speed * timer;
					return false;	
				}
			}
			emitter.particles.push(part);
		}
		emitter.Update = function(){
			if(emitter.particles.length < 1){
				return true;
			}
			for(var i = 0; i < emitter.particles.length;++i){
				if(emitter.particles[i].Update(CSTD.timer)){
					emitter.particles.splice(i,1);
				}
			}
			return false;
		}
		return emitter;
	}
	
	function GenerateMissileEmitter(source){
		var temp = source;
		var emitter = {
			particles : [],
			position : {
				x: temp.x,
				y: temp.y
			},
			elapsed : 0,
			spawnrate : 10,
			Update : function(timer){
				if(source.dead === true){
					return true;
				}
				else {
					for(var i = 0; i < emitter.particles.length;++i){
						if(emitter.particles[i].Update(CSTD.timer)){
							emitter.particles.splice(i,1);
						}
					}
					emitter.position.x = temp.x;
					emitter.position.y = temp.y;
					emitter.elapsed += timer;
					if(emitter.elapsed > emitter.spawnrate){
						emitter.elapsed -= emitter.spawnrate;
						emitter.particles.push(
							{
								position : {
									x: emitter.position.x,
									y: emitter.position.y
								},
								lifespan : 1000,
								life : 0,
								type : 2,
								Update : function(timer){
									this.life += timer;
									if(this.life > this.lifespan){
										return true;
									}
									else return false;
								}
							}
						)
					}
				}	
			}
		}
		
		return emitter;
	}
	
	function GenerateMissileSplodeEmitter(coord){
		var emitter = {
			particles : [],
			position : {
				x: coord.x,
				y: coord.y
			},
			Update : function(){
				for(var i = 0; i < this.particles.length;++i){
					if(this.particles[i].Update(CSTD.timer)){
						this.particles.splice(i,1);
					}
				}
			}
		};
		for(var i = 0; i < 10; ++i){
			var part = {
				x : coord.x,
				y : coord.y,
				vx : Math.random() - .5,
				vy : Math.random() - .5,
				lifespan : 500,
				life : 0,
				type : 0,
				Update : function(timer){
					if(this.life > this.lifespan){
						return true;
					}
					this.life += timer;
					this.x = this.x + this.vx;
					this.y = this.y + this.vy;	
					return false;
				}
			}
			emitter.particles.push(part);
		}
		return emitter;
	}
	
	that.GenerateEmitter = function(type, coord, sourceobject){
		switch(type){
			case 0: // sell
			this.emitters.push(GenerateTowerEmitter(coord));
			break;
			case 1: // die creeps
			this.emitters.push(GenerateDeathEmitter(sourceobject));
			break;
			case 2: // bombtrail
			this.emitters.push(GenerateBombTrailEmitter(sourceobject));
			break;
			case 3: // bombsplode
			this.emitters.push(GenerateBombSplodeEmitter(coord));
			break;
			case 4: // missile trail
			this.emitters.push(GenerateMissileEmitter(sourceobject));
			break;
			case 5: // miisle splode
			this.emitters.push(GenerateMissileSplodeEmitter(coord));
			break;
		}
	}

	that.Update = function(time){
		for(var i = 0; i < this.emitters.length;++i){
			if(this.emitters[i].Update(time)){
				this.emitters.splice(i,1);	
			}
		}
	}
	
	that.RenderInformation = function(){
		var temp = [];
		this.emitters.forEach(function(value,index,array){
			value.Particles.forEach(function(lue,dex,ray){
				temp.push(GenerateRenderInformation(lue));
			})
		})
		return temp;
	}
	
	return that;
}())