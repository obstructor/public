

CSTD.Renderer = (function(){
	var that = {
         clicksquare : false,
         mouse : {}
    };
	var towerimages = [];
	var creepimages = [];
	var canvas = document.getElementById('canvas-main');
	var context = canvas.getContext('2d');
	
	function InitializeImages(){
		towerimages[0] = new Image();
		towerimages[0].src = "styles/turrets/turret-1-1.png";
		towerimages[1] = new Image();
		towerimages[1].src = "styles/turrets/turret-2-1.png";
		towerimages[2] = new Image();
		towerimages[2].src = "styles/turrets/turret-3-1.png";
		towerimages[3] = new Image();
		towerimages[3].src = "styles/turrets/turret-4-1.png";
        towerimages[4] = new Image();
		towerimages[4].src = "styles/turrets/turret-1-2.png";
		towerimages[5] = new Image();
		towerimages[5].src = "styles/turrets/turret-2-2.png";
		towerimages[6] = new Image();
		towerimages[6].src = "styles/turrets/turret-3-2.png";
		towerimages[7] = new Image();
		towerimages[7].src = "styles/turrets/turret-4-2.png";
        towerimages[8] = new Image();
		towerimages[8].src = "styles/turrets/turret-1-3.png";
		towerimages[9] = new Image();
		towerimages[9].src = "styles/turrets/turret-2-3.png";
		towerimages[10] = new Image();
		towerimages[10].src = "styles/turrets/turret-3-3.png";
		towerimages[11] = new Image();
		towerimages[11].src = "styles/turrets/turret-4-3.png";
	
		function creep(filename){
				var creep = [];
				for(var i = 1; i < 7;++i)
				{
				creep[i] = new Image();
				creep[i].src = filename + i + ".png";
				}
				return creep;
			}
	
		creepimages.push(creep("styles/creeps/blue/"));
		creepimages.push(creep("styles/creeps/green/"));
		
            
    function creep2(filename){
				var creep = [];
				for(var i = 1; i < 5;++i)
				{
				creep[i] = new Image();
				creep[i].src = filename + i + ".png";
				}
				return creep;
			}
	
		creepimages.push(creep2("styles/creeps/red2/"));
		creepimages.push(creep2("styles/creeps/red/"));
		
	}
    
    
	
	function RenderTowers(){
		CSTD.grid.forEach(function(value,index,array){
			value.forEach(function(lue,dex,ray){
				if(lue.solid === true && dex > 0 && dex < CSTD.constants.gridheight - 1 && index > 0 && index < CSTD.constants.gridwidth - 1){
                    if(lue.tower.type === -1){
						return;
                    }
                    else{
						context.save();
						context.translate(index * 40 + 20, dex * 40 + 20);
						context.rotate(CSTD.grid[index][dex].tower.angle + Math.PI / 2);
						context.drawImage(
						towerimages[lue.tower.type],
						-20/*CSTD.MonsterManager.Monsters[i].position.x - 20*/,
						-20/*CSTD.MonsterManager.Monsters[i].position.y - 20*/
						);
						context.restore();
                     //   context.drawImage(towerimages[lue.tower.type],
                   //     index * (800 / CSTD.constants.gridwidth),
                   //     dex * (600 / CSTD.constants.gridheight));
                    }
				}
			})
		})
	}
    
	function RenderCreeps(){
			for(var i = 0; i < CSTD.MonsterManager.Monsters.length;++i){
				context.beginPath();
				context.rect(CSTD.MonsterManager.Monsters[i].position.x - 15, CSTD.MonsterManager.Monsters[i].position.y - 20, 40,4);
				context.closePath();
				context.fillStyle = "red";
				context.fill();
				
				context.beginPath();
				context.rect(CSTD.MonsterManager.Monsters[i].position.x - 15, CSTD.MonsterManager.Monsters[i].position.y - 20, CSTD.MonsterManager.Monsters[i].health,4);
				context.closePath();
				context.fillStyle = "green";
				context.fill();
				
				context.fillStyle = "rgba(0,0,0,1)";
				
				if(CSTD.MonsterManager.Monsters[i].type === 2){
					context.save();
					context.translate(CSTD.MonsterManager.Monsters[i].position.x, CSTD.MonsterManager.Monsters[i].position.y);
					context.rotate(CSTD.MonsterManager.Monsters[i].direction);
					context.drawImage(
					creepimages[CSTD.MonsterManager.Monsters[i].type][CSTD.MonsterManager.Monsters[i].state],
					-10/*CSTD.MonsterManager.Monsters[i].position.x - 20*/,
					-10/*CSTD.MonsterManager.Monsters[i].position.y - 20*/,
					20,
					20
					);
				context.restore();
					
					
				}
				if(CSTD.MonsterManager.Monsters[i].type === 0){
					context.save();
					context.translate(CSTD.MonsterManager.Monsters[i].position.x, CSTD.MonsterManager.Monsters[i].position.y);
					context.rotate(CSTD.MonsterManager.Monsters[i].direction);
					context.drawImage(
					creepimages[CSTD.MonsterManager.Monsters[i].type][CSTD.MonsterManager.Monsters[i].state],
					-13/*CSTD.MonsterManager.Monsters[i].position.x - 20*/,
					-13/*CSTD.MonsterManager.Monsters[i].position.y - 20*/,
					26,
					26
					);
				context.restore();
					
					
				}
				else{
					context.save();
					context.translate(CSTD.MonsterManager.Monsters[i].position.x, CSTD.MonsterManager.Monsters[i].position.y);
					context.rotate(CSTD.MonsterManager.Monsters[i].direction);
					context.drawImage(
					creepimages[CSTD.MonsterManager.Monsters[i].type][CSTD.MonsterManager.Monsters[i].state],
					-15/*CSTD.MonsterManager.Monsters[i].position.x - 20*/,
					-15/*CSTD.MonsterManager.Monsters[i].position.y - 20*/,
					30,
					30
					);
				context.restore();
				}
		}
		return;
	}
	
	function RenderBullets(){
		for(var i = 0; i < CSTD.BulletManager.Bullets.length;++i){
			var daball = CSTD.BulletManager.Bullets[i];
			var radius = 0;
			switch(daball.type){
				case 0:
				case 4:
				case 8:
				context.fillStyle = "#FF0000";
				radius = 2;
				break;
				case 1:
				case 5:
				case 9:
				radius = 5;
				context.fillStyle = "#00FF00";
				break;
				case 2:
				case 6:
				case 10:context.fillStyle = "#FF00FF";
				radius = 2;
				break;
				case 3:
				case 7:
				case 11:
				context.fillStyle = "#0000FF";
				radius = 4;
				break;
				
			}
			context.beginPath(); // Copied from w3Schools.com
			context.arc(daball.position.x,daball.position.y, radius ,0,2*Math.PI);
			context.closePath();
			 context.fill();
			context.strokeStyle = "#00FF00"; context.stroke();
		}
		return;
	}
    
    function RenderGold(){
        context.font = "20px Arial";
        context.fillStyle = '#000000';
        context.beginPath();
        context.fillText("Gold:", 20, 630);
        context.fillText(CSTD.money, 75, 630);
        context.closePath();
    }
	
	
   function RenderLevels(){
        context.font = "20px Arial";
        context.fillStyle = '#000000';
        context.beginPath();
        context.fillText("Level:", 500, 630);
        context.fillText(CSTD.level, 565, 630);
        context.closePath();
    }
    
    function RenderScore(){
        context.font = "20px Arial";
        context.fillStyle = '#000000';
        context.beginPath();
        context.fillText("Score:", 200, 630);
        context.fillText(CSTD.score, 265, 630);
        context.closePath();
    }
    
    function RenderLives(){
        context.font = "20px Arial";
        context.fillStyle = '#000000';
        context.beginPath();
        context.fillText("Lives:", 400, 630);
        context.fillText(CSTD.lives, 455, 630);
        context.closePath();
    }
    
    function RenderSelectedSquare(){
        if(CSTD.Renderer.clicksquare === true){
            context.fillStyle = "rgba(255, 239, 15, 0.36)";
            context.beginPath();
            context.arc(CSTD.Renderer.mouse.x*40+20,CSTD.Renderer.mouse.y*40+20, CSTD.grid[CSTD.Renderer.mouse.x][CSTD.Renderer.mouse.y].tower.radius, 0, 2*Math.PI);
            context.lineWidth = "1"
            context.strokeStyle="#ffff00";
            context.stroke();
            context.closePath();
            context.fill();
            context.strokeStyle="#ff4d4d";
            context.beginPath();
            context.rect(CSTD.Renderer.mouse.x*40, CSTD.Renderer.mouse.y*40, 40, 40);
            context.closePath();
            context.stroke();
           if(CSTD.grid[CSTD.Renderer.mouse.x][CSTD.Renderer.mouse.y].tower.type === 0){
                CSTD.Stats.ground1();
           }
           else if(CSTD.grid[CSTD.Renderer.mouse.x][CSTD.Renderer.mouse.y].tower.type === 1){
                CSTD.Stats.ground2();
           }                      
           else if(CSTD.grid[CSTD.Renderer.mouse.x][CSTD.Renderer.mouse.y].tower.type === 2){
                CSTD.Stats.mixed();
           }
           else if(CSTD.grid[CSTD.Renderer.mouse.x][CSTD.Renderer.mouse.y].tower.type === 3){
                CSTD.Stats.air();
           }
           else if(CSTD.grid[CSTD.Renderer.mouse.x][CSTD.Renderer.mouse.y].tower.type === 4){
                CSTD.Stats.ground12();
           }
           else if(CSTD.grid[CSTD.Renderer.mouse.x][CSTD.Renderer.mouse.y].tower.type === 5){
                CSTD.Stats.ground22();
           }                      
           else if(CSTD.grid[CSTD.Renderer.mouse.x][CSTD.Renderer.mouse.y].tower.type === 6){
                CSTD.Stats.mixed2();
           }
           else if(CSTD.grid[CSTD.Renderer.mouse.x][CSTD.Renderer.mouse.y].tower.type === 7){
                CSTD.Stats.air2();
           }
           else if(CSTD.grid[CSTD.Renderer.mouse.x][CSTD.Renderer.mouse.y].tower.type === 8){
                CSTD.Stats.ground13();
           }
           else if(CSTD.grid[CSTD.Renderer.mouse.x][CSTD.Renderer.mouse.y].tower.type === 9){
                CSTD.Stats.ground23();
           }                      
           else if(CSTD.grid[CSTD.Renderer.mouse.x][CSTD.Renderer.mouse.y].tower.type === 10){
                CSTD.Stats.mixed3();
           }
           else if(CSTD.grid[CSTD.Renderer.mouse.x][CSTD.Renderer.mouse.y].tower.type === 11){
                CSTD.Stats.air3();
           }                      
        }
        
        
    }
	
	function RenderMouse(){
        if(CSTD.TowerManager.placeTower === true){
            if(CSTD.mouse.x < 800 && CSTD.mouse.y < 600 && CSTD.selected >= 0 && CSTD.selected <= 4){
                if(CSTD.grid[(Math.trunc(CSTD.mouse.x / (800 / CSTD.constants.gridwidth)))][(Math.trunc(CSTD.mouse.y / (600 / CSTD.constants.gridheight)))].solid === true)
                {
                    
                }
                else{
                    
                    context.fillStyle = "rgba(255, 239, 15, 0.36)";
                    context.beginPath();
                    context.arc(CSTD.mouse.x,CSTD.mouse.y, 80, 0, 2*Math.PI);
                    context.lineWidth = "1"
                    context.strokeStyle="#ffff00";
                    context.stroke();
                    context.closePath();
                    context.fill();
                    context.beginPath();
                    context.drawImage(towerimages[CSTD.selected],CSTD.mouse.x - 20,CSTD.mouse.y - 20);
                    context.closePath();
                    context.strokeStyle="#ff4d4d";
                    context.beginPath();
                    context.rect((Math.trunc(CSTD.mouse.x / (800 / CSTD.constants.gridwidth)))*40, (Math.trunc(CSTD.mouse.y / (600 / CSTD.constants.gridheight)))*40, 40, 40);
                    context.closePath();
                    context.stroke();
                }
            }
        } 
    }
    
    
	function RenderParticles(){
		for(var i = 0; i < CSTD.ParticleManager.emitters.length;++i){
			for(var k = 0; k < CSTD.ParticleManager.emitters[i].particles.length;++k){
				if(CSTD.ParticleManager.emitters[i].particles[k].type === 1){
					context.font = "10px Arial";
					context.fillStyle = '#c0c0c0';
					context.beginPath();
					context.fillText(CSTD.ParticleManager.emitters[i].particles[k].value, CSTD.ParticleManager.emitters[i].particles[k].x, CSTD.ParticleManager.emitters[i].particles[k].y);
					context.closePath();
					context.strokeStyle = "#000000"; context.stroke();
				}
				else if (CSTD.ParticleManager.emitters[i].particles[k].type === 0){
					var daball = CSTD.ParticleManager.emitters[i].particles[k];
					context.beginPath(); // Copied from w3Schools.com
					context.arc(daball.x,daball.y, 2,0,2*Math.PI);
					context.closePath();
					context.fillStyle = "#B0171F"; context.fill();
					context.strokeStyle = "#000000"; context.stroke();
				} else {
					daball = CSTD.ParticleManager.emitters[i].particles[k];
					context.beginPath(); // Copied from w3Schools.com
					context.arc(daball.x,daball.y, 2,0,2*Math.PI);
					context.closePath();
					context.fillStyle = "#FFFFFF"; context.fill();
					context.strokeStyle = "#000000"; context.stroke();
				}
			}
		}
	}
	
	that.Render = function(){
		context.clearRect(0, 0, canvas.width, canvas.height);
        RenderMouse();
		RenderTowers();
		RenderCreeps();
		RenderBullets();
        RenderSelectedSquare();
        RenderGold();
        RenderLives();
        RenderScore();
		RenderLevels();
		RenderParticles();
	}
	
	that.Initialize = function(){
		InitializeImages();
	}
	
	return that;
}())

CSTD.Renderer.Initialize();