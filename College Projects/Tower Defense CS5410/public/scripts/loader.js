/*jslint browser: true, white: true */
/*global window, Modernizr, Image, yepnope */
//------------------------------------------------------------------
//
// Wait until the browser 'onload' is called before starting to load
// any external resources.  This is needed because a lot of JS code
// will want to refer to the HTML document.
//
//------------------------------------------------------------------
window.addEventListener('load', function() {
	console.log('Loading resources...');
	Modernizr.load([
		{
			load : [
				"preload!scripts/game.js",
				"preload!scripts/soundmanager.js",
				"preload!scripts/bulletmanager.js",
				"preload!scripts/particlemanager.js",
				"preload!styles/css.js",
				"preload!scripts/menumanager.js",
				"preload!scripts/highscores.js",
				"preload!scripts/monstermanager.js",
				"preload!scripts/renderer.js",
				"preload!scripts/towermanager.js",
				"preload!scripts/gamegrid.js",
				"preload!styles/creeps/blue/1.png",
				"preload!styles/creeps/blue/2.png",
				"preload!styles/creeps/blue/3.png",
				"preload!styles/creeps/blue/4.png",
				"preload!styles/creeps/blue/5.png",
				"preload!styles/creeps/blue/6.png",
				"preload!styles/creeps/green/1.png",
				"preload!styles/creeps/green/2.png",
				"preload!styles/creeps/green/3.png",
				"preload!styles/creeps/green/4.png",
				"preload!styles/creeps/green/5.png",
				"preload!styles/creeps/green/6.png",
				"preload!styles/creeps/red/1.png",
				"preload!styles/creeps/red/2.png",
				"preload!styles/creeps/red/3.png",
				"preload!styles/creeps/red/4.png",
				"preload!styles/creeps/red2/1.png",
				"preload!styles/creeps/red2/2.png",
				"preload!styles/creeps/red2/3.png",
				"preload!styles/creeps/red2/4.png",
				"preload!styles/Pictures/TowerStats/1-1.png",
				"preload!styles/Pictures/TowerStats/1-2.png",
				"preload!styles/Pictures/TowerStats/1-3.png",
				"preload!styles/Pictures/TowerStats/4-1.png",
				"preload!styles/Pictures/TowerStats/4-2.png",
				"preload!styles/Pictures/TowerStats/4-3.png",
				"preload!styles/Pictures/TowerStats/2-1.png",
				"preload!styles/Pictures/TowerStats/2-2.png",
				"preload!styles/Pictures/TowerStats/2-3.png",
				"preload!styles/Pictures/TowerStats/3-1.png",
				"preload!styles/Pictures/TowerStats/3-2.png",
				"preload!styles/Pictures/TowerStats/3-3.png",
				"preload!styles/Pictures/TowerStats/default.png",
				"preload!styles/Pictures/GameOver.png",
				"preload!styles/Pictures/Grid.png",
				"preload!styles/Pictures/Grid3.png",
				"preload!styles/Pictures/Menu.png",
				"preload!styles/Pictures/menu2.png",
				"preload!styles/Pictures/Towers.png",
				"preload!styles/Pictures/Towersinfo.png",
				"preload!styles/sounds/bleh/bleh0.wav",
				"preload!styles/sounds/bleh/bleh1.wav",
				"preload!styles/sounds/bleh/bleh2.wav",
				"preload!styles/sounds/bleh/bleh3.wav",
				"preload!styles/sounds/bleh/bleh4.wav",
				"preload!styles/sounds/bleh/bleh5.wav",
				"preload!styles/sounds/bleh/bleh6.wav",
				"preload!styles/sounds/bleh/bleh7.wav",
				"preload!styles/sounds/bleh/bleh8.wav",
				"preload!styles/sounds/bleh/bleh9.wav",
				"preload!styles/sounds/boom/boom0.wav",
				"preload!styles/sounds/boom/boom1.wav",
				"preload!styles/sounds/boom/boom2.wav",
				"preload!styles/sounds/boom/boom3.wav",
				"preload!styles/sounds/boom/boom4.wav",
				"preload!styles/sounds/boom/boom5.wav",
				"preload!styles/sounds/boom/boom6.wav",
				"preload!styles/sounds/boom/boom7.wav",
				"preload!styles/sounds/boom/boom8.wav",
				"preload!styles/sounds/boom/boom9.wav",
				"preload!styles/sounds/freeze/freeze0.wav",
				"preload!styles/sounds/freeze/freeze1.wav",
				"preload!styles/sounds/freeze/freeze2.wav",
				"preload!styles/sounds/freeze/freeze3.wav",
				"preload!styles/sounds/freeze/freeze4.wav",
				"preload!styles/sounds/freeze/freeze5.wav",
				"preload!styles/sounds/freeze/freeze6.wav",
				"preload!styles/sounds/freeze/freeze7.wav",
				"preload!styles/sounds/freeze/freeze8.wav",
				"preload!styles/sounds/freeze/freeze9.wav",
				"preload!styles/sounds/horray/horray0.wav",
				"preload!styles/sounds/horray/horray1.wav",
				"preload!styles/sounds/horray/horray2.wav",
				"preload!styles/sounds/horray/horray3.wav",
				"preload!styles/sounds/horray/horray4.wav",
				"preload!styles/sounds/horray/horray5.wav",
				"preload!styles/sounds/horray/horray6.wav",
				"preload!styles/sounds/horray/horray7.wav",
				"preload!styles/sounds/horray/horray8.wav",
				"preload!styles/sounds/horray/horray9.wav",
				"preload!styles/sounds/missile/missile0.wav",
				"preload!styles/sounds/missile/missile1.wav",
				"preload!styles/sounds/missile/missile2.wav",
				"preload!styles/sounds/missile/missile3.wav",
				"preload!styles/sounds/missile/missile4.wav",
				"preload!styles/sounds/missile/missile5.wav",
				"preload!styles/sounds/missile/missile6.wav",
				"preload!styles/sounds/missile/missile7.wav",
				"preload!styles/sounds/missile/missile8.wav",
				"preload!styles/sounds/missile/missile9.wav",
				"preload!styles/sounds/pew/pew0.wav",
				"preload!styles/sounds/pew/pew1.wav",
				"preload!styles/sounds/pew/pew2.wav",
				"preload!styles/sounds/pew/pew3.wav",
				"preload!styles/sounds/pew/pew4.wav",
				"preload!styles/sounds/pew/pew5.wav",
				"preload!styles/sounds/pew/pew6.wav",
				"preload!styles/sounds/pew/pew7.wav",
				"preload!styles/sounds/pew/pew8.wav",
				"preload!styles/sounds/pew/pew9.wav",
				"preload!styles/sounds/sell/sell0.wav",
				"preload!styles/sounds/sell/sell1.wav",
				"preload!styles/sounds/sell/sell2.wav",
				"preload!styles/sounds/sell/sell3.wav",
				"preload!styles/sounds/sell/sell4.wav",
				"preload!styles/sounds/sell/sell5.wav",
				"preload!styles/sounds/sell/sell6.wav",
				"preload!styles/sounds/sell/sell7.wav",
				"preload!styles/sounds/sell/sell8.wav",
				"preload!styles/sounds/sell/sell9.wav",
				"preload!styles/sounds/tower/tower0.wav",
				"preload!styles/sounds/tower/tower1.wav",
				"preload!styles/sounds/tower/tower2.wav",
				"preload!styles/sounds/tower/tower3.wav",
				"preload!styles/sounds/tower/tower4.wav",
				"preload!styles/sounds/tower/tower5.wav",
				"preload!styles/sounds/tower/tower6.wav",
				"preload!styles/sounds/tower/tower7.wav",
				"preload!styles/sounds/tower/tower8.wav",
				"preload!styles/sounds/tower/tower9.wav",
				"preload!styles/sounds/upgrade/upgrade0.wav",
				"preload!styles/sounds/upgrade/upgrade1.wav",
				"preload!styles/sounds/upgrade/upgrade2.wav",
				"preload!styles/sounds/upgrade/upgrade3.wav",
				"preload!styles/sounds/upgrade/upgrade4.wav",
				"preload!styles/sounds/upgrade/upgrade5.wav",
				"preload!styles/sounds/upgrade/upgrade6.wav",
				"preload!styles/sounds/upgrade/upgrade7.wav",
				"preload!styles/sounds/upgrade/upgrade8.wav",
				"preload!styles/sounds/upgrade/upgrade9.wav",
				"preload!styles/sounds/Music.mp3",
				"preload!styles/turrets/turret-1-1.png",
				"preload!styles/turrets/turret-1-2.png",
				"preload!styles/turrets/turret-1-3.png",
				"preload!styles/turrets/turret-2-1.png",
				"preload!styles/turrets/turret-2-2.png",
				"preload!styles/turrets/turret-2-3.png",
				"preload!styles/turrets/turret-3-1.png",
				"preload!styles/turrets/turret-3-2.png",
				"preload!styles/turrets/turret-3-3.png",
				"preload!styles/turrets/turret-4-1.png",
				"preload!styles/turrets/turret-4-2.png",
				"preload!styles/turrets/turret-4-3.png"

				
	
			],
			complete : function() {
				console.log('All files requested for loading...');
			}
		}
	]);
}, false);

//
// Extend yepnope with our own 'preload' prefix that...
// * Tracks how many have been requested to load
// * Tracks how many have been loaded
// * Places images into the 'images' object
yepnope.addPrefix('preload', function(resource) {
	console.log('preloading: ' + resource.url);

	MyGame.status.preloadRequest += 1;
	var isImage = /.+\.(jpg|png|gif)$/i.test(resource.url);
	resource.noexec = isImage;
	resource.autoCallback = function(e) {
		if (isImage) {
			var image = new Image();
			image.src = resource.url;
			MyGame.images[resource.url] = image;
			console.log('images: ' + JSON.stringify(MyGame.images));
		}
		MyGame.status.preloadComplete += 1;

		//
		// When everything has finished preloading, go ahead and start the game
		if (MyGame.status.preloadComplete === MyGame.status.preloadRequest) {
			console.log('Preloading complete!');
			MyGame.main.initialize();
		}
	};

	return resource;
});

//
// Extend yepnope with a 'preload-noexec' prefix that loads a script, but does not execute it.  This
// is expected to only be used for loading .js files.
yepnope.addPrefix('preload-noexec', function(resource) {
	console.log('preloading-noexec: ' + resource.url);
	resource.noexec = true;
	return resource;
});
