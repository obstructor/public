

CSTD.BulletManager = (function(){
	var that = {};
	that.Bullets = [];
	
	function Bullet(spec){
		var bullet = {
			position : spec.position,
			angle : spec.angle,
			speed : spec.speed,
			damage : spec.damage,
			range : spec.range,
			distance : 0,
			type : spec.type,
			dead : false,
			spawnrate : 100,
			spawn : 0,
			score : 'o',
			target : spec.target
			
		}
		
		bullet.Update = function(timer){
			bullet.distance += bullet.speed * timer;
			if(bullet.distance > bullet.range){
				return true;
			}
			bullet.spawn += timer;
			if(bullet.spawn > bullet.spawnrate && (bullet.type === 1 || bullet.type === 3 || bullet.type === 5|| bullet.type === 9|| bullet.type === 7|| bullet.type === 11)){
				bullet.spawn -= bullet.spawnrate;
				CSTD.ParticleManager.GenerateEmitter(2,0,this);
			}
			if(bullet.type === 3 || bullet.type === 7 || bullet.type === 11){
				var creep = CSTD.MonsterManager.GetMonsterCoord(bullet.target);
				var vector = {
					x: creep.x - bullet.position.x,
					y: creep.y - bullet.position.y
				}
				if(creep.x < 1000){			
					vector.x /= (Math.sqrt(vector.x * vector.x + vector.y * vector.y));
					vector.y /= (Math.sqrt(vector.x * vector.x + vector.y * vector.y));	
					
					bullet.position.x += vector.x * CSTD.timer * bullet.speed;
					bullet.position.y += vector.y * CSTD.timer * bullet.speed;
				}
				else return true;
				
			} else {
				bullet.position.x += Math.cos(bullet.angle) * CSTD.timer * bullet.speed;
				bullet.position.y += Math.sin(bullet.angle) * CSTD.timer * bullet.speed;
			}
			for(var i = 0; i < CSTD.MonsterManager.Monsters.length;++i){
				var coordinate = CSTD.MonsterManager.Monsters[i].position;
				if(bullet.position.x > coordinate.x - 15 && bullet.position.x < coordinate.x + 15){
					if(bullet.position.y > coordinate.y - 15 && bullet.position.y < coordinate.y + 15){
						switch(bullet.type){
							case 0:
								CSTD.MonsterManager.Monsters[i].health -= bullet.damage;
							break;
							case 1:
								if(CSTD.MonsterManager.Monsters[i].air === true)
									return false;
								for(var k = 0; k < CSTD.MonsterManager.Monsters.length;++k){
									if(Math.abs(CSTD.MonsterManager.Monsters[k].position.x - bullet.position.x) < 80 && Math.abs(CSTD.MonsterManager.Monsters[k].position.y - bullet.position.y) < 80){
										if(CSTD.MonsterManager.Monsters[k].air !== true)
											CSTD.MonsterManager.Monsters[k].health -= bullet.damage;
									}
								}
								CSTD.ParticleManager.GenerateEmitter(3,bullet.position, bullet);
							break;
							case 2:
							if(CSTD.MonsterManager.Monsters[i].air === true)
								return false;
							CSTD.MonsterManager.Monsters[i].slowtimer = 4000;
							CSTD.MonsterManager.Monsters[i].slow = bullet.damage;
							break;
							case 3:
							if(CSTD.MonsterManager.Monsters[i].air !== true)
								return false;
							CSTD.MonsterManager.Monsters[i].health -= bullet.damage;
							CSTD.ParticleManager.GenerateEmitter(5,bullet.position, bullet);
							break;
							case 4:
							CSTD.MonsterManager.Monsters[i].health -= bullet.damage;
							break;
							case 5:
							if(CSTD.MonsterManager.Monsters[i].air === true)
								return false;
							for(var k = 0; k < CSTD.MonsterManager.Monsters.length;++k){
									if(Math.abs(CSTD.MonsterManager.Monsters[k].position.x - bullet.position.x) < 80 && Math.abs(CSTD.MonsterManager.Monsters[k].position.y - bullet.position.y) < 80){
										if(CSTD.MonsterManager.Monsters[k].air !== true)
											CSTD.MonsterManager.Monsters[k].health -= bullet.damage;
									}
								}
								CSTD.ParticleManager.GenerateEmitter(3,bullet.position, bullet);
							break;
							case 6:
							if(CSTD.MonsterManager.Monsters[i].air === true)
								return false;
							CSTD.MonsterManager.Monsters[i].slowtimer = 6000;
							CSTD.MonsterManager.Monsters[i].slow = bullet.damage;
							break;
							case 7:
							if(CSTD.MonsterManager.Monsters[i].air !== true)
								return false;
							CSTD.MonsterManager.Monsters[i].health -= bullet.damage;
							break;
							case 8:
							CSTD.MonsterManager.Monsters[i].health -= bullet.damage;
							break;
							case 9:
							if(CSTD.MonsterManager.Monsters[i].air === true)
								return false;
							for(var k = 0; k < CSTD.MonsterManager.Monsters.length;++k){
									if(Math.abs(CSTD.MonsterManager.Monsters[k].position.x - bullet.position.x) < 80 && Math.abs(CSTD.MonsterManager.Monsters[k].position.y - bullet.position.y) < 80){
										if(CSTD.MonsterManager.Monsters[k].air !== true)
										CSTD.MonsterManager.Monsters[k].health -= bullet.damage;
									}
								}
								CSTD.ParticleManager.GenerateEmitter(3,bullet.position, bullet);
							break;
							case 10:
							if(CSTD.MonsterManager.Monsters[i].air === true)
								return false;
							CSTD.MonsterManager.Monsters[i].slowtimer = 8000;
							CSTD.MonsterManager.Monsters[i].slow = bullet.damage;
							break;
							case 11:
							if(CSTD.MonsterManager.Monsters[i].air !== true)
								return false;
							CSTD.MonsterManager.Monsters[i].health -= bullet.damage;
							break;
						}
						
						return true;
					}
				}
			}
			if(bullet.position.x > 800 || bullet.position.x < 0 || bullet.y > 600 || bullet.y < 0){
				return true;
			}
			return false;
		}
		return bullet;
	}
	
	that.SpawnBullet = function(tower, position, creep){
		that.Bullets.push(Bullet(
			{
				position : {x: position.x + 20, y: position.y + 20},
				angle : tower.angle,
				speed : tower.speed,
				damage : tower.damage,
				range : tower.radius,
				type : tower.type,
				target : creep
			}
		));
		switch(tower.type){
			case 0:
			
			break;
			case 1:
			CSTD.ParticleManager.GenerateEmitter(2,0,that.Bullets[that.Bullets.length -1]);
			break;
			case 2:
			break;
			case 3:
			CSTD.ParticleManager.GenerateEmitter(4,0,that.Bullets[that.Bullets.length -1]);
			break;
		}
	}
	
	that.Update = function(timer){
		for(var i = 0; i < that.Bullets.length;++i){
			if(that.Bullets[i].Update(timer)){
				that.Bullets[i].dead = true;
				that.Bullets.splice(i,1);
			}
		}
	}
	
	return that;
}())