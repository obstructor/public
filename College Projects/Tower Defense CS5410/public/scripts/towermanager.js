

CSTD.TowerManager = (function(){
    
	
   var that = {
       placeTower : false,
       sell: false,
       upgrade: false,
	   towerindex : [],
	   oldselected: -1
   }
   
   var thex = -1;
   var they = -1;
   
    function checkGold(type){
        switch(type){
			case 0:
				return CSTD.money >= 5;
			break;
			case 1:
				return CSTD.money >= 15;
			break;
			case 2:
				return CSTD.money >= 10;
			break;
			case 3:
				return CSTD.money >= 5;
			break;
			case 4:
				return CSTD.money >= 10;
			break;
			case 5:
				return CSTD.money >= 30;
			break;
			case 6:
				return CSTD.money >= 20;
			break;
			case 7:
				return CSTD.money >= 10;
			break;
			case 8:
				return CSTD.money >= 15;
			break;
			case 9:
				return CSTD.money >= 45;
			break;
			case 10:
				return CSTD.money >= 30;
			break;
			case 11:
				return CSTD.money >= 15;
			break;
		}
    }
   
   function AddTower(type, coord){
	   if(!checkGold(type)){
		   return false;
	   }
	   if(!(CSTD.GridManager.isSolid(coord)) && type >= 0 && type <= 3){
		   CSTD.GridManager.Solid(coord);
		   if (CSTD.GridManager.UpdateGrid()){
			   if(!GenerateTower(type)){
				   CSTD.GridManager.notSolid(coord);
				   CSTD.GridManager.UpdateGrid();
				   return false;
			   }
               else{
				   CSTD.grid[coord.x][coord.y].tower = GenerateTower(type);
				   CSTD.money -= CSTD.grid[coord.x][coord.y].tower.gold;
                        CSTD.TowerManager.placeTower = false;
                        CSTD.grid[coord.x][coord.y].towersolid = true;
						that.towerindex.push(coord);
                        CSTD.Stats.clear();
						that.oldselected = CSTD.selected;
						CSTD.selected = -2;
						CSTD.SoundManager.tower();
				        return true;
			   }
		   }
		   else {
			   CSTD.GridManager.notSolid(coord);
			   CSTD.GridManager.UpdateGrid();
			   return false;
		   }
	   }
	   return false;
   }
   
   function KeycodeMaker(Keycode){
	   return {
		   keyCode : Keycode,
		   shift : CSTD.shift,
		   ctrl : CSTD.ctrl,
		   alt : CSTD.alt
	   }
   }
   
   
   that.Keyboard = function(keycode){
	   if(CSTD.listening >= 0 && keycode.keyCode != 18 && keycode.keyCode != 16 && keycode.keyCode != 17){
		   CSTD.keycodes[CSTD.listening] = KeycodeMaker(keycode.keyCode);
		   console.log(CSTD.listening + " " + keycode.keyCode);
		   CSTD.UpdateKeyCodes();
		   CSTD.listening = -1;
	   }
	   
	   if(keycode.keyCode === CSTD.keycodes[0].keyCode && CSTD.keycodes[0].shift === CSTD.shift && CSTD.keycodes[0].alt === CSTD.alt && CSTD.keycodes[0].ctrl === CSTD.ctrl){
		   that.sell = true;
	   }
	   if(keycode.keyCode === CSTD.keycodes[1].keyCode && CSTD.keycodes[1].shift === CSTD.shift && CSTD.keycodes[1].alt === CSTD.alt && CSTD.keycodes[1].ctrl === CSTD.ctrl){
		   that.upgrade = true;
	   }
	   if(keycode.keyCode === CSTD.keycodes[2].keyCode && CSTD.keycodes[2].shift === CSTD.shift && CSTD.keycodes[2].alt === CSTD.alt && CSTD.keycodes[2].ctrl === CSTD.ctrl){
		   CSTD.ScreenManager.StartLevel();
	   }
	   if(keycode.keyCode === 9){
		   that.placeTower = true;
		   CSTD.selected = that.oldselected;
	   }
	   if(keycode.keyCode === 18){
		   CSTD.alt = true;
	   }
	   if(keycode.keyCode === 16){
		   CSTD.shift = true;
	   }
	   if(keycode.keyCode === 17){
		   CSTD.ctrl = true;
	   }
   }
   
   that.KeyboardUp = function(keycode){
	   if(keycode.keyCode === 18){
		   CSTD.alt = false;
	   }
	   if(keycode.keyCode === 16){
		   CSTD.shift = false;
	   }
	   if(keycode.keyCode === 17){
		   CSTD.ctrl = false;
	   }
   }
   
	that.Input = function(mouse){
        if(CSTD.TowerManager.placeTower === true){
            var click = {};
            click.x = Math.trunc(mouse.clientX / (800 / CSTD.constants.gridwidth));
            click.y = Math.trunc(mouse.clientY / (600 / CSTD.constants.gridheight));
            if(click.x >= 20 || click.y >= 15){
                return;
            }
            else{
                AddTower(CSTD.selected, click); 
            } 
        }
	}; 
    
    
   that.Select = function(type){
       CSTD.TowerManager.placeTower = true;
	   switch(type){
		   case 'ground1-1':
		   if(CSTD.selected === 0){
			   CSTD.selected = -1;
		   }
		   else CSTD.selected = 0;
		   break;
		   case 'ground2-1':
		   if(CSTD.selected === 1){
			   CSTD.selected = -1;
		   }
		   else CSTD.selected = 1;
		   break;
		   case 'mixed-1':
		   if(CSTD.selected === 2){
			   CSTD.selected = -1;
		   }
		   else CSTD.selected = 2;
		   break;
		   case 'air-1':
		   if(CSTD.selected === 3){
			   CSTD.selected = -1;
		   }
		   else CSTD.selected = 3;
		   break;
            case 'ground1-2':
		   if(CSTD.selected === 4){
			   CSTD.selected = -1;
		   }
		   else CSTD.selected = 4;
		   break;
		   case 'ground2-2':
		   if(CSTD.selected === 5){
			   CSTD.selected = -1;
		   }
		   else CSTD.selected = 5;
		   break;
		   case 'mixed-2':
		   if(CSTD.selected === 6){
			   CSTD.selected = -1;
		   }
		   else CSTD.selected = 6;
		   break;
		   case 'air-2':
		   if(CSTD.selected === 7){
			   CSTD.selected = -1;
		   }
		   else CSTD.selected = 7;
		   break;
            case 'ground1-3':
		  if(CSTD.selected === 8){
			   CSTD.selected = -1;
		   }
		   else  CSTD.selected = 8;
		   break;
		   case 'ground2-3':
		   if(CSTD.selected === 9){
			   CSTD.selected = -1;
		   }
		   else CSTD.selected = 9;
		   break;
		   case 'mixed-3':
		  if(CSTD.selected === 10){
			   CSTD.selected = -1;
		   }
		   else  CSTD.selected = 10;
		   break;
		   case 'air-3':
		  if(CSTD.selected === 11){
			   CSTD.selected = -1;
		   }
		   else  CSTD.selected = 11;
		   break;
           case 'none':
           if(CSTD.selected === -1){
			   CSTD.selected = -1;
		   }
		   else CSTD.selected = -1;
           break;
	   }
   }
   
   
    that.Update = function(){
        SellTower();
        UpgradeTower();
		for(var i = 0; i < that.towerindex.length; ++i){
			CSTD.grid[that.towerindex[i].x][that.towerindex[i].y].tower.Update(CSTD.timer, {x: that.towerindex[i].x * 40, y: that.towerindex[i].y * 40});
		}
    }; 
    
    
    that.render = function(){
        //Give info to render
       
    }
  
    that.EoR = function(){ // End of Round.
        // clear creep ids
    }
    
    that.addCreep = function(tower, id){
        CSTD.grid.tower.addcreep(id);
            
    }
    
    that.Initialize = function(){
	}
    
    
    var idcounter = 0;
    function GenerateTower(typer){
        
        var tower ={
             target: [],
			 creeps : [],
             firerate: 1000,
			 firecounter : 0,
             damage: 10,
             radius: 0,
			 angle : 0,
			 radiussquared : 0,
             gold: 0,
			 type : 0,
             rotate: .01,
			 speed : 0,
			 id : idcounter++,
			 PushCreep : function(id){
				 if((tower.type === 3 || tower.type === 7 || tower.type === 11) && CSTD.MonsterManager.GetMonsterAir(id) !== true){
					 return;
				 }
				 else if ((tower.type !== 3 && tower.type !== 7 && tower.type !== 11 && tower.type != 0 && tower.type != 4 && tower.type != 8) && CSTD.MonsterManager.GetMonsterAir(id) === true){
					 return;
				 }
				 else if(tower.creeps[id] === id){
					 return;
				 }
				 else{
					 tower.creeps[id] = id;
					 tower.target.push(id);
				 }
			 },
			 PopCreep : function(){
				 if(tower.target.length > 0){
					tower.creeps[tower.target[0]] = -1;
					tower.target.splice(0,1);
				 }
			 },
			 Update : function(timer, position){
				 tower.firecounter -= timer;
				 tower.Shoot(position);
			 },
			 Shoot : function(position){
				 if(tower.target.length > 0){
					 var smallest = {
						target : 9999,
						 value : 99999999999999999999999999999999999999};
					 for(var i =0; i < tower.target.length; ++i){
						var coordinate = CSTD.MonsterManager.GetMonsterCoord(tower.target[i]);
						coordinate.x -= position.x + 20;
						coordinate.y -= position.y + 20;
						var value = (coordinate.x) * (coordinate.x) + (coordinate.y) * (coordinate.y);
						if(value < tower.radiussquared){
							if(value < smallest.value){
								smallest.value = value; smallest.target = i;
							}
						}
						else {
							i--;
							tower.PopCreep();
						}
					 }	
						
							coordinate = CSTD.MonsterManager.GetMonsterCoord(tower.target[smallest.target]);
							var firedirection = {x: coordinate.x - position.x - 20, y: coordinate.y - position.y - 20};
							var radians = Math.atan2(firedirection.y,firedirection.x);
							
							if(Math.abs(tower.angle - radians) > Math.PI / 256){
								if(tower.angle > radians){
									tower.angle -= tower.rotate * CSTD.timer;
								}
								else{
									tower.angle += tower.rotate * CSTD.timer;
								}
							}
								if(Math.abs((tower.angle - radians))< Math.PI / 4){
									if(tower.firecounter < 0){

										if(tower.type === 0 || tower.type === 4 || tower.type === 8){
											CSTD.SoundManager.pew();
										}
										if(tower.type === 1 || tower.type === 5 || tower.type === 9){
											CSTD.SoundManager.boom();
										}
										if(tower.type === 2 || tower.type === 6 || tower.type === 10){
											CSTD.SoundManager.freeze();
										}
										if(tower.type === 3 || tower.type === 7 || tower.type === 11){
											CSTD.SoundManager.missile();
										}
										tower.firecounter = tower.firerate;
										CSTD.BulletManager.SpawnBullet(this, position, tower.target[smallest.target]);
									}
								}
				 
				 	}
				 }
             
         };
		
        switch(typer){
    
            case 0: // Ground 1-1
				tower.speed = .8;
				tower.damage = 10;
                tower.gold = 5;
                tower.radius = 80;
				tower.type = typer;
            break;
            case 1: //ground 2-1
			tower.damage = 2;
				tower.speed = .2;
                tower.gold = 15;
                tower.radius = 80;
				tower.type = typer;
            break;
            case 2: //mixed 1-1
				tower.speed = .8;
				tower.damage = .5;
                tower.gold = 10;
                tower.radius = 80;
				tower.type = typer;
            break;
            case 3: //air 1-1
				tower.damage = 20;
				tower.speed = .1;
                tower.gold = 5;
                tower.radius = 80;
				tower.type = typer;
            break;
                                                              
             
             case -1: //default
        
             break;
             
             
        }
		tower.radiussquared = tower.radius * tower.radius;
		return tower;
                
        
    }
       //upgrade function      
    
    function UpgradeTower(){
        if(CSTD.TowerManager.upgrade === true && thex >= 0){
			if(!(checkGold(CSTD.grid[thex][they].tower.type + 4))){
				return false;
			}
            if(CSTD.grid[thex][they].tower.type === 0){
				CSTD.SoundManager.upgrade();
				CSTD.grid[thex][they].tower.damage = 17;
                CSTD.grid[thex][they].tower.type = 4;
                CSTD.grid[thex][they].tower.radius = 100;
				CSTD.grid[thex][they].tower.gold = 10;
            }
            else if(CSTD.grid[thex][they].tower.type === 1){
				CSTD.SoundManager.upgrade();
				CSTD.grid[thex][they].tower.damage = 4;
                CSTD.grid[thex][they].tower.type = 5;
                CSTD.grid[thex][they].tower.radius = 100;
				CSTD.grid[thex][they].tower.gold = 30;
            }
            else if(CSTD.grid[thex][they].tower.type === 2){
				CSTD.SoundManager.upgrade();
				CSTD.grid[thex][they].tower.damage = .25;
                CSTD.grid[thex][they].tower.type = 6;
                CSTD.grid[thex][they].tower.radius = 100;
				CSTD.grid[thex][they].tower.gold = 20;
            }
            else if(CSTD.grid[thex][they].tower.type === 3){
				CSTD.SoundManager.upgrade();
				CSTD.grid[thex][they].tower.damage = 40;
                CSTD.grid[thex][they].tower.type = 7;
                CSTD.grid[thex][they].tower.radius = 100;
				CSTD.grid[thex][they].tower.gold = 10;
            }
            else if(CSTD.grid[thex][they].tower.type === 4){
				CSTD.SoundManager.upgrade();
				CSTD.grid[thex][they].tower.damage = 20;
                CSTD.grid[thex][they].tower.type = 8;
                CSTD.grid[thex][they].tower.radius = 120;
				CSTD.grid[thex][they].tower.gold = 15;
            }
            else if(CSTD.grid[thex][they].tower.type === 5){
				CSTD.SoundManager.upgrade();
				CSTD.grid[thex][they].tower.damage = 6;
                CSTD.grid[thex][they].tower.type = 9;
                CSTD.grid[thex][they].tower.radius = 120;
				CSTD.grid[thex][they].tower.gold = 45;
            }
            else if(CSTD.grid[thex][they].tower.type === 6){
				CSTD.SoundManager.upgrade();
				CSTD.grid[thex][they].tower.damage = 0;
                CSTD.grid[thex][they].tower.type = 10;
                CSTD.grid[thex][they].tower.radius = 120;
				CSTD.grid[thex][they].tower.gold = 30;
            }
            else if(CSTD.grid[thex][they].tower.type === 7){
				CSTD.SoundManager.upgrade();
				CSTD.grid[thex][they].tower.damage = 40;
                CSTD.grid[thex][they].tower.type = 11;
                CSTD.grid[thex][they].tower.radius = 200;
				CSTD.grid[thex][they].tower.gold = 15;
            }
			CSTD.money -= CSTD.grid[thex][they].tower.gold;
            CSTD.Stats.clear();
			CSTD.grid[thex][they].tower.radiussquared = CSTD.grid[thex][they].tower.radius * CSTD.grid[thex][they].tower.radius;
            thex = -1;
            they = -1;
			
		}
            CSTD.TowerManager.upgrade = false;
    }
                    
    
    //selling function
    
    function SellTower(){
        if(CSTD.TowerManager.sell === true && thex >= 0){
            CSTD.Renderer.clicksquare = false;
            CSTD.grid[thex][they] = CSTD.GridManager.GenerateSquare();
			for(var i = 0; i < that.towerindex.length;++i){
				if(that.towerindex[i] === CSTD.grid[thex][they].tower.id){
					that.towerindex.splice(i,1);
				}
			}
			CSTD.SoundManager.sell();
            CSTD.GridManager.UpdateGrid();
			CSTD.ParticleManager.GenerateEmitter(0,{x:thex, y : they}, 0);
            thex = -1;
            they = -1;
            CSTD.money += 1;
            CSTD.Stats.clear();
        }
		CSTD.TowerManager.sell = false;
    }
    
    
    
    
    
    //click listener function
    
    
    
    that.Clicked = function(mouse){
        
        CSTD.Renderer.clicksquare = false;
        var click = {};
        click.x = Math.trunc(mouse.clientX / (800 / CSTD.constants.gridwidth));
        click.y = Math.trunc(mouse.clientY / (600 / CSTD.constants.gridheight));
        if(click.x >= 20 || click.y >= 15){
                return;
            }
        CSTD.Stats.clear();
        if(CSTD.grid[click.x][click.y].selected === true){
            CSTD.Renderer.clicksquare = false;
            CSTD.grid[click.x][click.y].selected = false;
        }
        
        if(CSTD.grid[click.x][click.y].towersolid === false){
            CSTD.TowerManager.Input(mouse);
        }
        else{
            CSTD.Renderer.mouse = click;
            CSTD.Renderer.clicksquare = true;
            CSTD.grid[click.x][click.y].selected = true;
            thex = click.x;
            they = click.y;
            
        }

    }
    
	
	return that;
}());



    document.addEventListener('click', CSTD.TowerManager.Clicked);
	document.addEventListener('keydown', CSTD.TowerManager.Keyboard);
	document.addEventListener('keyup', CSTD.TowerManager.KeyboardUp);