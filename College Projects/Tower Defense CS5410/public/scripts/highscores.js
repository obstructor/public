/*jslint browser: true, white: true, plusplus: true */
/*global CSTD */

CSTD.ScoreManager = (function(){
	
	
	var that = {};
	var highscores = {};
	
	function UpdateHTML(){
		var htmlhighscores = document.getElementById('id-high-scores-list');
		htmlhighscores.innerHTML = "";
		for(var i = 0; i < 5; ++i){
			htmlhighscores.innerHTML += "<li>" + highscores[i] + "</li>";
		} 
	}
    
    function addScore(score) {

	$.ajax({
		url: 'http://localhost:3000/v1/high-scores?' + '&score=' + score,
		type: 'POST',
		error: function() { alert('POST failed'); },
		success: function() {
			showScores();
		}
	    });
    }
    function showScores() {
	$.ajax({
		url: 'http://localhost:3000/v1/high-scores',
		cache: false,
		type: 'GET',
		error: function() { alert('GET failed'); },
		success: function(data) {
			var list = $('#id-high-scores'),
				value,
				text;

			list.empty();
			for (value = 0; value < data.length; value++) {
			    HighScorePush(data[value]);
            }
		}
	});
}
    	
	function HighScorePush(){
        
            		
			localStorage.CSTD = JSON.stringify(highscores);
            
			UpdateHTML();
	}
	
	that.Pushscore= function(score){
        //addScore(score);
		for(var i = 0; i < 5;++i){
				if(score> highscores[i]){
					var temp = highscores[i];
					highscores[i] = score;
					score= temp;
				}
			}
			HighScorePush();
		}
	that.ResetScores = function(){
		highscores = [];
		for(var i = 0; i < 5;++i){highscores[i]=0;}
		HighScorePush();
	}
	that.Initialize = function(){
		if(localStorage.CSTD === undefined){
			CSTD.ScoreManager.ResetScores();
			localStorage.CSTD = JSON.stringify(highscores);
		}
		else{
			highscores = JSON.parse(localStorage.CSTD);
		}  
		HighScorePush();
	}
	return that;
}())

CSTD.ScoreManager.Initialize();