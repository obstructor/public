var CSTD = (function(){
    var that = {
        LevelManager : {},
        TowerManager : {},
        MonsterManager : {},
        RenderManager : {},
        GridManager: {},
        SoundManager: {},
		ScoreManager: {},
        money : 150,
        outofmoney : false,
        lives : 200,
        grid : [],
        score : 0,
		mouse : {x:0,y:0},
		selected : -1,
		timer : 0,
		difficulty : 0,
		keycodes : [],
        level : 1,
		shift : false,
		ctrl : false,
		alt : false,
		currentstate: 0,
    };
    
	that.easy = function(){
		CSTD.lives = 20;
        CSTD.Stats.background1();
		that.difficulty = 0;
		CSTD.ScreenManager.StartGame();
	}
	
	that.medium = function(){
		CSTD.lives = 30;
        CSTD.Stats.background1();
		that.difficulty = 1;
		CSTD.ScreenManager.StartGame();
	}
	
	
	that.hard = function(){
		CSTD.lives = 40;
        CSTD.Stats.background2();
		that.difficulty = 2;
		CSTD.ScreenManager.StartGame();
	}
	
    var timer = 0;
    that.constants = {
        gridwidth : 20,
        gridheight : 15
    }
    
    function Timer(timestamp){
        timer = timestamp - timer;
        if(timer < 0 || timer === undefined){
            timer = 0;
        }
        else if (!(timer < 30)){
            timer = 30;
        }
		CSTD.timer = timer;
    }
    
    that.GameStateChange = function(state){
        switch(state){
            case 0:
            CSTD.GameLoop = function(timestamp){ // Build Mode.
				currentstate = state;
                Timer(timestamp);
				CSTD.BulletManager.Update(timer);
				CSTD.ParticleManager.Update(timer);
                CSTD.TowerManager.Update(timer);
                CSTD.Renderer.Render();
				CSTD.Update();
                CSTD.RequestFrame(timestamp);
				// CSTD.selected = 1;
				//CSTD.TowerManager.placeTower = true;
            }
            break;
            case 1:
			CSTD.GameLoop = function(timestamp){
				currentstate = state;
				Timer(timestamp);
				CSTD.BulletManager.Update(timer);
				CSTD.TowerManager.Update(timer);
				CSTD.ParticleManager.Update(timer);
				CSTD.Update();
				CSTD.MonsterManager.Update();
				CSTD.Renderer.Render();
				CSTD.RequestFrame(timestamp);
			}
			break;
        }
    }
    
    that.RequestFrame = function(){
    return;    
    }
    
    that.Pause = function(){
        CSTD.RequestFrame = function(){return;};
    }
    
    that.Resume = function(){
        CSTD.RequestFrame = function(timestamp){
            timer = timestamp;
            requestAnimationFrame(CSTD.GameLoop);
            };
        CSTD.GameLoop();
    }
    
    that.Update = function(timestamp){
        Timer(timestamp);
        if(CSTD.lives <=0){
			CSTD.Pause();
			CSTD.ScreenManager.ShowScreen('gameover');
			CSTD.ScoreManager.Pushscore(CSTD.score);
		}
        //Update Creeps
        //Update Level Timer
    }
    
    that.GameLoop = function(timestamp){
        //Updates
        CSTD.Renderer.Render();
        CSTD.RequestFrame();
    }
	
	  function KeycodeMaker(Keycode){
	   return {
		   keyCode : Keycode,
		   shift : CSTD.shift,
		   ctrl : CSTD.ctrl,
		   alt : CSTD.alt
	   }
   }
	
	that.InitializeKeyCodes = function(){
		var temp = localStorage.getItem("CSTDkeycodes");
		if(temp != null){
			CSTD.keycodes = JSON.parse(temp);
		}
		else {
				CSTD.keycodes[0] = KeycodeMaker(83);
				CSTD.keycodes[1] = KeycodeMaker(85);
				CSTD.keycodes[2] = KeycodeMaker(71);
				localStorage.setItem("CSTDkeycodes", JSON.stringify(CSTD.keycodes));
		}
	}
	
	
	
	that.UpdateKeyCodes = function(){
		var modifiers = "";
		if(CSTD.keycodes[0].ctrl === true){
			modifiers += "ctrl - ";
		}
		if(CSTD.keycodes[0].shift === true){
			modifiers += "shift - ";
		}
		if(CSTD.keycodes[0].alt === true){
			modifiers += "alt - ";
		}
		document.getElementById("sell-key").innerHTML = modifiers + String.fromCharCode(CSTD.keycodes[0].keyCode);
		
		modifiers = "";
		if(CSTD.keycodes[1].ctrl === true){
			modifiers += "ctrl - ";
		}
		if(CSTD.keycodes[1].shift === true){
			modifiers += "shift - ";
		}
		if(CSTD.keycodes[1].alt === true){
			modifiers += "alt - ";
		}
		document.getElementById("upgrade-key").innerHTML = modifiers + String.fromCharCode(CSTD.keycodes[1].keyCode);
		
		modifiers = "";
		if(CSTD.keycodes[2].ctrl === true){
			modifiers += "ctrl - ";
		}
		if(CSTD.keycodes[2].shift === true){
			modifiers += "shift - ";
		}
		if(CSTD.keycodes[2].alt === true){
			modifiers += "alt - ";
		}
		document.getElementById("start-key").innerHTML = modifiers + String.fromCharCode(CSTD.keycodes[2].keyCode);
		localStorage.setItem("CSTDkeycodes", JSON.stringify(CSTD.keycodes));
	}
	
	that.Mouse = function(input){
		CSTD.mouse.x = input.clientX;
		CSTD.mouse.y = input.clientY;
	}
	
	that.Initialize = function(){
		timer = 0; CSTD.timer = 0;
		CSTD.ScoreManager.Initialize();
		CSTD.TowerManager.Initialize();
		CSTD.MonsterManager.Initialize();
		CSTD.GridManager.Initialize();
        CSTD.SoundManager.Initialize();
		CSTD.money = 150;
		CSTD.score = 0;
		CSTD.Resume();
		CSTD.GameStateChange(0);
		CSTD.GameLoop(performance.now());

	};
    
    return that;
}())

CSTD.Controls = function(num){
	switch(num){
		case 0:
			document.getElementById("sell-key").innerHTML = "...";
		break;
		case 1:
			document.getElementById("upgrade-key").innerHTML = "...";
		break;
		case 2:
			document.getElementById("start-key").innerHTML = "...";
		break;
	}
	console.log(num);
	CSTD.listening = num;
}

CSTD.InitializeKeyCodes();

CSTD.UpdateKeyCodes();

document.addEventListener('mousemove', CSTD.Mouse);