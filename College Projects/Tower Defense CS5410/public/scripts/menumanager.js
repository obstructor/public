CSTD.ScreenManager = (function(){
	var that = {};
	var divs = {};
	
	that.ReturnToMenu = function(){
		CSTD.ScreenManager.ShowScreen('main-menu');
	}
	
	that.Resume = function(){
		CSTD.Resume();
		CSTD.ScreenManager.ShowScreen('game-play');

	}
	
	that.StartLevel = function(){
		CSTD.GameStateChange(1);
	}
    
    that.Pause = function(){
		CSTD.Pause();
        CSTD.ScreenManager.ShowScreen('pause-screen');
    }
	
	that.ShowScreen = function(screentext){
		for(var property in divs){
			divs[property].classList.remove('active');
		}
		switch(screentext){
			case "about":
			divs.about.classList.add('active');
			break;
			case "help":
			divs.help.classList.add('active');
			break;
			case "high-scores":
			divs.highscore.classList.add('active');
			break;
			case "game-play":
			divs.game.classList.add('active');
			break;
			case"main-menu":
			divs.mainmenu.classList.add('active');
			break;
            case"pause-screen":
            divs.pause.classList.add('active');
			break;
            case"gameover":
            divs.gameover.classList.add('active');
			break;
			case"selection":
            divs.selection.classList.add('active');
			break;

        }
		return 0;
	} 
	
	that.StartGame = function(){
		CSTD.ScreenManager.ShowScreen('game-play');
		CSTD.Initialize();
	}
	
	that.Initialize = function(){
		divs.game = document.getElementById('game-play');
		divs.highscore = document.getElementById('high-scores');
		divs.mainmenu = document.getElementById('main-menu');
		divs.help = document.getElementById('help');
		divs.about = document.getElementById('about');
        divs.pause = document.getElementById('pause-screen');
        divs.gameover = document.getElementById('gameover');
		divs.selection = document.getElementById('selection');

		CSTD.ScreenManager.ReturnToMenu();
	}
	
	return that;
}());

CSTD.ScreenManager.Initialize();