

CSTD.MonsterManager = (function(){
	var that = {};
	
	that.Monsters = [];
		function switchstatement(quadrant, amazmonst){
			switch(quadrant){
				case 0:
					amazmonst.position.x += amazmonst.speed * amazmonst.slow *CSTD.timer / 1.41421;
					amazmonst.position.y += amazmonst.speed * amazmonst.slow *CSTD.timer / 1.41421;
					amazmonst.direction = 1/4*Math.PI;
				break;
				case 1:
					amazmonst.position.x += amazmonst.speed * amazmonst.slow *CSTD.timer;
					amazmonst.direction = 0;
				break;
				case 2:
					amazmonst.position.x += amazmonst.speed * amazmonst.slow *CSTD.timer / 1.41421;
					amazmonst.position.y -= amazmonst.speed * amazmonst.slow *CSTD.timer / 1.41421;
					amazmonst.direction = 7/4*Math.PI;
				break;
				case 3:
					amazmonst.position.y += amazmonst.speed * amazmonst.slow *CSTD.timer;
					amazmonst.direction = 1/2*Math.PI;
				break;
				case 4:
				console.log("oops");
				break;
				case 5:
					amazmonst.position.y -= amazmonst.speed * amazmonst.slow *CSTD.timer;
					amazmonst.direction = 3/2*Math.PI;
				break;
				case 6:
					amazmonst.position.x -= amazmonst.speed * amazmonst.slow *CSTD.timer / 1.41421;
					amazmonst.position.y += amazmonst.speed * amazmonst.slow *CSTD.timer / 1.41421;
					amazmonst.direction = 3/4*Math.PI;
				break;
				case 7:
					amazmonst.position.x -= amazmonst.speed * amazmonst.slow *CSTD.timer;
					amazmonst.direction = Math.PI;
				break;
				case 8:
						amazmonst.position.x -= amazmonst.speed * amazmonst.slow *CSTD.timer / 1.41421;
						amazmonst.position.y -= amazmonst.speed * amazmonst.slow *CSTD.timer / 1.41421;
						amazmonst.direction = 5/4*Math.PI;
				break;
			}
		}		
		var idcounter = 0;
		function GenerateMonster(typer, layerr){
			var amazmonst = {
				
				position : {x:0, y: 0}, // I guess this works.
				speed : .01,
				direction : 0,
				type : 0,
				state : 1,
				statetimer : 0,
				id : 0,
				health : 40,
				score : 0,
				money : 0,
				layer : layerr,
				slow : 1,
				slowtimer : 0,
				air : false
				};
				
				switch(typer){
					case 0:
						amazmonst.health = 40;
						amazmonst.score = 20;
						amazmonst.money = 1;
						amazmonst.speed = .01
					break;
					case 1:
						amazmonst.health = 40;
						amazmonst.score = 30;
						amazmonst.money = 1;
						amazmonst.speed = .011;
					break;
					case 2:
						amazmonst.health = 40;
						amazmonst.score = 40;
						amazmonst.money = 1;
						amazmonst.speed = .01;
						amazmonst.air = true;
					break;
					case 3:
						amazmonst.health = 40;
						amazmonst.score = 10;
						amazmonst.money = 1;
					break;
				}
				
				switch(layerr){
					case 0:
						amazmonst.position.x = 0
						amazmonst.position.y = (600 / CSTD.constants.gridheight) * CSTD.constants.gridheight / 2;
					break;
					case 1:
						amazmonst.position.x = (CSTD.constants.gridwidth * 40) - 2;
						amazmonst.position.y = (CSTD.constants.gridheight / 2) * 40;
					break;
					case 2:
						amazmonst.position.x = (CSTD.constants.gridwidth / 2) * 40 + 20;
						amazmonst.position.y = (CSTD.constants.gridheight) * 40 - 2
					break;
					case 3:
						amazmonst.position.x = (CSTD.constants.gridwidth / 2) * 40 + 20;
						amazmonst.position.y = 0;
					break;
				}
				
				amazmonst.id = idcounter++;
                amazmonst.type = typer;
				
			function Move(){
				var current = {};
				current.x = Math.trunc(amazmonst.position.x / (800 / CSTD.constants.gridwidth));
				current.y = Math.trunc(amazmonst.position.y / (600 / CSTD.constants.gridheight));
				var coordinate = {}; coordinate.x = current.x; coordinate.y = current.y;
				var flag = [];
				var quadrant = (function(){
					var tempx = Math.trunc((amazmonst.position.x % (800 / CSTD.constants.gridwidth) - 1)/ 13);
					var tempy = Math.trunc((amazmonst.position.y % (600 / CSTD.constants.gridheight) - 1) / 13);
					return tempy + tempx * 3;
				}())
				for(var x = -1; x <= 1;++x){
					for(var y = -1; y <= 1; ++y){
						var next = {}; next.x = current.x + x; next.y = current.y + y;
						flag.push(CSTD.GridManager.isNotSolid({x: current.x + x, y: current.y + y}) && CSTD.GridManager.DtoE(next, amazmonst.layer) < CSTD.GridManager.DtoE(current, amazmonst.layer));
					}
				}
				if(!(CSTD.timer > 0 && CSTD.timer <= 30)){
					return;
				}
				if(flag[0] && flag[1] && flag[3]){
					if(quadrant != 4 && quadrant != 0 && quadrant != 1 && quadrant != 3)
					{
						switchstatement(quadrant, amazmonst);
					}
					else{
						amazmonst.position.x -= amazmonst.speed * amazmonst.slow *CSTD.timer / 1.41421;
						amazmonst.position.y -= amazmonst.speed * amazmonst.slow *CSTD.timer / 1.41421;
						amazmonst.direction = 5/4*Math.PI;
					}
				}
				else if(flag[3] && flag[6] && flag[7]){
					if(quadrant != 4 && quadrant != 3 && quadrant != 6 && quadrant != 7)
					{
						switchstatement(quadrant, amazmonst);
					}
					else{
					amazmonst.position.x += amazmonst.speed * amazmonst.slow *CSTD.timer / 1.41421;
					amazmonst.position.y -= amazmonst.speed * amazmonst.slow *CSTD.timer / 1.41421;
					amazmonst.direction = 7/4*Math.PI;
					}
				}
				else if(flag[1] && flag[2] && flag[5]){
					if(quadrant != 4 && quadrant != 1 && quadrant != 2 && quadrant != 5)
					{
						switchstatement(quadrant, amazmonst);
					}
					else{
					amazmonst.position.x -= amazmonst.speed * amazmonst.slow *CSTD.timer / 1.41421;
					amazmonst.position.y += amazmonst.speed * amazmonst.slow *CSTD.timer / 1.41421;
					amazmonst.direction = 3/4*Math.PI;
					}
				}
				else if(flag[5] && flag[7] && flag[8]){
					if(quadrant != 4 && quadrant != 5 && quadrant != 7 && quadrant != 8)
					{
						switchstatement(quadrant, amazmonst);
					}
					else{
					amazmonst.position.x += amazmonst.speed * amazmonst.slow *CSTD.timer / 1.41421;
					amazmonst.position.y += amazmonst.speed * amazmonst.slow *CSTD.timer / 1.41421;
					amazmonst.direction = 1/4*Math.PI;
					}
				}
				
				else if(flag[1]){
					if(quadrant != 4 && quadrant != 1)
					{
						switchstatement(quadrant, amazmonst);
					}
					else{
					amazmonst.position.x -= amazmonst.speed * amazmonst.slow *CSTD.timer;
					amazmonst.direction = Math.PI;
					}
				}
				else if(flag[3]){
					if(quadrant != 4 && quadrant != 3)
					{
						switchstatement(quadrant, amazmonst);
					}
					else {
					amazmonst.position.y -= amazmonst.speed * amazmonst.slow *CSTD.timer;
					amazmonst.direction = 3/2*Math.PI;
					}
				}
				else if(flag[5]){
					if(quadrant != 4 && quadrant != 5)
					{
						switchstatement(quadrant, amazmonst);
					}
					else {
					amazmonst.position.y += amazmonst.speed * amazmonst.slow *CSTD.timer;
					amazmonst.direction = 1/2*Math.PI;
					}
				}
				else if(flag[7]){
					if(quadrant != 4 && quadrant != 7)
					{
						switchstatement(quadrant, amazmonst);
					}
					else {
					amazmonst.position.x += amazmonst.speed * amazmonst.slow *CSTD.timer;
					amazmonst.direction = 0;
					}
				}
				//console.log(flag[0] + " " + flag[1] + " " +flag[2] + " " +flag[3] + " " +flag[4] + " " +flag[5] + " " +flag[6] + " " +flag[7] + " " +flag[8] + " " +CSTD.timer*amazmonst.speed);
			}
			
				function FlyingMove(){
					switch(amazmonst.layer){
						case 0:
						amazmonst.direction = 0;
						amazmonst.position.x += amazmonst.speed * amazmonst.slow *CSTD.timer;
						break;
						case 1:
						amazmonst.direction = Math.PI;
						amazmonst.position.x -= amazmonst.speed * amazmonst.slow *CSTD.timer;
						break;
						case 2:
						amazmonst.direction = Math.PI / 2 + Math.PI;
						amazmonst.position.y -= amazmonst.speed * amazmonst.slow *CSTD.timer;
						break;
						case 3:
						amazmonst.direction = Math.PI / 2;
						amazmonst.position.y += amazmonst.speed * amazmonst.slow *CSTD.timer;
						break;
					}
				}
			
			function AtEnd(){
				switch(amazmonst.layer){
					case 0:
						if(amazmonst.position.x > (CSTD.constants.gridwidth - 1) * 40){
							CSTD.SoundManager.horray();
							return true;
						}
						else return false;
					break;
					case 1:
						if(amazmonst.position.x < 40){
							CSTD.SoundManager.horray();
							return true;
						}
						else return false;
					break;
					case 2:
						if(amazmonst.position.y < 40){
							CSTD.SoundManager.horray();
							return true;
						}
						else return false;
					break;
					case 3:
						if(amazmonst.position.y > (CSTD.constants.gridheight - 1) * 40){
							CSTD.SoundManager.horray();
							return true;
						}
						else return false;
					break;
				}
			}
			
			amazmonst.Update = function(){
				if(this.type != 2){
					Move();
				}
				else{
					FlyingMove();
				}
				amazmonst.slowtimer -= CSTD.timer;
				if(amazmonst.slowtimer < 0){
					amazmonst.slow = 1;
				}
				amazmonst.statetimer += CSTD.timer;	
				for(var i = 0; i < CSTD.TowerManager.towerindex.length;++i){
					var radiussquared = CSTD.grid[CSTD.TowerManager.towerindex[i].x][CSTD.TowerManager.towerindex[i].y].tower.radiussquared; 
					var distance = {
						x: (CSTD.TowerManager.towerindex[i].x * 40 + 20 - amazmonst.position.x),
						y: (CSTD.TowerManager.towerindex[i].y * 40 + 20 - amazmonst.position.y)
					}
					var distancesquared = {
						x: distance.x * distance.x,
						y: distance.y * distance.y
					}
					if(radiussquared > (distancesquared.x + distancesquared.y)){
						CSTD.grid[CSTD.TowerManager.towerindex[i].x][CSTD.TowerManager.towerindex[i].y].tower.PushCreep(amazmonst.id);
					}
				}
				
				if(amazmonst.statetimer > 250){
					amazmonst.statetimer = 0;
                    ++amazmonst.state
					if(amazmonst.state > 6  && amazmonst.type < 2){
						amazmonst.state = 1;
					}
                    else if(amazmonst.state > 4  && amazmonst.type >= 2){
						amazmonst.state = 1;
					}
				}
				
				return AtEnd();
			}
			return amazmonst;
		}
	
		that.Update = function(){
			that.StartLevel(CSTD.timer);
			if(that.Monsters.length < 1 && leveldone === true){
				level++;
				CSTD.level = level+1;
				levellist = [10 + Math.pow(2,level),10 +Math.pow(2,level),Math.pow(2,level),500,500,500,0,0,0,0,0,0];
				CSTD.GameStateChange(0);
			}
			that.Monsters.forEach(function(value,index,array){
				if(value.Update()){
					that.Monsters.splice(index,1);
					CSTD.lives--;
				}
				if(value.health <= 0){
					CSTD.ParticleManager.GenerateEmitter(1,value.position,value);
					CSTD.score += value.score;
					CSTD.money += value.money;
					CSTD.SoundManager.bleh();
					that.Monsters.splice(index,1);
				}
			})
		}
		
		// creep type 3, millisecond spawn rate, current spawned, timer
		var levellist = [1,1,10,500,500,500,0,0,0,0,0,0];
		
		var level = 0;
		var leveldone = false;
		var wave1 = false;
		var wave2 = false;
		var wave3 = false;
		
		that.StartLevel = function(timer){
			var layer = 0;
			if(CSTD.difficulty ===1){
				layer = Math.trunc((Math.random() * 10) % 2);
			}
			if(CSTD.difficulty === 2){
				layer = Math.trunc((Math.random() * 10) % 4); 
			}
			leveldone = true;
			if(levellist[0] > levellist[6]){
					levellist[9] += timer;
					leveldone = false;
					if(levellist[9] > levellist[3]){
						levellist[3] *= .999;
						levellist[9]-= levellist[3];
						levellist[6]++;
						that.SpawnMonsters(0,1, layer)
					}
				}
			if(levellist[1] > levellist[7]){
					levellist[10] += timer;
					leveldone = false;
					if(levellist[10] > levellist[4]){
						levellist[4] *= .999;
						levellist[10]-= levellist[4];
						levellist[7]++;
						that.SpawnMonsters(1,1, layer)
					}
				}
			if(levellist[2] > levellist[8]){
					levellist[11] += timer;
					leveldone = false;
					if(levellist[11] > levellist[5]){
						levellist[5] *= .999;
						levellist[8]++;
						levellist[11]-= levellist[5];
						that.SpawnMonsters(2,1, layer)
					}
				}
		}
		
		that.SpawnMonsters = function(type, amount, layer){
			for(var i = 0; i < amount;++i)
				that.Monsters.push(GenerateMonster(type, layer));
		}
        
        that.GetMonsterCoord = function(xid){
			for (var i = 0; i < that.Monsters.length;++i){
				if(that.Monsters[i].id === xid){
					return {
				x : that.Monsters[i].position.x,
				y : that.Monsters[i].position.y
					}
				}
			}
			return {
				x: 10000,
				y: 10000
					};
			
			};
			
			that.GetMonsterAir = function(xid){
			for (var i = 0; i < that.Monsters.length;++i){
				if(that.Monsters[i].id === xid){
					return that.Monsters[i].air;
				}
			}
			return false;
			
			};
        
		that.Initialize = function(){
			levellist = [10,1,0,5000,5000,5000,0,0,0,0,0,0];
			level = 0;
			leveldone = false;
			that.Monsters = [];
			return;
		}
	return that;
}());