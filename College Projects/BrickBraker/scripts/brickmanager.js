BrickBraker.BrickManager = (function(){
	var that = {};
	
	function GenerateBrick(spec){
		var that = {};
		that.solid = true;
		that.color = spec.color;
		that.stroke = spec.stroke;
		return that;
	}
	
	function GenerateSpace(){
		var that = {};
		that.solid = false;
		return that;
	}
	
	that.RemoveBrick = function(coords){
		if(BrickBraker.Bricks[coords.x][coords.y].solid === true){
		switch(coords.y){ // Score increase.
				case 0:
				case 1:
				case 2:
				case 3:
				break;
				case 4:
				case 5:
				BrickBraker.IncreaseScore(2);
				case 6:
				case 7:
				BrickBraker.IncreaseScore(1);
				case 8:
				case 9:
				BrickBraker.IncreaseScore(1);
				default:
				BrickBraker.IncreaseScore(1);
				break;
			}
			BrickBraker.Bricks[coords.x][coords.y].solid = false;
			BrickBraker.BrickCount[coords.y] -= 1;
			if(BrickBraker.BrickCount[coords.y] < 1){
				BrickBraker.IncreaseScore(25);
				var sum = 0;
				for(var i = 4; i < BrickBraker.BrickCount.length;++i){
					sum += BrickBraker.BrickCount[i];
				}
				if(sum < 1){
					BrickBraker.EndGame();
				}
			}
			return true;
		}
		else return false;
	}
	
	that.Initialize = function(){
		BrickWidth = 30; BrickHeight = 10;
		BrickBraker.Bricks.length = 0;
		for(var i = 0; i < 600 / BrickWidth;++i){
			BrickBraker.Bricks.push([]);
			var fillandstroke = { color : "#00FF00", stroke : "#000000"};
			for(var k = 0; k < 4; ++k){
				BrickBraker.Bricks[i].push(GenerateSpace());
			}
			for(var k = 0; k < 2;++k){
				BrickBraker.Bricks[i].push(GenerateBrick(fillandstroke));
			}
			fillandstroke.color = "#0000FF";
			for(var k = 0; k < 2;++k){
				BrickBraker.Bricks[i].push(GenerateBrick(fillandstroke));
			}
			fillandstroke.color = "#FF8000";
			for(var k = 0; k < 2;++k){
				BrickBraker.Bricks[i].push(GenerateBrick(fillandstroke));
			}
			fillandstroke.color = "#FFFF00";
			for(var k = 0; k < 2;++k){
				BrickBraker.Bricks[i].push(GenerateBrick(fillandstroke));
			}
		}
	}
return that;	
}())