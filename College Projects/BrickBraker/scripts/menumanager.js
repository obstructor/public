BrickBraker.ScreenManager = (function(){
	var that = {};
	var divs = {};
	
	that.ReturnToMenu = function(){
		BrickBraker.ScreenManager.ShowScreen('menu');
	}
	
	that.Resume = function(){
		BrickBraker.ScreenManager.ShowScreen('game-play');
		BrickBraker.Countdown.startplusrestore();
	}
	
	that.ShowScreen = function(screentext){
		for(var property in divs){
			divs[property].classList.remove('active');
		}
		switch(screentext){
			case "about":
			divs.about.classList.add('active');
			break;
			case "help":
			divs.help.classList.add('active');
			break;
			case "high-scores":
			divs.highscore.classList.add('active');
			break;
			case "game-play":
			divs.game.classList.add('active');
			BrickBraker.Resume();
			break;
			case"menu":
			divs.mainmenu.classList.add('active');
			break;
		}
	}
	
	that.StartGame = function(){
		BrickBraker.ScreenManager.ShowScreen('game-play');
		BrickBraker.Initialize();
	}
	
	that.Initialize = function(){
		divs.game = document.getElementById('game-play');
		divs.highscore = document.getElementById('high-scores');
		divs.mainmenu = document.getElementById('main-menu');
		divs.help = document.getElementById('help');
		divs.about = document.getElementById('about');
		
		document.getElementById('id-new-game').addEventListener('click',
			function() {BrickBraker.ScreenManager.ShowScreen('game-play'); });
		
		document.getElementById('id-high-scores').addEventListener('click',
			function() { BrickBraker.ScreenManager.ShowScreen('high-scores'); });
			
		document.getElementById('id-resume').addEventListener('click',
			function() { BrickBraker.ScreenManager.ShowScreen('game-play'); });
		
		document.getElementById('id-help').addEventListener('click',
			function() { BrickBraker.ScreenManager.ShowScreen('help'); });
		
		document.getElementById('id-about').addEventListener('click',
			function() { BrickBraker.ScreenManager.ShowScreen('about'); });
			
		document.getElementById('id-about-back').addEventListener('click',
			function() { BrickBraker.ReturnToMenu(); });
			
		document.getElementById('id-high-scores-back').addEventListener('click',
			function() { BrickBraker.ReturnToMenu(); });
			
		document.getElementById('id-help-back').addEventListener('click',
			function() { BrickBraker.ReturnToMenu(); });
			
		document.getElementById('id-about-back').addEventListener('click',
			function() { BrickBraker.ReturnToMenu(); });
			
		document.getElementById('id-game-play-back').addEventListener('click',
			function() { BrickBraker.ReturnToMenu(); });
			
		BrickBraker.ScreenManager.ReturnToMenu();
	}
	
	return that;
}());

BrickBraker.ScreenManager.Initialize();