

function Coordinate(){
	var that = {
		x : 0,
		y : 0
	}
	return that;
}

BrickBraker.Renderer = (function() {
	var that = {};
	
	var canvas = document.getElementById('canvas-main'),
		context = canvas.getContext('2d');

	function RenderBall(daball){
		context.beginPath(); // Copied from w3Schools.com
		context.arc(daball.position.x,daball.position.y,BallWidth / 2,0,2*Math.PI);
		context.fillStyle = "#FF0000"; context.fill();
		context.strokeStyle = "#00FF00"; context.stroke();
	};
	
	function RenderBricks(brick){
		BrickBraker.Bricks.forEach(function(value,index,array){
			value.forEach(function(lue,dex,ray){
				if(lue.solid){
					context.beginPath();
					context.moveTo(BrickWidth * index + 2, BrickHeight * dex + 2);
					context.lineTo(BrickWidth * index + 2,  BrickHeight * dex + BrickHeight - 2);
					context.lineTo(BrickWidth * index + BrickWidth - 2,  BrickHeight * dex + BrickHeight - 2);
					context.lineTo(BrickWidth * index + BrickWidth - 2, BrickHeight * dex + 2);
					context.closePath();
				
					context.fillStyle = lue.color;
					context.fill();
					
					context.strokeStyle = "#000000";
					context.lineWidth = 1;
					context.stroke();
				}
			})
		});
		
	}

	
	function clear() {
		context.setTransform(1, 0, 0, 1, 0, 0);
		context.clearRect(0, 0, canvas.width, canvas.height);
		}
	
	function RenderBalls(){
		var currentball = BrickBraker.BallManager.GetBall(0);
		
		for(var counter = 1; counter <= currentball.max; ++counter){
			RenderBall(currentball);
			currentball = BrickBraker.BallManager.GetBall(counter);
		}
		
	}
	
	function RenderPaddle(){
		context.beginPath();
			context.moveTo(BrickBraker.Paddle.x - BrickBraker.Paddle.width / 2, BrickBraker.Paddle.height);
			context.lineTo(BrickBraker.Paddle.x - BrickBraker.Paddle.width / 2,  BrickBraker.Paddle.height + 10);
			context.lineTo(BrickBraker.Paddle.x + BrickBraker.Paddle.width / 2,  BrickBraker.Paddle.height + 10);
			context.lineTo(BrickBraker.Paddle.x + BrickBraker.Paddle.width / 2, BrickBraker.Paddle.height);
			context.closePath();
				
			context.fillStyle = "#FF00FF";
			context.fill();
				
			context.strokeStyle = "#00FFFF";
			context.lineWidth = 1;
			context.stroke();
	}
	
	function RenderLives(){
		for(var i = 0; i < BrickBraker.Lives; ++i){
			context.moveTo(0 + i * 30, 588);
			context.lineTo(0 + i * 30,  595);
			context.lineTo(0 + (i + 1) * 30 - 5,  595);
			context.lineTo(0 + (i + 1) * 30 - 5, 588);
			context.closePath();
				
			context.fillStyle = "#FF00FF";
			context.fill();
				
			context.strokeStyle = "#00FFFF";
			context.lineWidth = 1;
			context.stroke();
		}
	}
	
	function RenderScore(){
		context.font = "20px Arial";
		context.fillText("Score: " + BrickBraker.Score.toString(), canvas.width - 150, canvas.height);
	}
	
	function RenderParticles(){
		var Particles = [];
		Particles = BrickBraker.ParticleManager.RenderInformation();
		Particles.forEach(function(value,index,array){
			context.moveTo(value.x,value.y + 2);
			context.lineTo(value.x - 2, value.y);
			context.lineTo(value.x, value.y - 2);
			context.lineTo(value.x + 2, value.y);
			context.closePath();
			
			context.fillStyle =  "#FFFFFF";
			context.fill();
			
			context.strokeStyle = "#c0c0c0";
			context.lineWidth = 1;
			context.stroke();
		});
	}
		
	that.Render = function(){
		clear();
		RenderParticles();
		RenderBalls();
		RenderBricks();
		RenderPaddle();
		RenderLives();
		RenderScore();
	}

	that.RenderNumber = function(Number){
		context.font = "50px Arial";
		context.fillText(Number.toString(), canvas.width / 2, canvas.height / 2);
	}
	
	that.Initialize = function(){
		canvas = document.getElementById('canvas-main'),
		context = canvas.getContext('2d');
	}
	
	return that;
}());