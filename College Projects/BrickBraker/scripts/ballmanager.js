BrickBraker.BallManager = (function(){
	var that = {},
	balls = [];
	
	function CheckCollisionBrick(Coordinate, remove){
		var temp = {x : 0 , y : 0};
		temp.x = Math.trunc(Coordinate.x / BrickWidth);
		temp.y = Math.trunc(Coordinate.y / BrickHeight);
		if(temp.x < 0 || temp.y < 0 ){
			return false;
		}
		if(BrickBraker.Bricks.length > temp.x && temp.y < BrickBraker.Bricks[0].length){
			if(BrickBraker.Bricks[temp.x][temp.y].solid ===true){
				remove.push({x: temp.x, y:temp.y})
				return true;
			}
		}
		return false;
	};
	
	function CheckCollisionPaddle(bottom){
		if(bottom.y > BrickBraker.Paddle.height){
			if(bottom.x > BrickBraker.Paddle.x - BrickBraker.Paddle.width / 2 && bottom.x < BrickBraker.Paddle.x + BrickBraker.Paddle.width / 2)
			return true;
		}
		return false;
	}
	
	function CheckCollision(ball){
		var top = Coordinate();  var right = Coordinate();  var bottom = Coordinate();  var left = Coordinate();
		top.y = (ball.y - BallHeight/2); top.x = ball.x;
		right.y = ball.y; right.x = ball.x + BallWidth / 2;
		left.y = ball.y; left.x = ball.x - BallWidth / 2;
		bottom.y = (ball.y + BallHeight/2); bottom.x = ball.x;
	
		var flagx, flagy;
		var brickstoremove = [];
		
		flagy = CheckCollisionBrick(top,brickstoremove) || CheckCollisionBrick(bottom,brickstoremove);
		flagx = CheckCollisionBrick(right,brickstoremove) || CheckCollisionBrick(left,brickstoremove);
		
		if(right.x > 600){
			flagx = true; ball.x = 599 - BallWidth / 2;
		}
		if(left.x < 0){
			flagx = true; ball.x = 1 + BallWidth / 2;
		}
		
		if(top.y < 0){
			ball.y = 1 + BallHeight / 2;
			flagy = true;
		}
		if(CheckCollisionPaddle(bottom)){
			flagy = true;
			ball.y = BrickBraker.Paddle.height - BallHeight / 2;
		}
		
		if(ball.kills + brickstoremove.length >= 4 && ball.kills < 4){
			ball.speed += .25;
		}
		if(ball.kills + brickstoremove.length >= 12 && ball.kills < 12){
			ball.speed += .25;
		}
		if(ball.kills + brickstoremove.length >= 36 && ball.kills < 36){
			ball.speed += .25;
		}
		if(ball.kills + brickstoremove.length >= 62 && ball.kills < 62){
			ball.speed += .25;
		}
		ball.kills += brickstoremove.length;
		brickstoremove.forEach(function(value,index,array){
			BrickBraker.BrickManager.RemoveBrick(value);
			BrickBraker.ParticleManager.GenerateEmitter(0,value);
			if(value.y === 4){
				BrickBraker.Paddle.width = 50;
			}
		});
		
		brickstoremove.length = 0;
		return {
			y : flagy,
			x : flagx
		}
	};
	
	function GenerateBall(){
		var positionpaddle = BrickBraker.Paddle.x;
		return {
				direction : {x : .15, y : -.60},
				speed : .5,
				kills : 0,
				position : {x: positionpaddle, y: BrickBraker.Paddle.height - BallHeight / 2},
				}
	}
	
	that.Initialize = function(){
		balls.length = 0;
		var ball = GenerateBall();
		balls.push(ball);	
		};
	
	that.addball = function(specificball){
		if(specificball !== undefined){
			balls.push(specificball);
		}
		else {
			balls.push(GenerateBall());
		}
	};
	
	that.update = function(elapsedtime){
		if(elapsedtime > 0){
			for(var count = 0; count < balls.length; ++count){
					balls[count].position.x = balls[count].position.x + balls[count].direction.x * elapsedtime * balls[count].speed;
					balls[count].position.y = balls[count].position.y + balls[count].direction.y * elapsedtime * balls[count].speed;
					
					var collision = CheckCollision(balls[count].position);
					if(collision.x){
						balls[count].direction.x *= -1;
					}
					if(collision.y){
						balls[count].direction.y *= -1;
					}
					if(balls[count].position.y > 600){
						balls.splice(count,1);
						if(balls.length === 0) {
							if(--BrickBraker.Lives < 0){
								BrickBraker.EndGame();
							}
							else BrickBraker.PaddleDeath();
						}
						
					}
					
				}
		}
	};
	
	that.GetBall = function(ballnumber){
			ballnumber = Math.trunc(ballnumber);
			if(ballnumber < balls.length && ballnumber >= 0){
				return {
					position :balls[ballnumber].position,
					max : balls.length
					}
			}
			return {
				position : {x : -1, y : -1},
				max : balls.length
			}
	};
	
	return that;
	
}());
