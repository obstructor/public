
var BrickWidth, BrickHeight;
var BallWidth, BallHeight;



var BrickBraker = {
	Bricks : [[]],
	BrickCount : [],
	Paddle : {
		x: 300,
		width : 100,
		height : 570,
		direction: 0
	},
	Score : 0,
	Lives : 3,
	IncreaseScore : function(value){
		if(this.Score % 100 + value >= 100){
			BrickBraker.BallManager.addball();
		}
		this.Score += value;
	},
	RequestAnimation : function(timestamp){
		return; //Uninitialized games shouldn't continue running.
	},
	TimerHandle: function(timestamp){
		if(timer > 30)
			timer = 30;
		if(timer === undefined){
			timer = 0;
		}
	},
	ReturnToMenu : function(){
		BrickBraker.RequestAnimation = function(timestamp){return;};
		BrickBraker.ScreenManager.ReturnToMenu();
	},
	Resume: function(){
		return; //Uninitialized games can't be resumed.
	},
	Pause : function(){
		BrickBraker.RequestAnimation = function(timestamp){return;};
	},
	Initialize : function(){
		BrickBraker.RequestAnimation = function(){return;}
		BallWidth = 15; BallHeight = 15; BrickBraker.Lives = 3; BrickBraker.Paddle.width = 100; BrickBraker.Score = 0; BrickBraker.Paddle.x = 300;
		BrickBraker.BallManager.Initialize();
		BrickBraker.Renderer.Initialize();
		BrickBraker.BrickManager.Initialize();
		this.BrickCount.length = this.Bricks[0].length;
		for(var i = 0; i < this.Bricks[0].length;++i){
				this.BrickCount[i]=(600 / BrickWidth);
			}
		timer = performance.now();
		BrickBraker.Resume = function(){BrickBraker.RequestAnimation = function(timestamp){timer = timestamp;requestAnimationFrame(BrickBraker.GameLoop); return true;}}
		BrickBraker.Countdown.startplusrestore();
	},
	EndGame : function(){
		BrickBraker.RequestAnimation = function(timestamp){return false;}
		BrickBraker.ScoreManager.PushScore(BrickBraker.Score);
		BrickBraker.ScreenManager.ShowScreen('high-scores');
	},
	PaddleDeath : function(){
		BrickBraker.RequestAnimation = function(timestamp){return false;}
		BrickBraker.Paddle.x = 300;
		BrickBraker.BallManager.Initialize();
		BrickBraker.Countdown.startplusrestore();
	},
	UpdatePaddle : function(time){
		BrickBraker.Paddle.x = BrickBraker.Paddle.x + BrickBraker.Paddle.direction * time;
		if(BrickBraker.Paddle.x > 600)
			BrickBraker.Paddle.x = 600;
		if(BrickBraker.Paddle.x < 0)
			BrickBraker.Paddle.x = 0;
	},
	Update : function(timestamp){
		BrickBraker.TimerHandle(timestamp);
		BrickBraker.BallManager.update(timer);
		BrickBraker.UpdatePaddle(timer);
		BrickBraker.ParticleManager.Update(timer);
},
	GameLoop : function(timestamp){
	BrickBraker.Renderer.Render();
	BrickBraker.Update(timestamp);
	BrickBraker.RequestAnimation(timestamp);
	},
	BallManager : {},
	Renderer : {},
	Countdown : {},
	BrickManager : {},
	ScoreManager : {},
	ParticleManager : {}
	
};

BrickBraker.MovePaddleLeft = function(){
	BrickBraker.Paddle.direction = -.50;
}

BrickBraker.MovePaddleRight = function(){
	BrickBraker.Paddle.direction = .50;
}

BrickBraker.StopPaddle = function(){
	BrickBraker.Paddle.direction = 0;
}



var timer = performance.now();

