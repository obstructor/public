BrickBraker.Countdown = (function(){
	var that = {};
	var timer;
	var running = false;;
	var restoreflag = false;
	function run(timestamp){
		
			BrickBraker.Renderer.Render();
			if(timestamp - timer > 2000){
				BrickBraker.Renderer.RenderNumber(1);
			}
			else if(timestamp - timer > 1000){
				BrickBraker.Renderer.RenderNumber(2);
			}
			else{
				BrickBraker.Renderer.RenderNumber(3);
			}
			if(timestamp - timer < 3000){
				requestAnimationFrame(run)
				}
			else if(restoreflag === false){
				running = false;
				BrickBraker.GameLoop(timestamp);
			}
			else if (restoreflag === true){
				running = false;
				BrickBraker.Resume();
				BrickBraker.GameLoop(timestamp);
			}
	}
	
	that.start = function(){
		if(running === true)
		return false;
		else{
			running = true; restoreflag = false;
			timer = performance.now();
			run(timer);
		}
	}
	that.startplusrestore = function(){
		if(running === true)
		return false;
		else{
			running = true; restoreflag = true;
			timer = performance.now();
			run(timer);
		}
	}
	
	return that;
}())