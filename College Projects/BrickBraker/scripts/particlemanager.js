BrickBraker.ParticleManager = (function(){
	var that = {};
	that.emitters = [];
	
	function GenerateRenderInformation(particle){
		var temp = {};
		temp.x = particle.position.x;
		temp.y = particle.position.y;
		return temp;
		}

	function GenerateGravityParticle(spec){
		var temp = {
			position : {}
		};
		temp.gravity = spec.gravityvalue;
		temp.position.x = spec.x;
		temp.position.y = spec.y;
		temp.lifespan = spec.lifespan;
		temp.color = spec.color;
		temp.update = function(time){
			this.position.y += this.gravity * time * -.01;
			this.lifespan -= time;
			if(this.lifespan < 0){
				return true;
			}
			else return false;
		}
		return temp;
	}

	function GenerateBrickEmitter(coord){
		var temp = {};
		temp.Particles = [];
		
		temp.update = function(time){
			for(var i = 0; i < this.Particles.length;++i){
				if(this.Particles[i].update(time)){
					this.Particles.splice(i,1);
				}
			}
			if(this.Particles.length < 1){
				return true;
			}
			else{
				return false;
			}
		}
		
		var partspec = {
				x : 0,
				y : 0,
				gravityvalue : 0,
				lifespan : 0,
			};
			
		
		for(var l = 0; l < 25;++l){
			
			partspec.x = coord.x * BrickWidth + Math.random() * BrickWidth;
			partspec.y = coord.y * BrickHeight + Math.random() * BrickHeight;
			partspec.gravityvalue = (coord.y * BrickHeight - partspec.y);
			partspec.lifespan = 1000 *  Math.random();
			
			temp.Particles.push(GenerateGravityParticle(partspec));
		}
		return temp;
	}

	that.GenerateEmitter = function(type, coord){
		if(type === 0){
			this.emitters.push(GenerateBrickEmitter(coord));
		}
	}

	that.Update = function(time){
		for(var i = 0; i < this.emitters.length;++i){
			if(this.emitters[i].update(time)){
				this.emitters.splice(i,1);	
			}
		}
	}
	
	that.RenderInformation = function(){
		var temp = [];
		this.emitters.forEach(function(value,index,array){
			value.Particles.forEach(function(lue,dex,ray){
				temp.push(GenerateRenderInformation(lue));
			})
		})
		return temp;
	}
	
	return that;
}())