
BrickBraker.ScoreManager = (function(){
	var that = {};
	var highscores = {};
	
	function UpdateHTML(){
		var htmlhighscores = document.getElementById('id-high-scores-list');
		htmlhighscores.innerHTML = "";
		for(var i = 0; i < 5; ++i){
			htmlhighscores.innerHTML += "<li>" + highscores[i] + "</li>";
		} 
	}
	
	function HighScorePush(){
			localStorage.BrickBraker = JSON.stringify(highscores);
			UpdateHTML();
	}
	
	that.PushScore = function(Score){
		for(var i = 0; i < 5;++i){
				if(Score > highscores[i]){
					var temp = highscores[i];
					highscores[i] = Score;
					Score = temp;
				}
			}
			HighScorePush();
		}
	that.ResetScores = function(){
		highscores = [];
		for(var i = 0; i < 5;++i){highscores[i]=0;}
		HighScorePush();
	}
	that.Initialize = function(){
		if(localStorage.BrickBraker === undefined){
			BrickBraker.ScoreManager.ResetScores();
			localStorage.BrickBraker = JSON.stringify(highscores);
		}
		else{
			highscores = JSON.parse(localStorage.BrickBraker);
		}  
		HighScorePush();
	}
	return that;
}())

BrickBraker.ScoreManager.Initialize();