function BrickBrackerEvent(eventcode){
	switch(eventcode.keyCode){
		case 65:
		case 74:
		case 37:
		BrickBraker.MovePaddleLeft();
		break;
		case 68:
		case 76:
		case 39:
		BrickBraker.MovePaddleRight();
		break;
	}
	
}

function BrickBrakerup(eventcode){
	switch(eventcode.keyCode){
		case 65:
		case 74:
		case 37:
		case 68:
		case 76:
		case 39:
		BrickBraker.StopPaddle();
		break;
	}
}

document.addEventListener('keydown',BrickBrackerEvent);
document.addEventListener('keyup',BrickBrakerup);