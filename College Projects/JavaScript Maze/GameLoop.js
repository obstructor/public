function GenerateSquare(){
	return {
	solid : true,
	player : false,
    distancetoend : 9000,
    startingpoint : false,
	endingpoint : false,
    visited : false
	};
};

function CoordinateConstruct(Coordinate){
	var that = {
		x : 0,
		y : 0
	}
	that.x = Coordinate.x;
	that.y = Coordinate.y;
	return that;
};

var MazeGame = {
    player : {x : 0, y : 0},
	endpoint : {x: 0, y : 0},
	startingpoint : {x : 0, y : 0},
    GameMap : [[]],
	ShortStack : [],
	Score : 0,
	HighScores : [],
    
    IsValid : function(Coordinate){
          if(MazeGame.GameMap.length > Coordinate.x && MazeGame.GameMap[0].length > Coordinate.y && Coordinate.x >= 0 && Coordinate.y >= 0){
              return true;
          }
          else return false;
      },
	  
	  UpdateHighScores : function(){
		  var doc = document.getElementById("id-HighScores");
		  doc.innerHTML = "";
		  for(var i = this.HighScores.length - 1; i >= 0;--i){
			  doc.innerHTML += "<p>" + this.HighScores[i] + "</p>";
		  }
	  },
	  
	  EndGame : function(){
		  if(this.timerrunning === true){
				this.Score += 100;
				this.timerrunning = false;
				this.HighScores.push(this.Score);
				this.HighScores.sort();
				this.UpdateHighScores();
		  }
	  },
    
     CheckCollision : function(coordinate){
          if(MazeGame.IsValid(coordinate)){
              if(MazeGame.GameMap[coordinate.x][coordinate.y].solid === true){
                  return true
              }
              else return false;

          }
          else return true;
         
      },
      
       MoveRight : function(){
          var Coordinate = {x : 0, y : 0};
		  Coordinate.x = MazeGame.player.x;  Coordinate.y = MazeGame.player.y;
          Coordinate.x += 1;
          if(MazeGame.CheckCollision(Coordinate)){
              return false;
          }
          else {
			  if(this.GameMap[Coordinate.x][Coordinate.y].distancetoend < this.GameMap[this.player.x][this.player.y].distancetoend){
				  this.Score += this.GameMap[Coordinate.x][Coordinate.y].distancetoend;
				  if(this.GameMap[Coordinate.x][Coordinate.y].endpoint === true){
					  this.EndGame();
				  }
			  }
			  else{
				  this.Score -= this.GameMap[Coordinate.x][Coordinate.y].distancetoend;
			  }
              this.GameMap[this.player.x][this.player.y].player = false;
			  this.GameMap[this.player.x][this.player.y].visited = true;
			  this.GameMap[Coordinate.x][Coordinate.y].player = true;
              MazeGame.player.x = Coordinate.x; MazeGame.player.y = Coordinate.y;
          }
      },
       MoveLeft : function(){
          var Coordinate = {x : 0, y : 0};
		  Coordinate.x = MazeGame.player.x;  Coordinate.y = MazeGame.player.y;
          Coordinate.x -= 1;
          if(MazeGame.CheckCollision(Coordinate)){
              return false;
          }
          else{
			  if(this.GameMap[Coordinate.x][Coordinate.y].distancetoend < this.GameMap[this.player.x][this.player.y].distancetoend){
				  this.Score += this.GameMap[Coordinate.x][Coordinate.y].distancetoend;
				  if(this.GameMap[Coordinate.x][Coordinate.y].endpoint === true){
					  this.EndGame();
				  }
			  }
			  else{
				  this.Score -= this.GameMap[Coordinate.x][Coordinate.y].distancetoend;
			  }
              this.GameMap[this.player.x][this.player.y].player = false;
			  this.GameMap[this.player.x][this.player.y].visited = true;
			  this.GameMap[Coordinate.x][Coordinate.y].player = true;
              MazeGame.player.x = Coordinate.x; MazeGame.player.y = Coordinate.y;
          }
      },
       MoveUp : function(){
          var Coordinate = {x : 0, y : 0};
		  Coordinate.x = MazeGame.player.x;  Coordinate.y = MazeGame.player.y;
          Coordinate.y -= 1;
          if(MazeGame.CheckCollision(Coordinate)){ 
              return false;
          }
          else{
			  if(this.GameMap[Coordinate.x][Coordinate.y].distancetoend < this.GameMap[this.player.x][this.player.y].distancetoend){
				  this.Score += this.GameMap[Coordinate.x][Coordinate.y].distancetoend;
				  if(this.GameMap[Coordinate.x][Coordinate.y].endpoint === true){
					  this.EndGame();
				  }
			  }
			  else{
				  this.Score -= this.GameMap[Coordinate.x][Coordinate.y].distancetoend;
			  }
              this.GameMap[this.player.x][this.player.y].player = false;
			  this.GameMap[this.player.x][this.player.y].visited = true;
			  this.GameMap[Coordinate.x][Coordinate.y].player = true;
              MazeGame.player.x = Coordinate.x; MazeGame.player.y = Coordinate.y;
          }
      },
	  
        MoveDown : function(){
          var Coordinate = {x : 0, y : 0};
		  Coordinate.x = MazeGame.player.x;  Coordinate.y = MazeGame.player.y;
          Coordinate.y += 1;
          if(MazeGame.CheckCollision(Coordinate)){
              return false;
          }
          else{
			  if(this.GameMap[Coordinate.x][Coordinate.y].distancetoend < this.GameMap[this.player.x][this.player.y].distancetoend){
				  this.Score += this.GameMap[Coordinate.x][Coordinate.y].distancetoend;
				  if(this.GameMap[Coordinate.x][Coordinate.y].endpoint === true){
					  this.EndGame();
				  }
			  }
			  else{
				  this.Score -= this.GameMap[Coordinate.x][Coordinate.y].distancetoend;
			  }
			  this.GameMap[this.player.x][this.player.y].player = false;
			  this.GameMap[this.player.x][this.player.y].visited = true;
			  this.GameMap[Coordinate.x][Coordinate.y].player = true;
              MazeGame.player.x = Coordinate.x; MazeGame.player.y = Coordinate.y;
          }
      },
    
		FrontierPush : function(array, value){
			var Coordinate = {x : 0, y : 0};
			Coordinate.x = value.x;  Coordinate.y = value.y;
          Coordinate.x += 2;
		  
          if(MazeGame.IsValid(Coordinate)){
              if(MazeGame.GameMap[Coordinate.x][Coordinate.y].solid === true){
                  array.push(CoordinateConstruct(Coordinate));
          	}
		  }
          Coordinate.x = value.x;  Coordinate.y = value.y;
          Coordinate.x -= 2;
          
          if(MazeGame.IsValid(Coordinate)){
              if(MazeGame.GameMap[Coordinate.x][Coordinate.y].solid === true){
                  array.push(CoordinateConstruct(Coordinate));
          	}
		  }
		  
          Coordinate.x = value.x;  Coordinate.y = value.y;
         Coordinate.y += 2;
         
          if(MazeGame.IsValid(Coordinate)){
             if(MazeGame.GameMap[Coordinate.x][Coordinate.y].solid === true){
                    array.push(CoordinateConstruct(Coordinate));
          		}
		  }
          
          Coordinate.x = value.x;  Coordinate.y = value.y;
          Coordinate.y -= 2;
          
         if(MazeGame.IsValid(Coordinate)){
              if(MazeGame.GameMap[Coordinate.x][Coordinate.y].solid === true){
                    array.push(CoordinateConstruct(Coordinate));
              }
          }
		},
      
      Frontier : function(array,index,value) {
          var Coordinate = {x: 0, y : 0};         
          Coordinate.x = value.x;  Coordinate.y = value.y; 
          
          if(MazeGame.IsValid(Coordinate)){  // Open Frontier Square to be connected to Maze.
              if(MazeGame.GameMap[Coordinate.x][Coordinate.y].solid === true){
                MazeGame.GameMap[Coordinate.x][Coordinate.y].solid = false;
				}
				else if (index !== 0){
					return; // The starting value is the only one that should already be open.  The square is already connected to the maze if its open.
				}
          }
		  else return;  //If the coordinate is not within the map something happened which is very bad and we should just run away.
          
		  var direction = Math.trunc((Math.random() * 10 % 4)); // Picks a random starting direction to try attaching to the maze.
		  
		  var done = false;
          for(var i = 0; i < 4 && done === false; ++i){
            switch(direction){
                case 0:
                    Coordinate.y -=2;
                    if(MazeGame.IsValid(Coordinate)){
                        if(MazeGame.GameMap[Coordinate.x][Coordinate.y].solid === false){
                        MazeGame.GameMap[Coordinate.x][Coordinate.y + 1].solid = false; // Remove Wall
						done = true;
                    	}
					}
                break;
                case 1:
               		Coordinate.y +=2;
                    if(MazeGame.IsValid(Coordinate)){
                        if(MazeGame.GameMap[Coordinate.x][Coordinate.y].solid === false){
                        MazeGame.GameMap[Coordinate.x][Coordinate.y - 1].solid = false; // Remove Wall
						done = true;
                    	}
					}
                break;
                case 2:
               		Coordinate.x -=2;
                    if(MazeGame.IsValid(Coordinate)){
                        if(MazeGame.GameMap[Coordinate.x][Coordinate.y].solid === false){
                        MazeGame.GameMap[Coordinate.x + 1][Coordinate.y].solid = false; // Remove Wall
						done = true;
                    	}
					}
                break;
                case 3:
               		Coordinate.x +=2;
                    if(MazeGame.IsValid(Coordinate)){
                        if(MazeGame.GameMap[Coordinate.x][Coordinate.y].solid === false){
                        MazeGame.GameMap[Coordinate.x - 1][Coordinate.y].solid = false; // Remove Wall
						done = true;
                    	}
					}
                break;
                default: // Well something went wrong, set direction to a valid number. Ignore randomness.
                	direction = 0;
                break;
            }    
            Coordinate.x = value.x;  Coordinate.y = value.y; 
            direction = (direction + 1) % 4;
          }
          
		  if(done === false){
			  return; // No direction attaches to the maze, something is wrong just continue to the next value.
		  }
		  
		   /* Queue up all frontier squares.
		  ** The code will requeue many squares.
		  ** Adding a flag to check here would be of little use as we check immediately if its already been added to the maze.*/
		  
          this.FrontierPush(array,value);
      },
	  
	  FindEndPoint : function(){
		  var xl = this.GameMap.length - 1;
		  var yl = this.GameMap[0].length - 1;
		  if(this.GameMap[xl][yl].solid === false){
		 this.GameMap[xl][yl].endpoint = true;
		 this.endpoint.x = xl; this.endpoint.y = yl;
		  }
		  else if(this.GameMap[xl][yl - 1].solid === false){
		 this.GameMap[xl][yl - 1].endpoint = true;
		 this.endpoint.x = xl; this.endpoint.y = yl - 1;
		  }
		  else if(this.GameMap[xl - 1][yl].solid === false){
		 this.GameMap[xl - 1][yl].endpoint = true;
		 this.endpoint.x = xl - 1; this.endpoint.y = yl;
		  }
		   else if(this.GameMap[xl -1][yl - 1].solid === false){
		this.GameMap[xl - 1][yl - 1].endpoint = true;
		 this.endpoint.x = xl - 1; this.endpoint.y = yl - 1;
		  }
	  },
	  
	  FindStartingPoint : function(){
		  if(this.GameMap[0][0].solid === false){
		 this.GameMap[0][0].player = true;
		 this.GameMap[0][0].startingpoint = true;
		 this.player.x = 0; this.player.y = 0;
		 this.startingpoint.x = 0; this.startingpoint.y = 0;
		  }
		  else if(this.GameMap[0][1].solid === false){
		this.GameMap[0][1].player = true;
		 this.GameMap[0][1].startingpoint = true;
		 this.player.x = 0; this.player.y = 1;
		 this.startingpoint.x = 0; this.startingpoint.y = 1;
		  }
		  else if(this.GameMap[1][0].solid === false){
		this.GameMap[1][0].player = true;
		 this.GameMap[1][0].startingpoint = true;
		 this.player.x = 1; this.player.y = 0;
		 this.startingpoint.x = 1; this.startingpoint.y = 0;
		  }
		   else if(this.GameMap[1][1].solid === false){
		this.GameMap[1][1].player = true;
		 this.GameMap[1][1].startingpoint = true;
		 this.player.x = 1; this.player.y = 1;
		 this.startingpoint.x = 1; this.startingpoint.y = 1;
		  }
	  },
	  
	  GenerateShortStack: function(Coordinate, iterator){
		  if(this.IsValid(Coordinate)){
			if(this.GameMap[Coordinate.x][Coordinate.y].solid === false && this.GameMap[Coordinate.x][Coordinate.y].distancetoend > iterator){
				this.GameMap[Coordinate.x][Coordinate.y].distancetoend = iterator;
				var c = {x:0, y:0}; c.x = Coordinate.x + 1; c.y = Coordinate.y;
				this.GenerateShortStack(c,iterator + 1);
				var d = {x:0, y:0}; d.x = Coordinate.x - 1; d.y = Coordinate.y;
				this.GenerateShortStack(d,iterator + 1);
				var e = {x:0, y:0}; e.x = Coordinate.x; e.y = Coordinate.y - 1;
				this.GenerateShortStack(e,iterator + 1);
				var f = {x:0, y:0}; f.x = Coordinate.x; f.y = Coordinate.y + 1;
				this.GenerateShortStack(f,iterator + 1);
			}
			else return;
		}
		return;
	  },
	  
	  GenerateShortness : function(Coordinate){
		  if(this.IsValid(Coordinate)){
			  if(this.GameMap[Coordinate.x][Coordinate.y].solid === false){
				  this.ShortStack.push(Coordinate);
				  var temp = {x : 0, y : 0}; 
				  temp.x = Coordinate.x; temp.y = Coordinate.y + 1;
				  
				  if(this.IsValid(temp)){
					if(this.GameMap[temp.x][temp.y].distancetoend > this.GameMap[Coordinate.x][Coordinate.y].distancetoend && this.GameMap[temp.x][temp.y].solid === false){
						this.GenerateShortness(temp);
					}
				  }
			  }
				  temp.y = Coordinate.y - 1; temp.x = Coordinate.x;
				  if(this.IsValid(temp)){
					if(this.GameMap[temp.x][temp.y].distancetoend > this.GameMap[Coordinate.x][Coordinate.y].distancetoend && this.GameMap[temp.x][temp.y].solid === false){
						this.GenerateShortness(temp);
					}
			  }
				  temp.x = Coordinate.x + 1; temp.y = Coordinate.y;
				  if(this.IsValid(temp)){
					if(this.GameMap[temp.x][temp.y].distancetoend > this.GameMap[Coordinate.x][Coordinate.y].distancetoend && this.GameMap[temp.x][temp.y].solid === false){
						this.GenerateShortness(temp);
					}
			  }
				  temp.x = Coordinate.x - 1; temp.y = Coordinate.y;
				  if(this.IsValid(temp)){
				  if(this.GameMap[temp.x][temp.y].distancetoend > this.GameMap[Coordinate.x][Coordinate.y].distancetoend && this.GameMap[temp.x][temp.y].solid === false){
					  this.GenerateShortness(temp);
				  }
			  }
		  }
		  return;
	  },
	  
	  GenerateShortnessNonRecursively : function(){
		  var Coordinate = {x : 0, y : 0}; Coordinate.x = this.startingpoint.x; Coordinate.y = this.startingpoint.y;
		  var temp = {x : 0, y : 0}; temp.x = Coordinate.x; temp.y = Coordinate.y;
		  var running = true;
		  this.ShortStack.push(this.Endpoint);
		  while(running){
			  temp.x = Coordinate.x + 1; temp.y = Coordinate.y;
			if(this.IsValid(temp)){
				if(this.GameMap[temp.x][temp.y].distancetoend < this.GameMap[Coordinate.x][Coordinate.y].distancetoend){
					this.ShortStack.push(temp);
				}
			}
			temp.x = Coordinate.x - 1; temp.y = Coordinate.y;
			if(this.IsValid(temp)){
				if(this.GameMap[temp.x][temp.y].distancetoend < this.GameMap[Coordinate.x][Coordinate.y].distancetoend){
					this.ShortStack.push(temp);
				}
			}
			temp.x = Coordinate.x; temp.y = Coordinate.y + 1;
			if(this.IsValid(temp)){
				if(this.GameMap[temp.x][temp.y].distancetoend < this.GameMap[Coordinate.x][Coordinate.y].distancetoend){
					this.ShortStack.push(temp);
				}
			}
			temp.x = Coordinate.x; temp.y = Coordinate.y - 1;
			if(this.IsValid(temp)){
				if(this.GameMap[temp.x][temp.y].distancetoend < this.GameMap[Coordinate.x][Coordinate.y].distancetoend){
					this.ShortStack.push(temp);
				}
			}  
		  };
	  },
	  
	  GenerateShortestPath: function(){ // Well this function used to do more, now everything is calculated using distancetoend.
		  this.GenerateShortStack(this.endpoint,0);  
	  },
	  
     GenerateMaze : function(){
          var frontier = [];
          var Coordinate = {x : 0, y : 0};
		  Coordinate.x = Math.trunc(Math.random() *100 % this.GameMap.length);
		  Coordinate.y = Math.trunc(Math.random() *100 % this.GameMap[0].length);
          this.GameMap[Coordinate.x][Coordinate.y].solid = false;
          this.FrontierPush(frontier, Coordinate);
          for(var i = 0; i < frontier.length;++i){
			  this.Frontier(frontier,i,frontier[i]);
		  }
		 this.FindStartingPoint();
		 this.FindEndPoint();
		 this.GenerateShortestPath();
      },
	  
      timer : 0,
	  screentimer : 0,
	  timerrunning : false,
	  
     Generate : function(Number){
		 this.GameMap.length = 0;
         for(var i = 0; i < Number; ++i){
			  this.GameMap.push(new Array(Number));
		  }
		  for(var i = 0; i < Number; ++i){
			  for(var k = 0; k < Number; ++k){
				  this.GameMap[i][k] = GenerateSquare();
			  }
		  }
		this.GenerateMaze();
		this.screentimer = 0;
		this.Score = 0;
		this.timerrunning = true;
      },

	 
	  Update : function(){
		  if(this.timerrunning){
			var temp = performance.now();
			this.screentimer += (temp - this.timer) / 1000;
			this.timer = temp;
			temp = Math.trunc(this.screentimer);
			var documenttemp = document.getElementById("id-timer");
			documenttemp.innerHTML = "Timer: " + String(temp);
		  }
	  },
	  
	  canvas : 0,
	  context : 0,
		
	  CharacterImage : 0,
	  togglescore : false,
	  togglebread : false,
	  togglehint : false,
	  toggleshortest : false,
	  
	  breadcrums : 0,
	  walls : 0,
	  water : 0,
	  endpointImage : 0,
	  startpointimage: 0,
	  hint : 0,
	  
	  
	  Blocksize : 600 / 20,
	  
	  
	  initialize : function(){
		  this.timer = performance.now();
		  
		  this.CharacterImage = new Image()
		  this.CharacterImage.src = 'octopus.png';
		  this.canvas = document.getElementById('canvas-main');
		  this.context = this.canvas.getContext('2d');
		  
		  this.walls = new Image();
	  	  this.water = new Image();
		  this.hint = new Image();
		  this.breadcrums = new Image();
	  	  this.endpointimage = new Image();
		  this.startpointimage = new Image();
		  
	  	  this.water.src = 'water.png';
	  	  this.walls.src = 'sand.png';
		  this.hint.src = 'hint.png';
		  this.breadcrums.src = 'breadcrumbs.png';
	  	  this.endpointimage.src = 'endpoint.png';
		  this.startpointimage.src = 'startpoint.png';
		  
		  this.GameMap.length = 0;
		  for(var i = 0; i < 20; ++i){
			  this.GameMap.push(new Array(20));
		  }
		  for(var i = 0; i < 20; ++i){
			  for(var k = 0; k < 20; ++k){
				  this.GameMap[i][k] = GenerateSquare();
			  }
		  }
	  },
	 
	  
	
	clear : function(){
		this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
	},
	  
	 RenderMaze : function(){
		  for(var i = 0; i < this.GameMap.length;++i){
			  for(var k = 0; k < this.GameMap[0].length;k++){
				  if(this.GameMap[i][k].solid === true){
					  this.context.drawImage(this.walls, i * this.Blocksize, k * this.Blocksize, this.Blocksize,this.Blocksize);
				  }
				  else this.context.drawImage(this.water, i * this.Blocksize, k * this.Blocksize, this.Blocksize,this.Blocksize);
				  
				  if(this.GameMap[i][k].player === true){
					 this.context.drawImage(this.CharacterImage, i * this.Blocksize, k * this.Blocksize, this.Blocksize,this.Blocksize);
				  }
				  
				  if(this.GameMap[i][k].endpoint === true){
					  this.context.drawImage(this.endpointimage, i * this.Blocksize, k * this.Blocksize, this.Blocksize,this.Blocksize);
				  }
				  
				  if(this.GameMap[i][k].startingpoint === true){
					  this.context.drawImage(this.startpointimage, i * this.Blocksize, k * this.Blocksize, this.Blocksize,this.Blocksize);
				  }
				  
				  if(this.GameMap[i][k].visited === true && this.togglebread === true){
					  this.context.drawImage(this.breadcrums, i * this.Blocksize, k * this.Blocksize, this.Blocksize,this.Blocksize);
				  }
			  };
		  };
	  },
	  
	  RenderPlayer : function(){
		this.context.drawImage(this.CharacterImage, this.player.x * this.Blocksize, this.player.y * this.Blocksize, this.Blocksize, this.Blocksize);  
	  },
	  
	RenderMisc : function(){
		  if(MazeGame.toggleshortest){
			  var temp = {x : 0, y : 0};
			  var temp2 = {x : 0, y : 0};
			  temp2.x = temp.x = this.player.x; temp2.y = temp.y = this.player.y;
			  while(this.GameMap[temp.x][temp.y].distancetoend > 1){
				  temp2.y = temp.y - 1; temp2.x = temp.x;
				  if(this.IsValid(temp2)){
				 if(this.GameMap[temp2.x][temp2.y].distancetoend < this.GameMap[temp.x][temp.y].distancetoend){
					 temp.y -= 1;
					 this.context.drawImage(this.hint, temp.x * this.Blocksize, temp.y * this.Blocksize, this.Blocksize,this.Blocksize);
				}}
				temp2.y = temp.y + 1; temp2.x = temp.x;
				if(this.IsValid(temp2)){
				if(this.GameMap[temp2.x][temp2.y].distancetoend < this.GameMap[temp.x][temp.y].distancetoend){
					 temp.y += 1;
					 this.context.drawImage(this.hint, temp.x * this.Blocksize, temp.y * this.Blocksize, this.Blocksize,this.Blocksize);
				}}
				temp2.x = temp.x - 1; temp2.y = temp.y;
				if(this.IsValid(temp2)){
				if(this.GameMap[temp2.x][temp2.y].distancetoend < this.GameMap[temp.x][temp.y].distancetoend){
					temp.x -= 1;
					 this.context.drawImage(this.hint, temp.x * this.Blocksize, temp.y * this.Blocksize, this.Blocksize,this.Blocksize);
				}}
				temp2.x = temp.x + 1; temp2.y = temp.y;
				if(this.IsValid(temp2)){
				if(this.GameMap[temp2.x][temp2.y].distancetoend < this.GameMap[temp.x][temp.y].distancetoend){
					temp.x += 1;
					this.context.drawImage(this.hint, temp.x * this.Blocksize, temp.y * this.Blocksize, this.Blocksize,this.Blocksize);
				}}
			  }
		  }
		  if(MazeGame.togglehint){
			  temp = {x:0, y: 0};
			  temp.x = this.player.x; temp.y = this.player.y;
			  temp.y -= 1; temp.x =this.player.x;
			  if(this.IsValid(temp)){
				if(this.GameMap[temp.x][temp.y].distancetoend < this.GameMap[this.player.x][this.player.y].distancetoend){
					this.context.drawImage(this.hint, (this.player.x) * this.Blocksize, (this.player.y - 1) * this.Blocksize, this.Blocksize,this.Blocksize);
				}
			  }
			  temp.y = this.player.y + 1;temp.x =this.player.x;
			  if(this.IsValid(temp)){
				if(this.GameMap[temp.x][temp.y].distancetoend < this.GameMap[this.player.x][this.player.y].distancetoend){
					this.context.drawImage(this.hint, (this.player.x) * this.Blocksize, (this.player.y + 1) * this.Blocksize, this.Blocksize,this.Blocksize);
			  	}
			  }
			  temp.y = this.player.y; temp.x =this.player.x - 1;
			  if(this.IsValid(temp)){
				if(this.GameMap[temp.x][temp.y].distancetoend < this.GameMap[this.player.x][this.player.y].distancetoend){
					this.context.drawImage(this.hint, (this.player.x - 1) * this.Blocksize, (this.player.y) * this.Blocksize, this.Blocksize,this.Blocksize);
			 	 }
			  }
			  temp.y = this.player.y; temp.x = this.player.x + 1;
			  if(this.IsValid(temp)){
				if(this.GameMap[temp.x][temp.y].distancetoend < this.GameMap[this.player.x][this.player.y].distancetoend){
					this.context.drawImage(this.hint, (this.player.x + 1) * this.Blocksize, (this.player.y) * this.Blocksize, this.Blocksize,this.Blocksize);
			  	}
			  }
		  }
		  if(MazeGame.togglescore){
			  var scoretemp = document.getElementById("id-score");
		  	  scoretemp.innerHTML = " Score: " + this.Score;
		  }
	  },
	  
	Render : function() {
		this.Blocksize = 600 / this.GameMap.length;
		  MazeGame.clear();
		 MazeGame.RenderMaze();
		  MazeGame.RenderPlayer();
		  MazeGame.RenderMisc();
	},


CollectInput : function (event){
    switch(event.keyCode){
    case 68: // d
    case 76: // l
    MazeGame.MoveRight();
    break;
    case 87: // w
    case 73: // i
    MazeGame.MoveUp();
    break;
    
    case 83: // s
    case 75: // k
    MazeGame.MoveDown();
    break;
    
    case 65: // a
    case 74: // j
    MazeGame.MoveLeft();
    break;
	
	case 72: // h
    MazeGame.togglehint = !MazeGame.togglehint;
    break;
	
	case 66: // b
	MazeGame.togglebread = !MazeGame.togglebread;
	break;
	
	case 80: // p
	MazeGame.toggleshortest = !MazeGame.toggleshortest;
	break;
	
	case 89: // y
	MazeGame.togglescore = !MazeGame.togglescore;
	break;
    }
}
};



document.addEventListener('keydown', MazeGame.CollectInput);
function GameLoop(){
    //Compute elapsed time is part of update.
	//Collect Input is handled by the document listener.
   MazeGame.Update();
   MazeGame.Render();
   requestAnimationFrame(GameLoop);
   };

MazeGame.initialize();
GameLoop();
requestAnimationFrame(GameLoop);

