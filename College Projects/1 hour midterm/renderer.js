

MiniGame.Renderer = (function(){
	var that = {};
	var background = {};
	var bombimage = {};
	var numberimages = [];
	var checkmark = {};
	var explosion = {};
	
	var canvas = document.getElementById('canvas-main'),
		context = canvas.getContext('2d');
		
	function clear() {
		context.setTransform(1, 0, 0, 1, 0, 0);
		context.clearRect(0, 0, canvas.width, canvas.height);
		}
	
	function RenderBackGround(){
		context.drawImage(background, 0, 0, 600, 600);
	}
	
	function RenderBombs(){
		for(var i = 0; i < MiniGame.bombs.length;++i){
			for(var k = 0; k < MiniGame.bombs[i].length; ++k){
				if(MiniGame.bombs[i][k].dead === false){
					context.drawImage(bombimage, 85 * k + 100, 85 * i + 50, 84,84);
					if(MiniGame.bombs[i][k].defused === true){
						context.drawImage(checkmark, 85 * k+ 100 + 30, 85 * i +30 +50, 45, 45);
					}
					else{
						if(Math.trunc(MiniGame.bombs[i][k].time) >= 0)
						context.drawImage(numberimages[Math.trunc(MiniGame.bombs[i][k].time)], 85 * k + 30 + 100, 85 * i +30 + 50);
						}
					
				}
			}
		}
	}
	
	function RenderParticles(){
		
		for(var i = 0; i < MiniGame.ParticleManager.emitters.length;++i){
			for(var k = 0; k < MiniGame.ParticleManager.emitters[i].length;++k){
				if(MiniGame.ParticleManager.emitters[i][k].type == 0){
					context.drawImage(explosion, MiniGame.ParticleManager.emitters[i][k].x, MiniGame.ParticleManager.emitters[i][k].y,84,84);
				}
				else{
					context.beginPath();
					context.moveTo(MiniGame.ParticleManager.emitters[i][k].x,MiniGame.ParticleManager.emitters[i][k].y + 2);
					context.lineTo(MiniGame.ParticleManager.emitters[i][k].x - 2, MiniGame.ParticleManager.emitters[i][k].y);
					context.lineTo(MiniGame.ParticleManager.emitters[i][k].x, MiniGame.ParticleManager.emitters[i][k].y - 2);
					context.lineTo(MiniGame.ParticleManager.emitters[i][k].x + 2, MiniGame.ParticleManager.emitters[i][k].y);
					context.closePath();
					
					context.fillStyle =  "#FFFFFF";
					context.fill();
					
					context.strokeStyle = "#c0c0c0";
					context.lineWidth = 1;
					context.stroke();
				}
			}
		}
	}
	
	function RenderScore(){
		context.font = "20px Arial";
		context.fillText("Score: " + MiniGame.BombManager.LevelScore(), 0, canvas.height - 20);
		context.fillText("Time: " + Math.trunc(.001 * MiniGame.BombManager.LevelTime()), canvas.width - 450, canvas.height - 20);
		context.fillText("TotalTime: " + Math.trunc(.001*MiniGame.totaltime), canvas.width - 300, canvas.height - 20);
		context.fillText("TotalScore: " + MiniGame.totalscore, canvas.width - 150, canvas.height- 20);
	}
	
	that.RenderNumber = function(Number){
		context.font = "50px Arial";
		context.fillText(Number.toString(), canvas.width / 2 - 25, canvas.height / 2 - 25);
	}
	
	that.Initialize = function(){
		background = new Image();
		background.src = "images/Background.png";
		bombimage = new Image();
		bombimage.src = "images/Bomb.png";
		checkmark = new Image();
		checkmark.src = "images/checkmark.png";
		explosion = new Image();
		explosion.src = "images/Explosion.png";
		
		for(var i = 0; i <= 9;++i){
			numberimages[i] = new Image();
			numberimages[i].src = "images/" + i + ".png";
		}
	}
	
	that.Render = function(){
		clear();
		RenderBackGround();
		RenderScore();
		RenderBombs();
		RenderParticles();
		
	}
		
	return that;
}());

MiniGame.Renderer.Initialize();