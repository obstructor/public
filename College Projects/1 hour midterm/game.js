
var MiniGame = (function(){
	var that = {
		bombs : [],
		totalscore : 0,
		totaltime : 0,
		running : false
	};
	var timer;
	
	function Timer(timestamp){
		timer = timestamp - timer;
		if(timer > 30){
			timer = 30;
		}
		else if(timer > 0){
			timer = timer;
		}
		else
			timer = 0;
	}
	
	function Update(timestamp){
		Timer(timestamp);
		MiniGame.BombManager.Update(timer);
		MiniGame.ParticleManager.Update(timer);
	}
	
	function Render(){
		MiniGame.Renderer.Render();
	}
	
	that.EndGame = function(){
		MiniGame.ScoreManager.PushScore(that.totalscore, that.totaltime, 6);
		that.RequestFrame = function(){
			that.running = false;
			return;
			}
		MiniGame.ScreenManager.EndGame();
	}
	
	that.RequestFrame = function(){
		that.running = false;
		return;
	}
	
	that.Pause = function(){
		that.running = false;
		that.RequestFrame = function(){
		return;
		}
	}
	
	that.ReturnToMenu = function(){
		MiniGame.ScoreManager.PushScore(that.totalscore, that.totaltime, 6);
		that.EndGame();
	}
	
	function GenerateBombs(){
		var list = [];
		for(var k = 0; k < 3; ++k){
			list.push({
			dead : true,
			time : 0,
			defused : true,
			Update : function(timer){
				return false;
				}
			});
		}
		return list;
	}
	
	that.Initialize = function(){
		that.score = 0;
		that.totalscore = 0;
		that.bombs = [];
		for(var i = 0; i < 8; ++i){
			that.bombs.push(GenerateBombs());
		}
		that.RequestFrame = function(timestamp){
			timer = timestamp;
			requestAnimationFrame(that.GameLoop);
		}
		MiniGame.BombManager.Initialize();
	}
	
	that.GameLoop = function(timestamp){
		Update(timestamp);
		Render();
		that.RequestFrame(timestamp);
	}
	
	that.Resume = function(){
		that.RequestFrame = function(timestamp){
			timer = timestamp;
			that.running = true;
			requestAnimationFrame(that.GameLoop);
		}
		that.GameLoop(performance.now());
	}
	
	
	return that;
}());