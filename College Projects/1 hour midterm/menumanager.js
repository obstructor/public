
MiniGame.ScreenManager = (function(){
	var that = {};
	var divs = {};
	
	that.ReturnToMenu = function(){
		MiniGame.ScreenManager.ShowScreen('menu');
	}
	
	that.EndGame = function(){
		that.ReturnToMenu();
	}
	
	that.Resume = function(){
		MiniGame.ScreenManager.ShowScreen('game-play');
		MiniGame.Countdown.startplusrestore();
	}
	
	that.ShowScreen = function(screentext){
		for(var property in divs){
			divs[property].classList.remove('active');
		}
		switch(screentext){
			case "about":
			divs.about.classList.add('active');
			break;
			case "help":
			divs.help.classList.add('active');
			break;
			case "high-scores":
			divs.highscore.classList.add('active');
			break;
			case "game-play":
			divs.game.classList.add('active');
			MiniGame.Initialize();
			break;
			case"menu":
			divs.mainmenu.classList.add('active');
			break;
		}
	}
	
	that.StartGame = function(){
		MiniGame.ScreenManager.ShowScreen('game-play');
		MiniGame.Initialize();
	}
	
	that.Initialize = function(){
		divs.game = document.getElementById('game-play');
		divs.highscore = document.getElementById('high-scores');
		divs.mainmenu = document.getElementById('main-menu');
		divs.help = document.getElementById('help');
		divs.about = document.getElementById('about');
		
		document.getElementById('id-new-game').addEventListener('click',
			function() {MiniGame.ScreenManager.ShowScreen('game-play'); });
		
		document.getElementById('id-high-scores').addEventListener('click',
			function() { MiniGame.ScreenManager.ShowScreen('high-scores'); });
		
		document.getElementById('id-about').addEventListener('click',
			function() { MiniGame.ScreenManager.ShowScreen('help'); });
			
		document.getElementById('id-about-back').addEventListener('click',
			function() { MiniGame.ReturnToMenu(); });
			
		document.getElementById('id-high-scores-back').addEventListener('click',
			function() { MiniGame.ReturnToMenu(); });
			
		document.getElementById('id-about-back').addEventListener('click',
			function() { MiniGame.ReturnToMenu(); });
			
		document.getElementById('id-game-play-back').addEventListener('click',
			function() { MiniGame.ReturnToMenu(); });
			
		MiniGame.ScreenManager.ReturnToMenu();
	}
	
	return that;
}());

MiniGame.ScreenManager.Initialize();