
MiniGame.ScoreManager = (function(){
	var that = {};
	var highscores = {
		time : [],
		points : []
	};
	
	function UpdateHTML(){
		var htmlhighscores = document.getElementById('id-high-scores-list');
		htmlhighscores.innerHTML = "";
		for(var j = 1; j < 7; ++j){
				htmlhighscores.innerHTML += "<li> Score </li>";
			for(var i = 0; i < 5; ++i){
				if(j <= 5)
					htmlhighscores.innerHTML += "<li> Level: " + j + " " + highscores.points[j][i] + "</li>";
				else
					htmlhighscores.innerHTML += "<li> Total: " + " " + highscores.points[j][i] + "</li>";
			}  
		}
		
		for(var j = 1; j < 7; ++j){
			htmlhighscores.innerHTML += "<li> Times </li>";
			for(var i = 0; i < 5; ++i){
				if(j <= 5)
					htmlhighscores.innerHTML += "<li> Level: " + j + " " + highscores.time[j][i] + "</li>";
				else
					htmlhighscores.innerHTML += "<li> Total: " + " " + highscores.time[j][i] + "</li>";
			}  
		}
	}
	
	function HighScorePush(){
			localStorage.MiniGame = JSON.stringify(highscores);
			UpdateHTML();
	}
	
	function CreateScore(xtime, xscore){
		var that = {
			time : xtime,
			score : xscore
		}
		return that;
	}
	
	that.PushScore = function(Score, time, Level){
		for(var i = 0; i < 5;++i){
				if(Score > highscores.points[Level][i]){
					var temp = highscores.points[Level][i];
					highscores.points[Level][i] = Score;
					Score = temp;
				}
			}
			
		for(var i = 1; i < 7;++i){
				if(time < highscores.time[Level][i]){
					var temp2 = highscores.time[Level][i];
					highscores.time[Level][i] = time;
					time = temp2;
				}
			}
			HighScorePush();
		}
	that.ResetScores = function(){
		highscores.points = []; highscores.time = [];
		for(var i = 1; i < 7;++i){
			var scores = [];
			for(var j = 0; j < 5; ++j){
				scores.push(0);
			}
			highscores.points[i] = scores;
		}
		for(var i = 1; i < 7;++i){
			var times = [];
			for(var j = 0; j < 5; ++j){
				times.push(999999999);
			}
			highscores.time[i] = times;
		}
		HighScorePush();
	}
	that.Initialize = function(){
		if(localStorage.MiniGame === undefined){
			MiniGame.ScoreManager.ResetScores();
			localStorage.MiniGame = JSON.stringify(highscores);
		}
		else{
			highscores = JSON.parse(localStorage.MiniGame);
		}  
		HighScorePush();
	}
	return that;
}())

MiniGame.ScoreManager.Initialize();