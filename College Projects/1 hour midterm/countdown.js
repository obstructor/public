MiniGame.Countdown = (function(){
	var that = {};
	var timer;
	var running = false;;
	var restoreflag = false;
	function run(timestamp){
		
			MiniGame.Renderer.Render();
			if(timestamp - timer > 2000){
				MiniGame.Renderer.RenderNumber(1);
			}
			else if(timestamp - timer > 1000){
				MiniGame.Renderer.RenderNumber(2);
			}
			else{
				MiniGame.Renderer.RenderNumber(3);
			}
			if(timestamp - timer < 3000){
				requestAnimationFrame(run)
				}
			else if(restoreflag === false){
				running = false;
				MiniGame.GameLoop(timestamp);
			}
			else if (restoreflag === true){
				running = false;
				MiniGame.Resume();
			}
	}
	
	that.start = function(){
		if(running === true)
		return false;
		else{
			running = true; restoreflag = false;
			timer = performance.now();
			run(timer);
		}
	}
	that.startplusrestore = function(){
		if(running === true)
		return false;
		else{
			running = true; restoreflag = true;
			timer = performance.now();
			run(timer);
		}
	}
	
	return that;
}())