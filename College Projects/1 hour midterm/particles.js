MiniGame.ParticleManager = (function(){
	var that = {};
	that.emitters = [];

	function Copy(spec){
		var thecopy = {};
		thecopy.type = spec.type; 
		thecopy.x = spec.x; 
		thecopy.y = spec.y; 
		thecopy.vx = spec.vx; 
		thecopy.vy = spec.vy; 
		thecopy.life = spec.life; 
		thecopy.speed = spec.speed;
		return thecopy;
	}

	function Particles(spec){
		var temp = [];
		
		var partspec = {
			type: 1, x: 0, y : 0, vx : 0, vy: 0, life : 0, speed : 0
		}
		for(var l = 0; l < 25;++l){
			partspec.x = spec.x; partspec.y = spec.y; partspec.vx = Math.random() * .1; partspec.vy = Math.random() * .1;
			partspec.life = 1000; partspec.speed = .00001;
			switch(l % 4){
				case 0:
				break;
				case 1:
				partspec.vx *= -1;
				break;
				case 2:
				partspec.vy *= -1;
				break;
				case 3:
				partspec.vy *= -1;
				partspec.vx *= -1;
				break;
			}
			temp.push(Copy(partspec));
		}
		return temp;
	}
	
	function Explosion(coord){
		var temp = [];
		var partspec = {
			type : 0, x: coord.x, y : coord.y, vx : 0, vy: 0, life : 250, speed : 0
		}
		temp.push(partspec);
		return temp;
	}
	
	that.GenerateEmitter = function(coord){
		var coordinate = {};
		coordinate.y = coord.x * 85 + 50;
		coordinate.x = coord.y * 85 + 100;
		this.emitters.push(Explosion(coordinate));
		coordinate.y = coord.x * 85 + 50 + 40;
		coordinate.x = coord.y * 85 + 100 + 40;
		this.emitters.push(Particles(coordinate));
	}

	that.Update = function(time){
		for(var i = 0; i < this.emitters.length;++i){
			for(var k = 0; k < that.emitters[i].length; ++k){
				that.emitters[i][k].life -= time;
				that.emitters[i][k].x += that.emitters[i][k].vx * time;
				that.emitters[i][k].y += that.emitters[i][k].vy * time;
				if(that.emitters[i][k].type==1)
				that.emitters[i][k].vy += .0001 * time;
				that.emitters[i][k].vx -= .001 * that.emitters[i][k].vx * time;
				if(that.emitters[i][k].life < 0){
					that.emitters[i].splice(k,1);
				}
			}
			if(that.emitters[i].length < 1){
				that.emitters.splice(i,1);
			}
		}
	}
	that.Initialize = function(){
		that.emitters = [];
	}
	
	return that;
}())