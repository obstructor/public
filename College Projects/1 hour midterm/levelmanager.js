

MiniGame.BombManager = (function(){
	var that = {};
	
	var level = 0;
	var leveltime = 0;
	var levelscore = 0;
	var bombnumbers = [1,1,2,2,3,3,2,3,4,3,4,5,4,5,6,5,6,7];
	var Bombs = [];
	var endflag = false;
	
	function CreateEmptyBomb(){
		return {
			dead : true,
			time : 0,
			defused : true,
			Update : function(timer){
				return false;
			}
		};
	}
	
	function CreateBomb(iterator){
		var bomb = {
			dead : false,
			defused : false
		};
		bomb.time = bombnumbers[iterator];
		bomb.Update = function(timer){
			if(bomb.defused === true || bomb.dead === true){
				return false;
			}
			else{
			bomb.time -= .01 * timer;
			}
			if(bomb.time < 0){
				bomb.dead = true;
				return true;
			}
			return false;
		}
		return bomb;
	}
	
	function PushToBoard(){
		var swapvalue = 0;
		var temp = {};
		for(var i = 0; i < Bombs.length; ++i){
			swapvalue = Math.trunc((Math.random() * 50) % Bombs.length);
			temp = Bombs[i]; Bombs[i] = Bombs[swapvalue];
			Bombs[swapvalue] = temp;
		}
		
		var bombcounter = 0;
		
		for(var k = 0; k < MiniGame.bombs.length && bombcounter < Bombs.length;++k){
			for(var l = 0; l < MiniGame.bombs[k].length && bombcounter < Bombs.length;++l){
				MiniGame.bombs[k][l] = Bombs[bombcounter];
				++bombcounter;
			}
		}
	}
	
	function SpawnBombs(count){
		Bombs = [];
		for(var i = 0; i < count; ++i){
			Bombs[i] = CreateBomb(i);
		}
		
		PushToBoard();
		
		if(count > 18){
			MiniGame.EndGame();
		}
	}
	that.Level = function(){
		return level;
	}
	that.LevelTime = function(){
		return leveltime;
	}
	that.LevelScore = function(){
		return levelscore;
	}
	
	that.Update = function(time){
		leveltime += Math.trunc(time);
		var flag = false;
		endflag = true;
		for(var i = 0; i < MiniGame.bombs.length;++i){
			for(var k = 0; k < MiniGame.bombs[i].length; ++k){
				if(MiniGame.bombs[i][k].defused === true || MiniGame.bombs[i][k].dead === true){
					flag = false;
				}
				else  if(MiniGame.bombs[i][k].time > 0){
				MiniGame.bombs[i][k].time -= .001 * time;
				}
				else if(MiniGame.bombs[i][k].time < 0){
					MiniGame.bombs[i][k].dead = true;
					MiniGame.ParticleManager.GenerateEmitter({x:i,y:k});
				}
				endflag = endflag && (MiniGame.bombs[i][k].dead || MiniGame.bombs[i][k].defused);
			}
		}
		if(endflag === true){
			that.EndLevel();
		}
	}
	
	that.Defuse = function(click){
		if(click.x < 0 || click.x > 8 || click.y < 0 || click.y > 3)
		{
			return;
		}
		else if(MiniGame.running === true){
			if(MiniGame.bombs[click.x][click.y].defused === false && MiniGame.bombs[click.x][click.y].dead === false)
			{
				MiniGame.bombs[click.x][click.y].defused = true;
				levelscore += (level + 1) * 5 * Math.trunc(MiniGame.bombs[click.x][click.y].time);
			}
		}
	}
	
	that.NextLevel = function(){
		SpawnBombs(level * 3 + 6);
		endflag = false;
		MiniGame.Countdown.startplusrestore();
	}
	
	that.EndLevel = function(){
		++level;
		MiniGame.ScoreManager.PushScore(levelscore, leveltime, level);
		MiniGame.totalscore += levelscore;
		MiniGame.totaltime += Math.trunc(leveltime);
		levelscore = 0; leveltime + 0;
		MiniGame.ParticleManager.Initialize();
		MiniGame.Pause();
		that.NextLevel();
	}
	
	that.Initialize = function(){
		level = 0;
		levelscore = 0;
		leveltime = 0;
		that.NextLevel();
	}
	return that;
}())