#include "stdafx.h"
extern int FallingBlockx;
extern int FallingBlocky;
extern int FallingRotation;
extern int RotateEvent;
extern int BlockType;
extern int Xmovement;
extern int GameGrid[23][12];
bool Collision = false;
bool Collision1 = false;
int Destinationy = 0;
int Destinationx = 0;
int DestinationRotation = 0;
extern int Frame;

void MoveBlocksDown(int x, int y)
{
	do
	{
		GameGrid[y][x] = GameGrid [y-1][x];
		x=x+1;
		if(x == 10)
		{
			y=y-1;
			x = 1;
		}
	}
	while(y > 0);


}

void ClearLines()
{
	int x = 1;
	int y = 21;
	do
	{
		if(GameGrid[y][x] == 0)
		{
			y = y - 1;
			x=1;
		}
		else
		{
			x = x + 1;
			if(x > 10)
			{
				x = 1;
				int x2 = x;
				do
				{
					GameGrid[y][x2] = 0;
					x2 = x2 +1;
				}
				while(x2 < 10);
				MoveBlocksDown(x, y);

			}
		}

	}
	while(y > 0);

}

bool CheckDestination()
{
	switch(BlockType)
	{
		case 1:
			if(GameGrid[Destinationy][Destinationx] != 0)
			return true;
			if(GameGrid[Destinationy][Destinationx-1] != 0)
			return true;
			if(GameGrid[Destinationy+1][Destinationx] != 0)
			return true;
			if(GameGrid[Destinationy+1][Destinationx-1] != 0)
			return true;
		break;
		case 2:  //Line Piece //Line Piece
			if(DestinationRotation == 1 || DestinationRotation == 3)
			{
			if(GameGrid[Destinationy][Destinationx] != 0)
			return true;
			if(GameGrid[Destinationy][Destinationx-1] != 0)
			return true;
			if(GameGrid[Destinationy][Destinationx-2] != 0)
			return true;
			if(GameGrid[Destinationy][Destinationx-3] != 0)
			return true;
			}
			if (DestinationRotation == 0 || DestinationRotation == 2)
			{

			if(GameGrid[Destinationy][Destinationx] != 0)
			return true;
			if(GameGrid[Destinationy+1][Destinationx] != 0)
			return true;
			if(GameGrid[Destinationy+2][Destinationx] != 0)
			return true;
			if(GameGrid[Destinationy+3][Destinationx] != 0)
			return true;
			}

		break;
		case 3:  //S Piece // S Piece
			if(DestinationRotation == 1 || DestinationRotation == 3)
			{
			if(GameGrid[Destinationy][Destinationx] != 0)
			return true;
			if(GameGrid[Destinationy][Destinationx+1] != 0)
			return true;
			if(GameGrid[Destinationy+1][Destinationx] != 0)
			return true;
			if(GameGrid[Destinationy+1][Destinationx-1] != 0)
			return true;
			}
			if (DestinationRotation == 0 || DestinationRotation == 2)
			{
			if(GameGrid[Destinationy][Destinationx] != 0)
			return true;
			if(GameGrid[Destinationy-1][Destinationx] != 0)
			return true;
			if(GameGrid[Destinationy][Destinationx+1] != 0)
			return true;
			if(GameGrid[Destinationy+1][Destinationx+1] != 0)
			return true;
			}
		break;
		case 4:  //Z Piece // Z Piece
			if(DestinationRotation == 1 || DestinationRotation == 3)
			{
			if(GameGrid[Destinationy][Destinationx] != 0)
			return true;
			if(GameGrid[Destinationy+1][Destinationx] != 0)
			return true;
			if(GameGrid[Destinationy][Destinationx+1] != 0)
			return true;
			if(GameGrid[Destinationy-1][Destinationx+1] != 0)
			return true;
			}
			if (DestinationRotation == 0 || DestinationRotation == 2)
			{
			if(GameGrid[Destinationy][Destinationx] != 0)
			return true;
			if(GameGrid[Destinationy+1][Destinationx] != 0)
			return true;
			if(GameGrid[Destinationy][Destinationx-1] != 0)
			return true;
			if(GameGrid[Destinationy+1][Destinationx+1] != 0)
			return true;
			}
		break;
		case 5:  //L Piece // L Piece
			if(DestinationRotation == 0)
			{
			if(GameGrid[Destinationy][Destinationx] != 0)
			return true;
			if(GameGrid[Destinationy][Destinationx+1] != 0)
			return true;
			if(GameGrid[Destinationy][Destinationx-1] != 0)
			return true;
			if(GameGrid[Destinationy+1][Destinationx-1] != 0)
			return true;
			}
			if (DestinationRotation == 1)
			{
			if(GameGrid[Destinationy][Destinationx] != 0)
			return true;
			if(GameGrid[Destinationy-1][Destinationx] != 0)
			return true;
			if(GameGrid[Destinationy+1][Destinationx+1] != 0)
			return true;
			if(GameGrid[Destinationy+1][Destinationx] != 0)
			return true;
			}
			if(DestinationRotation == 2)
			{
			if(GameGrid[Destinationy][Destinationx] != 0)
			return true;
			if(GameGrid[Destinationy][Destinationx-1] != 0)
			return true;
			if(GameGrid[Destinationy][Destinationx+1] != 0)
			return true;
			if(GameGrid[Destinationy-1][Destinationx+1] != 0)
			return true;
			}
			if (DestinationRotation == 3)
			{
			if(GameGrid[Destinationy][Destinationx] != 0)
			return true;
			if(GameGrid[Destinationy-1][Destinationx] != 0)
			return true;
			if(GameGrid[Destinationy-1][Destinationx-1] != 0)
			return true;
			if(GameGrid[Destinationy+1][Destinationx] != 0)
			return true;
			}
		break;
		case 6:  //J Piece // J Piece
			if(DestinationRotation == 0)
			{
			if(GameGrid[Destinationy][Destinationx] != 0)
			return true;
			if(GameGrid[Destinationy][Destinationx+1] != 0)
			return true;
			if(GameGrid[Destinationy][Destinationx-1] != 0)
			return true;
			if(GameGrid[Destinationy+1][Destinationx+1] != 0)
			return true;
			}
			if (DestinationRotation == 1)
			{
			if(GameGrid[Destinationy][Destinationx] != 0)
			return true;
			if(GameGrid[Destinationy-1][Destinationx] != 0)
			return true;
			if(GameGrid[Destinationy+1][Destinationx] != 0)
			return true;
			if(GameGrid[Destinationy-1][Destinationx+1] != 0)
			return true;
			}
			if(DestinationRotation == 2)
			{
			if(GameGrid[Destinationy][Destinationx] != 0)
			return true;
			if(GameGrid[Destinationy][Destinationx-1] != 0)
			return true;
			if(GameGrid[Destinationy][Destinationx+1] != 0)
			return true;
			if(GameGrid[Destinationy-1][Destinationx-1] != 0)
			return true;
			}
			if (DestinationRotation == 3)
			{
			if(GameGrid[Destinationy][Destinationx] != 0)
			return true;
			if(GameGrid[Destinationy-1][Destinationx] != 0)
			return true;
			if(GameGrid[Destinationy+1][Destinationx-1] != 0)
			return true;
			if(GameGrid[Destinationy+1][Destinationx] != 0)
			return true;
			}
		break;
		case 7:  //T Piece// T Piece
			if(DestinationRotation == 0)
			{
			if(GameGrid[Destinationy][Destinationx] != 0)
			return true;
			if(GameGrid[Destinationy][Destinationx+1] != 0)
			return true;
			if(GameGrid[Destinationy][Destinationx-1] != 0)
			return true;
			if(GameGrid[Destinationy+1][Destinationx] != 0)
			return true;
			}
			if (DestinationRotation == 1)
			{
			if(GameGrid[Destinationy][Destinationx] != 0)
			return true;
			if(GameGrid[Destinationy+1][Destinationx] != 0)
			return true;
			if(GameGrid[Destinationy-1][Destinationx] != 0)
			return true;
			if(GameGrid[Destinationy][Destinationx+1] != 0)
			return true;
			}
			if(DestinationRotation == 2)
			{
			if(GameGrid[Destinationy][Destinationx] != 0)
			return true;
			if(GameGrid[Destinationy][Destinationx+1] != 0)
			return true;
			if(GameGrid[Destinationy][Destinationx-1] != 0)
			return true;
			if(GameGrid[Destinationy-1][Destinationx] != 0)
			return true;
			}
			if (DestinationRotation == 3)
			{
			if(GameGrid[Destinationy][Destinationx] != 0)
			return true;
			if(GameGrid[Destinationy-1][Destinationx] != 0)
			return true;
			if(GameGrid[Destinationy+1][Destinationx] != 0)
			return true;
			if(GameGrid[Destinationy][Destinationx-1] != 0)
			return true;
			}
		break;
		default:
			return false;
		break;
	}
}

void CheckXmovement()
{
	if(Xmovement == 1)
	{
		switch(BlockType)
		{
				case 1: //Square
					if(GameGrid[FallingBlockx + Xmovement][FallingBlocky] != 0)
						Xmovement = 0;
					if(GameGrid[FallingBlockx + Xmovement][FallingBlocky+1] != 0)
						Xmovement = 0;
	
				break;
				case 2:  //Line Piece
					if(FallingRotation == 1 || FallingRotation == 3)
					{
						if(GameGrid[FallingBlockx + Xmovement][FallingBlocky] != 0)
						Xmovement = 0;
					}
					if (FallingRotation == 0 || FallingRotation == 2)
					{
					if(GameGrid[FallingBlockx + Xmovement][FallingBlocky] != 0)
						Xmovement = 0;
					if(GameGrid[FallingBlockx + Xmovement][FallingBlocky+1] != 0)
						Xmovement = 0;
					if(GameGrid[FallingBlockx + Xmovement][FallingBlocky-1] != 0)
						Xmovement = 0;
					if(GameGrid[FallingBlockx + Xmovement][FallingBlocky+2] != 0)
						Xmovement = 0;
					}
				break;

				case 3:  //S Piece
					if(FallingRotation == 1 || FallingRotation == 3)
					{
					if(GameGrid[FallingBlockx + Xmovement][FallingBlocky] != 0)
						Xmovement = 0;
					if(GameGrid[FallingBlockx + Xmovement+1][FallingBlocky+1] != 0)
						Xmovement = 0;
					}
					if (FallingRotation == 0 || FallingRotation == 2)
					{
					if(GameGrid[FallingBlockx + Xmovement+1][FallingBlocky] != 0)
						Xmovement = 0;
					if(GameGrid[FallingBlockx + Xmovement+1][FallingBlocky-1] != 0)
						Xmovement = 0;
					if(GameGrid[FallingBlockx + Xmovement][FallingBlocky+1] != 0)
						Xmovement = 0;
					}
	
				break;

				case 4:  //Z Piece
					if(FallingRotation == 1 || FallingRotation == 3)
					{
					if(GameGrid[FallingBlockx + Xmovement][FallingBlocky] != 0)
						Xmovement = 0;
					if(GameGrid[FallingBlockx + Xmovement+1][FallingBlocky+1] != 0)
						Xmovement = 0;
					}
					if (FallingRotation == 0 || FallingRotation == 2)
					{
					if(GameGrid[FallingBlockx + Xmovement][FallingBlocky+1] != 0)
						Xmovement = 0;
					if(GameGrid[FallingBlockx + Xmovement+1][FallingBlocky-1] != 0)
						Xmovement = 0;
					if(GameGrid[FallingBlockx + Xmovement+1][FallingBlocky] != 0)
						Xmovement = 0;
					}
				break;

				case 5:  //L Piece
					if(FallingRotation == 0)
					{
					if(GameGrid[FallingBlockx + Xmovement-1][FallingBlocky+1] != 0)
						Xmovement = 0;
					if(GameGrid[FallingBlockx + Xmovement+1][FallingBlocky] != 0)
						Xmovement = 0;
					}
					if (FallingRotation == 1)
					{
					if(GameGrid[FallingBlockx + Xmovement][FallingBlocky] != 0)
						Xmovement = 0;
					if(GameGrid[FallingBlockx + Xmovement][FallingBlocky-1] != 0)
						Xmovement = 0;
					if(GameGrid[FallingBlockx + Xmovement+1][FallingBlocky+1] != 0)
						Xmovement = 0;
					}
					if(FallingRotation == 2)
					{
					if(GameGrid[FallingBlockx + Xmovement+1][FallingBlocky] != 0)
						Xmovement = 0;
					if(GameGrid[FallingBlockx + Xmovement+1][FallingBlocky-1] != 0)
						Xmovement = 0;
					}
					if (FallingRotation == 3)
					{
					if(GameGrid[FallingBlockx + Xmovement][FallingBlocky-1] != 0)
						Xmovement = 0;
					if(GameGrid[FallingBlockx + Xmovement][FallingBlocky] != 0)
						Xmovement = 0;
					if(GameGrid[FallingBlockx + Xmovement][FallingBlocky+1] != 0)
						Xmovement = 0;

					}
				break;

				case 6:  //J Piece
					if(FallingRotation == 0)
					{
					if(GameGrid[FallingBlockx + Xmovement+1][FallingBlocky] != 0)
						Xmovement = 0;
					if(GameGrid[FallingBlockx + Xmovement+1][FallingBlocky+1] != 0)
						Xmovement = 0;
					}
					if (FallingRotation == 1)
					{
					if(GameGrid[FallingBlockx + Xmovement][FallingBlocky] != 0)
						Xmovement = 0;
					if(GameGrid[FallingBlockx + Xmovement][FallingBlocky+1] != 0)
						Xmovement = 0;
					if(GameGrid[FallingBlockx + Xmovement+1][FallingBlocky-1] != 0)
						Xmovement = 0;
					}
					if(FallingRotation == 2)
					{
					if(GameGrid[FallingBlockx + Xmovement+1][FallingBlocky] != 0)
						Xmovement = 0;
					if(GameGrid[FallingBlockx + Xmovement-1][FallingBlocky-1] != 0)
						Xmovement = 0;
					}
					if (FallingRotation == 3)
					{
					if(GameGrid[FallingBlockx + Xmovement][FallingBlocky] != 0)
						Xmovement = 0;
					if(GameGrid[FallingBlockx + Xmovement][FallingBlocky-1] != 0)
						Xmovement = 0;
					if(GameGrid[FallingBlockx + Xmovement][FallingBlocky+1] != 0)
						Xmovement = 0;
					}
				break;

					case 7:  //T Piece
					if(FallingRotation == 0)
					{
					if(GameGrid[FallingBlockx + Xmovement+1][FallingBlocky] != 0)
						Xmovement = 0;
					if(GameGrid[FallingBlockx + Xmovement][FallingBlocky+1] != 0)
						Xmovement = 0;
					}
					if (FallingRotation == 1)
					{
					if(GameGrid[FallingBlockx + Xmovement+1][FallingBlocky] != 0)
						Xmovement = 0;
					if(GameGrid[FallingBlockx + Xmovement][FallingBlocky-1] != 0)
						Xmovement = 0;
					if(GameGrid[FallingBlockx + Xmovement][FallingBlocky+1] != 0)
						Xmovement = 0;
					}
					if(FallingRotation == 2)
					{
					if(GameGrid[FallingBlockx + Xmovement+1][FallingBlocky] != 0)
						Xmovement = 0;
					if(GameGrid[FallingBlockx + Xmovement][FallingBlocky-1] != 0)
						Xmovement = 0;
					}
					if (FallingRotation == 3)
					{
					if(GameGrid[FallingBlockx + Xmovement][FallingBlocky] != 0)
						Xmovement = 0;
					if(GameGrid[FallingBlockx + Xmovement][FallingBlocky-1] != 0)
						Xmovement = 0;
					if(GameGrid[FallingBlockx + Xmovement][FallingBlocky+1] != 0)
						Xmovement = 0;
					}
				break;

				default:
					break;
		}
		if(Xmovement == -1)
		{
			switch(BlockType)
			{
				case 1: //Square
					if(GameGrid[FallingBlockx + Xmovement -1][FallingBlocky] != 0)
						Xmovement = 0;
					if(GameGrid[FallingBlockx + Xmovement -1][FallingBlocky+1] != 0)
						Xmovement = 0;
	
				break;
				case 2:  //Line Piece
					if(FallingRotation == 1 || FallingRotation == 3)
					{
					if(GameGrid[FallingBlockx + Xmovement -2][FallingBlocky] != 0)
						Xmovement = 0;
					}
					if (FallingRotation == 0 || FallingRotation == 2)
					{
					if(GameGrid[FallingBlockx + Xmovement][FallingBlocky-1] != 0)
						Xmovement = 0;
					if(GameGrid[FallingBlockx + Xmovement][FallingBlocky+1] != 0)
						Xmovement = 0;
					if(GameGrid[FallingBlockx + Xmovement][FallingBlocky+2] != 0)
						Xmovement = 0;
					if(GameGrid[FallingBlockx + Xmovement][FallingBlocky] != 0)
						Xmovement = 0;
					}
				break;

				case 3:  //S Piece
					if(FallingRotation == 1 || FallingRotation == 3)
					{
					if(GameGrid[FallingBlockx + Xmovement-1][FallingBlocky+1] != 0)
						Xmovement = 0;
					if(GameGrid[FallingBlockx + Xmovement][FallingBlocky] != 0)
						Xmovement = 0;
					}
					if (FallingRotation == 0 || FallingRotation == 2)
					{
					if(GameGrid[FallingBlockx + Xmovement][FallingBlocky] != 0)
						Xmovement = 0;
					if(GameGrid[FallingBlockx + Xmovement][FallingBlocky-1] != 0)
						Xmovement = 0;
					if(GameGrid[FallingBlockx + Xmovement+1][FallingBlocky+1] != 0)
						Xmovement = 0;
					}
	
				break;

				case 4:  //Z Piece
					if(FallingRotation == 1 || FallingRotation == 3)
					{
					if(GameGrid[FallingBlockx + Xmovement-1][FallingBlocky] != 0)
						Xmovement = 0;
					if(GameGrid[FallingBlockx + Xmovement][FallingBlocky+1] != 0)
						Xmovement = 0;
					}
					if (FallingRotation == 0 || FallingRotation == 2)
					{
					if(GameGrid[FallingBlockx + Xmovement][FallingBlocky] != 0)
						Xmovement = 0;
					if(GameGrid[FallingBlockx + Xmovement][FallingBlocky+1] != 0)
						Xmovement = 0;
					if(GameGrid[FallingBlockx + Xmovement+1][FallingBlocky-1] != 0)
						Xmovement = 0;
					}
				break;

				case 5:  //L Piece
					if(FallingRotation == 0)
					{
					if(GameGrid[FallingBlockx + Xmovement-1][FallingBlocky] != 0)
						Xmovement = 0;
					if(GameGrid[FallingBlockx + Xmovement-1][FallingBlocky+1] != 0)
						Xmovement = 0;
					}
					if (FallingRotation == 1)
					{
					if(GameGrid[FallingBlockx + Xmovement][FallingBlocky-1] != 0)
						Xmovement = 0;
					if(GameGrid[FallingBlockx + Xmovement][FallingBlocky] != 0)
						Xmovement = 0;
					if(GameGrid[FallingBlockx + Xmovement][FallingBlocky+1] != 0)
						Xmovement = 0;

					}
					if(FallingRotation == 2)
					{
					if(GameGrid[FallingBlockx + Xmovement -1][FallingBlocky] != 0)
						Xmovement = 0;
					if(GameGrid[FallingBlockx + Xmovement+1][FallingBlocky-1] != 0)
						Xmovement = 0;
					}
					if (FallingRotation == 3)
					{
					if(GameGrid[FallingBlockx + Xmovement][FallingBlocky] != 0)
						Xmovement = 0;
					if(GameGrid[FallingBlockx + Xmovement-1][FallingBlocky-1] != 0)
						Xmovement = 0;
					if(GameGrid[FallingBlockx + Xmovement][FallingBlocky+1] != 0)
						Xmovement = 0;
					}
				break;

				case 6:  //J Piece
					if(FallingRotation == 0)
					{
					if(GameGrid[FallingBlockx + Xmovement-1][FallingBlocky] != 0)
						Xmovement = 0;
					if(GameGrid[FallingBlockx + Xmovement+1][FallingBlocky+1] != 0)
						Xmovement = 0;
					}
					if (FallingRotation == 1)
					{
					if(GameGrid[FallingBlockx + Xmovement][FallingBlocky] != 0)
						Xmovement = 0;
					if(GameGrid[FallingBlockx + Xmovement][FallingBlocky+1] != 0)
						Xmovement = 0;
					if(GameGrid[FallingBlockx + Xmovement][FallingBlocky-1] != 0)
						Xmovement = 0;
					}
					if(FallingRotation == 2)
					{
					if(GameGrid[FallingBlockx + Xmovement-1][FallingBlocky] != 0)
						Xmovement = 0;
					if(GameGrid[FallingBlockx + Xmovement-1][FallingBlocky-1] != 0)
						Xmovement = 0;
					}
					if (FallingRotation == 3)
					{
					if(GameGrid[FallingBlockx + Xmovement][FallingBlocky] != 0)
						Xmovement = 0;
					if(GameGrid[FallingBlockx + Xmovement][FallingBlocky-1] != 0)
						Xmovement = 0;
					if(GameGrid[FallingBlockx + Xmovement-1][FallingBlocky+1] != 0)
						Xmovement = 0;
					}
				break;

					case 7:  //T Piece
					if(FallingRotation == 0)
					{
					if(GameGrid[FallingBlockx + Xmovement-1][FallingBlocky] != 0)
						Xmovement = 0;
					if(GameGrid[FallingBlockx + Xmovement][FallingBlocky+1] != 0)
						Xmovement = 0;
					}
					if (FallingRotation == 1)
					{
					if(GameGrid[FallingBlockx + Xmovement][FallingBlocky] != 0)
						Xmovement = 0;
					if(GameGrid[FallingBlockx + Xmovement][FallingBlocky-1] != 0)
						Xmovement = 0;
					if(GameGrid[FallingBlockx + Xmovement][FallingBlocky+1] != 0)
						Xmovement = 0;
					}
					if(FallingRotation == 2)
					{
					if(GameGrid[FallingBlockx + Xmovement -1][FallingBlocky] != 0)
						Xmovement = 0;
					if(GameGrid[FallingBlockx + Xmovement][FallingBlocky-1] != 0)
						Xmovement = 0;
					}
					if (FallingRotation == 3)
					{
					if(GameGrid[FallingBlockx + Xmovement-1][FallingBlocky] != 0)
						Xmovement = 0;
					if(GameGrid[FallingBlockx + Xmovement][FallingBlocky-1] != 0)
						Xmovement = 0;
					if(GameGrid[FallingBlockx + Xmovement][FallingBlocky+1] != 0)
						Xmovement = 0;
					}
				break;

				default:
					break;
			}



		}
	}
}

bool CheckCollision()
{
	switch(BlockType)
	{
			case 1: //Square
				if(GameGrid[FallingBlocky+2][FallingBlockx] != 0)
					return true;
				if(GameGrid[FallingBlocky+2][FallingBlockx-1] != 0)
					return true;
			break;
			case 2:  //Line Piece
				if(FallingRotation == 1 || FallingRotation == 3)
				{
					if(GameGrid[FallingBlocky+1][FallingBlockx] != 0)
					return true;
					if(GameGrid[FallingBlocky+1][FallingBlockx+1] != 0)
					return true;
					if(GameGrid[FallingBlocky+1][FallingBlockx-1] != 0)
					return true;
					if(GameGrid[FallingBlocky+1][FallingBlockx-2] != 0)
					return true;
				}
				if (FallingRotation == 0 || FallingRotation == 2)
				{
					if(GameGrid[FallingBlocky+3][FallingBlockx] != 0)
					return true;
				}
			break;

			case 3:  //S Piece
				if(FallingRotation == 1 || FallingRotation == 3)
				{
					if(GameGrid[FallingBlockx+1][FallingBlocky] != 0)
						return true;
					if(GameGrid[FallingBlockx-1][FallingBlocky+1] != 0)
						return true;
					if(GameGrid[FallingBlockx][FallingBlocky+1] != 0)
						return true;

				}
				if (FallingRotation == 0 || FallingRotation == 2)
				{
					if(GameGrid[FallingBlockx][FallingBlocky+1] != 0)
						return true;
					if(GameGrid[FallingBlockx+1][FallingBlocky] != 0)
						return true;
				}
	
			break;

			case 4:  //Z Piece
				if(FallingRotation == 1 || FallingRotation == 3)
				{
					if(GameGrid[FallingBlockx][FallingBlocky+1] != 0)
						return true;
					if(GameGrid[FallingBlockx-1][FallingBlocky] != 0)
						return true;
					if(GameGrid[FallingBlockx+1][FallingBlocky+1] != 0)
						return true;
				}
				if (FallingRotation == 0 || FallingRotation == 2)
				{
					if(GameGrid[FallingBlockx][FallingBlocky+1] != 0)
						return true;
					if(GameGrid[FallingBlockx+1][FallingBlocky] != 0)
						return true;
				}
			break;

			case 5:  //L Piece
				if(FallingRotation == 0)
				{
					if(GameGrid[FallingBlockx-1][FallingBlocky+1] != 0)
						return true;
					if(GameGrid[FallingBlockx][FallingBlocky] != 0)
						return true;
					if(GameGrid[FallingBlockx+1][FallingBlocky] != 0)
						return true;
				}
				if (FallingRotation == 1)
				{
					if(GameGrid[FallingBlockx][FallingBlocky+1] != 0)
						return true;
					if(GameGrid[FallingBlockx+1][FallingBlocky+1] != 0)
						return true;
				}
				if(FallingRotation == 2)
				{
					if(GameGrid[FallingBlockx][FallingBlocky] != 0)
						return true;
					if(GameGrid[FallingBlockx-1][FallingBlocky] != 0)
						return true;
					if(GameGrid[FallingBlockx+1][FallingBlocky] != 0)
						return true;

				}
				if (FallingRotation == 3)
				{
					if(GameGrid[FallingBlockx][FallingBlocky+1] != 0)
						return true;
					if(GameGrid[FallingBlockx-1][FallingBlocky-1] != 0)
						return true;
				}
			break;

			case 6:  //J Piece
				if(FallingRotation == 0)
				{
					if(GameGrid[FallingBlockx][FallingBlocky] != 0)
						return true;
					if(GameGrid[FallingBlockx-1][FallingBlocky] != 0)
						return true;
					if(GameGrid[FallingBlockx+1][FallingBlocky+1] != 0)
						return true;
				}
				if (FallingRotation == 1)
				{
					if(GameGrid[FallingBlockx][FallingBlocky+1] != 0)
						return true;
					if(GameGrid[FallingBlockx+1][FallingBlocky-1] != 0)
						return true;
				}
				if(FallingRotation == 2)
				{
					if(GameGrid[FallingBlockx-1][FallingBlocky] != 0)
						return true;
					if(GameGrid[FallingBlockx][FallingBlocky] != 0)
						return true;
					if(GameGrid[FallingBlockx+1][FallingBlocky] != 0)
						return true;
				}
				if (FallingRotation == 3)
				{
					if(GameGrid[FallingBlockx][FallingBlocky+1] != 0)
						return true;
					if(GameGrid[FallingBlockx-1][FallingBlocky+1] != 0)
						return true;
				}
			break;

				case 7:  //T Piece
				if(FallingRotation == 0)
				{
					if(GameGrid[FallingBlockx-1][FallingBlocky] != 0)
						return true;
					if(GameGrid[FallingBlockx+1][FallingBlocky] != 0)
						return true;
					if(GameGrid[FallingBlockx][FallingBlocky+1] != 0)
						return true;
				}
				if (FallingRotation == 1)
				{
					if(GameGrid[FallingBlockx+1][FallingBlocky] != 0)
						return true;
					if(GameGrid[FallingBlockx][FallingBlocky+1] != 0)
						return true;
				}
				if(FallingRotation == 2)
				{
					if(GameGrid[FallingBlockx][FallingBlocky] != 0)
						return true;
					if(GameGrid[FallingBlockx-1][FallingBlocky] != 0)
						return true;
					if(GameGrid[FallingBlockx+1][FallingBlocky] != 0)
						return true;
				}
				if (FallingRotation == 3)
				{
					if(GameGrid[FallingBlockx][FallingBlocky+1] != 0)
						return true;
					if(GameGrid[FallingBlockx-1][FallingBlocky] != 0)
						return true;
				}
			break;

			default:
				break;
	}
	return false;
}

bool CheckBoundaries()
{
	switch(BlockType)
	{
			case 1: //Square
				if(FallingBlockx + Xmovement > 10)
					Xmovement = 0;
				if(FallingBlockx + Xmovement <1 )
					Xmovement = 0;
				if(FallingBlocky == 21)
					return true;
			break;
			case 2:  //Line Piece
				if(FallingRotation == 1 || FallingRotation == 3)
				{
					if(FallingBlockx + Xmovement > 9)
						Xmovement = 0;
					if(FallingBlockx + Xmovement < 2 )
						Xmovement = 0;
					if (FallingBlocky == 22)
						return true;
				}
				if (FallingRotation == 0 || FallingRotation == 2)
				{
					if(FallingBlockx + Xmovement > 10)
						Xmovement = 0;
					if(FallingBlockx + Xmovement < 0 )
						Xmovement = 0;
					if (FallingBlocky == 2)
						return true;
				}
			break;

			case 3:  //S Piece
				if(FallingRotation == 1 || FallingRotation == 3)
				{
					if(FallingBlockx + Xmovement > 9)
						Xmovement = 0;
					if(FallingBlockx + Xmovement < 1 )
						Xmovement = 0;
					if (FallingBlocky == 21)
						return true;
				}
				if (FallingRotation == 0 || FallingRotation == 2)
				{
					if(FallingBlockx + Xmovement > 9)
						Xmovement = 0;
					if(FallingBlockx + Xmovement < 0 )
						Xmovement = 0;
					if (FallingBlocky == 21)
						return true;
				}
	
			break;

			case 4:  //Z Piece
				if(FallingRotation == 1 || FallingRotation == 3)
				{
					if(FallingBlockx + Xmovement > 9)
						Xmovement = 0;
					if(FallingBlockx + Xmovement < 1 )
						Xmovement = 0;
					if (FallingBlocky == 21)
						return true;
				}
				if (FallingRotation == 0 || FallingRotation == 2)
				{
					if(FallingBlockx + Xmovement > 9)
						Xmovement = 0;
					if(FallingBlockx + Xmovement < 0 )
						Xmovement = 0;
					if (FallingBlocky == 21)
						return true;
				}
			break;

			case 5:  //L Piece
				if(FallingRotation == 0)
				{
					if(FallingBlockx + Xmovement > 9)
						Xmovement = 0;
					if(FallingBlockx + Xmovement < 1 )
						Xmovement = 0;
					if (FallingBlocky == 21)
						return true;
				}
				if (FallingRotation == 1)
				{
					if(FallingBlockx + Xmovement > 9)
						Xmovement = 0;
					if(FallingBlockx + Xmovement < 0 )
						Xmovement = 0;
					if (FallingBlocky == 21)
						return true;
				}
				if(FallingRotation == 2)
				{
					if(FallingBlockx + Xmovement > 9)
						Xmovement = 0;
					if(FallingBlockx + Xmovement < 1 )
						Xmovement = 0;
					if (FallingBlocky == 22)
						return true;
				}
				if (FallingRotation == 3)
				{
					if(FallingBlockx + Xmovement > 10)
						Xmovement = 0;
					if(FallingBlockx + Xmovement < 1 )
						Xmovement = 0;
					if (FallingBlocky == 21)
						return true;
				}
			break;

			case 6:  //J Piece
				if(FallingRotation == 0)
				{
					if(FallingBlockx + Xmovement > 9)
						Xmovement = 0;
					if(FallingBlockx + Xmovement < 1 )
						Xmovement = 0;
					if (FallingBlocky == 21)
						return true;
				}
				if (FallingRotation == 1)
				{
					if(FallingBlockx + Xmovement > 9)
						Xmovement = 0;
					if(FallingBlockx + Xmovement < 0 )
						Xmovement = 0;
					if (FallingBlocky == 21)
						return true;
				}
				if(FallingRotation == 2)
				{
					if(FallingBlockx + Xmovement > 9)
						Xmovement = 0;
					if(FallingBlockx + Xmovement < 1 )
						Xmovement = 0;
					if (FallingBlocky == 22)
						return true;
				}
				if (FallingRotation == 3)
				{
					if(FallingBlockx + Xmovement > 10)
						Xmovement = 0;
					if(FallingBlockx + Xmovement < 1 )
						Xmovement = 0;
					if (FallingBlocky == 21)
						return true;
				}
			break;

				case 7:  //T Piece
				if(FallingRotation == 0)
				{
					if(FallingBlockx + Xmovement > 9)
						Xmovement = 0;
					if(FallingBlockx + Xmovement < 1 )
						Xmovement = 0;
					if (FallingBlocky == 21)
						return true;
				}
				if (FallingRotation == 1)
				{
					if(FallingBlockx + Xmovement > 9)
						Xmovement = 0;
					if(FallingBlockx + Xmovement < 0 )
						Xmovement = 0;
					if (FallingBlocky == 21)
						return true;
				}
				if(FallingRotation == 2)
				{
					if(FallingBlockx + Xmovement > 9)
						Xmovement = 0;
					if(FallingBlockx + Xmovement < 1 )
						Xmovement = 0;
					if (FallingBlocky == 22)
						return true;
				}
				if (FallingRotation == 3)
				{
					if(FallingBlockx + Xmovement > 10)
						Xmovement = 0;
					if(FallingBlockx + Xmovement < 1 )
						Xmovement = 0;
					if (FallingBlocky == 21)
						return true;
				}
			break;

			default:
				break;
	}

	return false;
}

void DrawBlock()
{
	switch(BlockType)
	{
		case 1:
			GameGrid[FallingBlocky][FallingBlockx] = 8;
			GameGrid[FallingBlocky][FallingBlockx-1] = 8;
			GameGrid[FallingBlocky+1][FallingBlockx] = 8;
			GameGrid[FallingBlocky+1][FallingBlockx-1] = 8;
		break;
		case 2:  //Line Piece //Line Piece
			if(FallingRotation == 1 || FallingRotation == 3)
			{

			GameGrid[FallingBlocky][FallingBlockx] = 2;
			GameGrid[FallingBlocky][FallingBlockx-1] = 2;
			GameGrid[FallingBlocky][FallingBlockx-2] = 2;
			GameGrid[FallingBlocky][FallingBlockx-3] = 2;
			}
			if (FallingRotation == 0 || FallingRotation == 2)
			{

			GameGrid[FallingBlocky][FallingBlockx] = 2;
			GameGrid[FallingBlocky+1][FallingBlockx] = 2;
			GameGrid[FallingBlocky+2][FallingBlockx] = 2;
			GameGrid[FallingBlocky+3][FallingBlockx] = 2;
			}

		break;
		case 3:  //S Piece // S Piece
			if(FallingRotation == 1 || FallingRotation == 3)
			{
			GameGrid[FallingBlocky][FallingBlockx] = 5;
			GameGrid[FallingBlocky][FallingBlockx+1] = 5;
			GameGrid[FallingBlocky+1][FallingBlockx] = 5;
			GameGrid[FallingBlocky+1][FallingBlockx-1] = 5;
			}
			if (FallingRotation == 0 || FallingRotation == 2)
			{
			GameGrid[FallingBlocky][FallingBlockx] = 5;
			GameGrid[FallingBlocky-1][FallingBlockx] = 5;
			GameGrid[FallingBlocky][FallingBlockx+1] = 5;
			GameGrid[FallingBlocky+1][FallingBlockx+1] = 5;
			}
		break;
		case 4:  //Z Piece // Z Piece
			if(FallingRotation == 1 || FallingRotation == 3)
			{
			GameGrid[FallingBlocky][FallingBlockx] = 1;
			GameGrid[FallingBlocky+1][FallingBlockx] = 1;
			GameGrid[FallingBlocky][FallingBlockx+1] = 1;
			GameGrid[FallingBlocky-1][FallingBlockx+1] = 1;
			}
			if (FallingRotation == 0 || FallingRotation == 2)
			{
			GameGrid[FallingBlocky][FallingBlockx] = 1;
			GameGrid[FallingBlocky+1][FallingBlockx] = 1;
			GameGrid[FallingBlocky][FallingBlockx-1] = 1;
			GameGrid[FallingBlocky+1][FallingBlockx+1] = 1;
			}
		break;
		case 5:  //L Piece // L Piece
			if(FallingRotation == 0)
			{
			GameGrid[FallingBlocky][FallingBlockx] = 4;
			GameGrid[FallingBlocky][FallingBlockx+1] = 4;
			GameGrid[FallingBlocky][FallingBlockx-1] = 4;
			GameGrid[FallingBlocky+1][FallingBlockx-1] = 4;
			}
			if (FallingRotation == 1)
			{
			GameGrid[FallingBlocky][FallingBlockx] = 4;
			GameGrid[FallingBlocky-1][FallingBlockx] = 4;
			GameGrid[FallingBlocky+1][FallingBlockx+1] = 4;
			GameGrid[FallingBlocky+1][FallingBlockx] = 4;
			}
			if(FallingRotation == 2)
			{
			GameGrid[FallingBlocky][FallingBlockx] = 4;
			GameGrid[FallingBlocky][FallingBlockx-1] = 4;
			GameGrid[FallingBlocky][FallingBlockx+1] = 4;
			GameGrid[FallingBlocky-1][FallingBlockx+1] = 4;
			}
			if (FallingRotation == 3)
			{
			GameGrid[FallingBlocky][FallingBlockx] = 4;
			GameGrid[FallingBlocky-1][FallingBlockx] = 4;
			GameGrid[FallingBlocky-1][FallingBlockx-1] = 4;
			GameGrid[FallingBlocky+1][FallingBlockx] = 4;
			}
		break;
		case 6:  //J Piece // J Piece
			if(FallingRotation == 0)
			{
			GameGrid[FallingBlocky][FallingBlockx] = 6;
			GameGrid[FallingBlocky][FallingBlockx+1] = 6;
			GameGrid[FallingBlocky][FallingBlockx-1] = 6;
			GameGrid[FallingBlocky+1][FallingBlockx+1] = 6;
			}
			if (FallingRotation == 1)
			{
			GameGrid[FallingBlocky][FallingBlockx] = 6;
			GameGrid[FallingBlocky-1][FallingBlockx] = 6;
			GameGrid[FallingBlocky+1][FallingBlockx] = 6;
			GameGrid[FallingBlocky-1][FallingBlockx+1] = 6;
			}
			if(FallingRotation == 2)
			{
			GameGrid[FallingBlocky][FallingBlockx] = 6;
			GameGrid[FallingBlocky][FallingBlockx-1] = 6;
			GameGrid[FallingBlocky][FallingBlockx+1] = 6;
			GameGrid[FallingBlocky-1][FallingBlockx-1] = 6;
			}
			if (FallingRotation == 3)
			{
			GameGrid[FallingBlocky][FallingBlockx] = 6;
			GameGrid[FallingBlocky-1][FallingBlockx] = 6;
			GameGrid[FallingBlocky+1][FallingBlockx-1] = 6;
			GameGrid[FallingBlocky+1][FallingBlockx] = 6;
			}
		break;
		case 7:  //T Piece// T Piece
			if(FallingRotation == 0)
			{
			GameGrid[FallingBlocky][FallingBlockx] = 7;
			GameGrid[FallingBlocky][FallingBlockx+1] = 7;
			GameGrid[FallingBlocky][FallingBlockx-1] = 7;
			GameGrid[FallingBlocky+1][FallingBlockx] = 7;
			}
			if (FallingRotation == 1)
			{
			GameGrid[FallingBlocky][FallingBlockx] = 7;
			GameGrid[FallingBlocky+1][FallingBlockx] = 7;
			GameGrid[FallingBlocky-1][FallingBlockx] = 7;
			GameGrid[FallingBlocky][FallingBlockx+1] = 7;
			}
			if(FallingRotation == 2)
			{
			GameGrid[FallingBlocky][FallingBlockx] = 7;
			GameGrid[FallingBlocky][FallingBlockx+1] = 7;
			GameGrid[FallingBlocky][FallingBlockx-1] = 7;
			GameGrid[FallingBlocky-1][FallingBlockx] = 7;
			}
			if (FallingRotation == 3)
			{
			GameGrid[FallingBlocky][FallingBlockx] = 7;
			GameGrid[FallingBlocky-1][FallingBlockx] = 7;
			GameGrid[FallingBlocky+1][FallingBlockx] = 7;
			GameGrid[FallingBlocky][FallingBlockx-1] = 7;
			}
		break;
		default:
		break;
	}
}

void CleanBlock()
{
	switch(BlockType)
	{
		case 1:
			GameGrid[FallingBlocky][FallingBlockx]  = 0;
			GameGrid[FallingBlocky][FallingBlockx-1]  = 0;
			GameGrid[FallingBlocky+1][FallingBlockx]  = 0;
			GameGrid[FallingBlocky+1][FallingBlockx-1]  = 0;
		break;
		case 2:  //Line Piece //Line Piece
			if(FallingRotation == 1 || FallingRotation == 3)
			{

			GameGrid[FallingBlocky][FallingBlockx]  = 0;
			GameGrid[FallingBlocky][FallingBlockx-1]  = 0;
			GameGrid[FallingBlocky][FallingBlockx-2]  = 0;
			GameGrid[FallingBlocky][FallingBlockx-3]  = 0;
			}
			if (FallingRotation == 0 || FallingRotation == 2)
			{

			GameGrid[FallingBlocky][FallingBlockx]  = 0;
			GameGrid[FallingBlocky+1][FallingBlockx]  = 0;
			GameGrid[FallingBlocky+2][FallingBlockx]  = 0;
			GameGrid[FallingBlocky+3][FallingBlockx]  = 0;
			}

		break;
		case 3:  //S Piece // S Piece
			if(FallingRotation == 1 || FallingRotation == 3)
			{
			GameGrid[FallingBlocky][FallingBlockx]  = 0;
			GameGrid[FallingBlocky][FallingBlockx+1]  = 0;
			GameGrid[FallingBlocky+1][FallingBlockx]  = 0;
			GameGrid[FallingBlocky+1][FallingBlockx-1]  = 0;
			}
			if (FallingRotation == 0 || FallingRotation == 2)
			{
			GameGrid[FallingBlocky][FallingBlockx]  = 0;
			GameGrid[FallingBlocky-1][FallingBlockx]  = 0;
			GameGrid[FallingBlocky][FallingBlockx+1]  = 0;
			GameGrid[FallingBlocky+1][FallingBlockx+1]  = 0;
			}
		break;
		case 4:  //Z Piece // Z Piece
			if(FallingRotation == 1 || FallingRotation == 3)
			{
			GameGrid[FallingBlocky][FallingBlockx]  = 0;
			GameGrid[FallingBlocky+1][FallingBlockx]  = 0;
			GameGrid[FallingBlocky][FallingBlockx+1]  = 0;
			GameGrid[FallingBlocky-1][FallingBlockx+1]  = 0;
			}
			if (FallingRotation == 0 || FallingRotation == 2)
			{
			GameGrid[FallingBlocky][FallingBlockx]  = 0;
			GameGrid[FallingBlocky+1][FallingBlockx]  = 0;
			GameGrid[FallingBlocky][FallingBlockx-1]  = 0;
			GameGrid[FallingBlocky+1][FallingBlockx+1]  = 0;
			}
		break;
		case 5:  //L Piece // L Piece
			if(FallingRotation == 0)
			{
			GameGrid[FallingBlocky][FallingBlockx]  = 0;
			GameGrid[FallingBlocky][FallingBlockx+1]  = 0;
			GameGrid[FallingBlocky][FallingBlockx-1]  = 0;
			GameGrid[FallingBlocky+1][FallingBlockx-1]  = 0;
			}
			if (FallingRotation == 1)
			{
			GameGrid[FallingBlocky][FallingBlockx]  = 0;
			GameGrid[FallingBlocky-1][FallingBlockx]  = 0;
			GameGrid[FallingBlocky+1][FallingBlockx+1]  = 0;
			GameGrid[FallingBlocky+1][FallingBlockx]  = 0;
			}
			if(FallingRotation == 2)
			{
			GameGrid[FallingBlocky][FallingBlockx]  = 0;
			GameGrid[FallingBlocky][FallingBlockx-1]  = 0;
			GameGrid[FallingBlocky][FallingBlockx+1]  = 0;
			GameGrid[FallingBlocky-1][FallingBlockx+1]  = 0;
			}
			if (FallingRotation == 3)
			{
			GameGrid[FallingBlocky][FallingBlockx]  = 0;
			GameGrid[FallingBlocky-1][FallingBlockx]  = 0;
			GameGrid[FallingBlocky-1][FallingBlockx-1]  = 0;
			GameGrid[FallingBlocky+1][FallingBlockx]  = 0;
			}
		break;
		case 6:  //J Piece // J Piece
			if(FallingRotation == 0)
			{
			GameGrid[FallingBlocky][FallingBlockx]  = 0;
			GameGrid[FallingBlocky][FallingBlockx+1]  = 0;
			GameGrid[FallingBlocky][FallingBlockx-1]  = 0;
			GameGrid[FallingBlocky+1][FallingBlockx+1]  = 0;
			}
			if (FallingRotation == 1)
			{
			GameGrid[FallingBlocky][FallingBlockx]  = 0;
			GameGrid[FallingBlocky-1][FallingBlockx]  = 0;
			GameGrid[FallingBlocky+1][FallingBlockx]  = 0;
			GameGrid[FallingBlocky-1][FallingBlockx+1]  = 0;
			}
			if(FallingRotation == 2)
			{
			GameGrid[FallingBlocky][FallingBlockx]  = 0;
			GameGrid[FallingBlocky][FallingBlockx-1]  = 0;
			GameGrid[FallingBlocky][FallingBlockx+1]  = 0;
			GameGrid[FallingBlocky-1][FallingBlockx-1]  = 0;
			}
			if (FallingRotation == 3)
			{
			GameGrid[FallingBlocky][FallingBlockx]  = 0;
			GameGrid[FallingBlocky-1][FallingBlockx]  = 0;
			GameGrid[FallingBlocky+1][FallingBlockx-1]  = 0;
			GameGrid[FallingBlocky+1][FallingBlockx]  = 0;
			}
		break;
		case 7:  //T Piece// T Piece
			if(FallingRotation == 0)
			{
			GameGrid[FallingBlocky][FallingBlockx]  = 0;
			GameGrid[FallingBlocky][FallingBlockx+1]  = 0;
			GameGrid[FallingBlocky][FallingBlockx-1]  = 0;
			GameGrid[FallingBlocky+1][FallingBlockx]  = 0;
			}
			if (FallingRotation == 1)
			{
			GameGrid[FallingBlocky][FallingBlockx]  = 0;
			GameGrid[FallingBlocky+1][FallingBlockx]  = 0;
			GameGrid[FallingBlocky-1][FallingBlockx]  = 0;
			GameGrid[FallingBlocky][FallingBlockx+1]  = 0;
			}
			if(FallingRotation == 2)
			{
			GameGrid[FallingBlocky][FallingBlockx]  = 0;
			GameGrid[FallingBlocky][FallingBlockx+1]  = 0;
			GameGrid[FallingBlocky][FallingBlockx-1]  = 0;
			GameGrid[FallingBlocky-1][FallingBlockx]  = 0;
			}
			if (FallingRotation == 3)
			{
			GameGrid[FallingBlocky][FallingBlockx]  = 0;
			GameGrid[FallingBlocky-1][FallingBlockx]  = 0;
			GameGrid[FallingBlocky+1][FallingBlockx]  = 0;
			GameGrid[FallingBlocky][FallingBlockx-1]  = 0;
			}
		break;
		default:
		break;
	}
}

void NewBlock()
{
	BlockType = rand() % 7 + 1;

		FallingBlocky = 1;
		FallingBlockx = 6;
}

int Logic(int Gamestate)
{
	Uint8 *numkeys = SDL_GetKeyState(NULL);
	if(Frame % 5 == 0)
	{
	CleanBlock();
	if(Frame % 10 == 0)
	{
		DestinationRotation = RotateEvent;
		if(CheckDestination() == true)
		{
			DestinationRotation = FallingRotation;
		}
		else
		{
			FallingRotation = DestinationRotation;
		}
	}
	Destinationx = FallingBlockx + Xmovement;
	Destinationy = FallingBlocky;
	if(CheckDestination() == true)
	{
		Destinationx = FallingBlockx;
	}
	else
	{
		FallingBlockx = Destinationx;
	}

	if(Frame == 30 || numkeys[SDLK_DOWN] == 1 || numkeys[SDLK_s] == 1)
	{
	Destinationy = FallingBlocky +1;
	}

	if(CheckDestination() == true)
	{
		DrawBlock();
		ClearLines();
		NewBlock();
	}
	else
	{
		FallingBlocky = Destinationy;
	}

	Xmovement = 0;

	DrawBlock();
	}


	return Gamestate;
}