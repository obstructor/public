#include "stdafx.h"

	extern SDL_Surface* Screen;
	extern SDL_Surface* MainMenu;
	extern SDL_Surface* Blue;
	extern SDL_Surface* Crystal;
	extern SDL_Surface* Desat;
	extern SDL_Surface* Green;
	extern SDL_Surface* Purple;
	extern SDL_Surface* Red;
	extern SDL_Surface* White;
	extern SDL_Surface* Yellow;
	extern SDL_Surface* Play_Back;
	extern int ScreenSetting;
	extern int GameGrid[23][12];
	
void apply_surface( int x, int y, SDL_Surface* source, SDL_Surface* destination )
{
    SDL_Rect offset;
		 offset.x = x;
		 offset.y = y;
    SDL_BlitSurface( source, NULL, destination, &offset );
}


int Render (int Gamestate)
{
	int squarex = 0;
	int squarey = 0;
	
	if (Gamestate == 1)
	{	
		if(ScreenSetting !=1)
		{
			Screen = NULL;
			Screen = SDL_SetVideoMode( 200, 200, 32, SDL_SWSURFACE );
			ScreenSetting = 1;
		}

		apply_surface(0,0, MainMenu, Screen);

	}
	if (Gamestate == 2)
	{
			
			if(ScreenSetting !=2)
			{
				Screen = NULL;
				Screen = SDL_SetVideoMode( 240, 460, 32, SDL_SWSURFACE );
				ScreenSetting = 2;
			}
			apply_surface( 0, 0, Play_Back , Screen );

			do
			{
				switch(GameGrid[squarey][squarex])
				{
					case 1:
						apply_surface(squarex*20, squarey*20, Blue, Screen) ;
					break;
					case 2:
						apply_surface(squarex*20, squarey*20, Crystal, Screen) ;
					break;
					case 3:
						apply_surface(squarex*20, squarey*20, Desat, Screen) ;
					break;
					case 4:
						apply_surface(squarex*20, squarey*20, Green, Screen) ;
					break;
					case 5:
						apply_surface(squarex*20, squarey*20, Purple, Screen) ;
					break;
					case 6:
						apply_surface(squarex*20, squarey*20, Red, Screen) ;
					break;
					case 7:
						apply_surface(squarex*20, squarey*20, White, Screen) ;
					break;
					case 8:
						apply_surface(squarex*20, squarey*20, Yellow, Screen) ;
					break;

					default:
						break;
				}

				squarex = squarex +1;	

				if(squarex > 11)
				{
					squarex = 0;
					squarey = squarey + 1;
				}

	
			}
			while (squarey < 23);
	

	}

		
		SDL_Flip (Screen);
	return 0;
}