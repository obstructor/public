// Game.cpp : Defines the entry point for the console application.
//
#include "stdafx.h"

int Logic(int);
int Events(int);
int Render(int);
int Gamestate = 2;
int Xmovement = 0;
int RotateEvent = 0;
int ScreenSetting = 0;
int FallingBlockx = 5;
int FallingBlocky = 2;
int FallingRotation = 1;
int BlockType = 1;
int GameGrid[23][12] = {0};
int Frame = 0;
int StartTicks = 0;



	SDL_Surface* Screen = NULL;
	SDL_Surface* MainMenu = NULL;
	SDL_Surface* Blue = NULL;
	SDL_Surface* Crystal = NULL;
	SDL_Surface* Desat = NULL;
	SDL_Surface* Green = NULL;
	SDL_Surface* Purple = NULL;
	SDL_Surface* Red = NULL;
	SDL_Surface* White = NULL;
	SDL_Surface* Yellow = NULL;
	SDL_Surface* Play_Back = NULL;


SDL_Surface* load_image( std::string filename )
{

    SDL_Surface* loadedImage = NULL;
    SDL_Surface* optimizedImage = NULL;
    loadedImage = IMG_Load( filename.c_str() );
    if( loadedImage != NULL )
    {
        optimizedImage = SDL_DisplayFormat( loadedImage );
        SDL_FreeSurface( loadedImage );
    }

	if(loadedImage == NULL)
		return 0;

	if(optimizedImage != NULL)
	{
	 Uint32 colorkey = SDL_MapRGB( optimizedImage->format, 0, 0xFF, 0xFF );
	 SDL_SetColorKey( optimizedImage, SDL_SRCCOLORKEY, colorkey );
	}
    return optimizedImage;
}


int main( int argc, char* args[] ) 
{ 
	Uint8 *numkeys = SDL_GetKeyState(NULL);
	
	if(SDL_Init( SDL_INIT_EVERYTHING ) == - 1)
		{
			return false;
		}

		Screen = SDL_SetVideoMode (800, 800, 32, SDL_SWSURFACE );
		SDL_WM_SetCaption ("Tetris" , NULL);

		MainMenu= load_image ("MainMenu.bmp");
		 Blue = load_image ("BLUE.png");
		 Crystal = load_image ("CRYSTAL.png");
		 Desat = load_image ("DESAT.png");
		 Green = load_image ("GREEN.png");
		 Purple = load_image ("PURPLE.png");
		 Red = load_image ("RED.png");
		 White = load_image ("WHITE.png");
		 Yellow = load_image ("YELLOW.png");
		 Play_Back = load_image("play_back.png");

		 int x = 0;

		 do
		 {
			 
			 GameGrid[22][x] = 3;
			 x = x +1;
		 }
		 while(x < 12);
		 x=0;

		do
		 {
			 
			 GameGrid[x][0] = 3;
			 GameGrid[x][11] =3;

			 x = x +1;
		 }
		 while(x < 22);

		 int TickCount = 0;

	while(Gamestate != 0)
    {
		StartTicks = SDL_GetTicks();

		 Render(Gamestate);

		 Gamestate = Events(Gamestate);

		 Gamestate = Logic(Gamestate);

		 TickCount = SDL_GetTicks() - StartTicks;

		 Frame = Frame + 1;
		 if(Frame == 31)
		 {
			 Frame = 0;
			}

		  if(( TickCount < 1000 / 30 ))
		  {
			  SDL_Delay(( 1000 / 30 ) - TickCount); 
		  }
    }

	
	SDL_FreeSurface(Blue);
	SDL_FreeSurface(Crystal);
	SDL_FreeSurface(Desat);
	SDL_FreeSurface(Green);
	SDL_FreeSurface(Purple);
	SDL_FreeSurface(Red);
	SDL_FreeSurface(White);
	SDL_FreeSurface(Yellow);
	SDL_FreeSurface(Play_Back);
	SDL_FreeSurface(MainMenu);
	SDL_Quit(); 

	return 0;
}