#include "Game.h"

int main(int argc, char* argv[])
{
	unsigned int starttime;
	if (SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		printf("SDL could not initialize! SDL_Error: %s\n", SDL_GetError());
	}
	else
	{
		board GameBoard;
		bool quit = true;
		while (quit)
		{
			//starttime = SDL_GetTicks();
			quit = GameBoard.GameCycle();
			//SDL_Delay((1000/30) - (SDL_GetTicks() - starttime));
		}

	}
	SDL_Quit();
	return 0;
}
