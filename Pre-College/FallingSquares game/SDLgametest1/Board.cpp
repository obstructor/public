#include "Game.h"


void board::ClearBoard()
{
	player = 3;
	framecounter = 0;

	for (int i = 0; i < 15; i++)
	for (int n = 0; n <8; n++)
		theboard[n][i] = 0;
	theboard[player][14] = 1;
	difficulty = 1;
}

bool board::LoadImages()
{
	block = SDL_LoadBMP("Block.bmp");
	imageplayer = SDL_LoadBMP("Player.bmp");
	boardsurface = SDL_LoadBMP("board.bmp");
	mainmenu = SDL_LoadBMP("Mainmenu.bmp");
	screensurface = SDL_GetWindowSurface(window);

	if (block == NULL || imageplayer == NULL || boardsurface == NULL)
	{
		printf("Unable to load image %s! SDL Error:", "0", SDL_GetError());
	}
	return true;
}


board::board()
{
	window = NULL;
	screensurface = NULL;
	block = NULL;
	imageplayer = NULL;
	window = SDL_CreateWindow("SDL Tutorial", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 640, 480, SDL_WINDOW_SHOWN);

	gamestate = 1;

	ClearBoard();
	LoadImages();
}

board::~board()
{
	SDL_FreeSurface(screensurface);
	SDL_FreeSurface(block);
	SDL_FreeSurface(imageplayer);
	SDL_FreeSurface(boardsurface);
	SDL_DestroyWindow(window);
}

bool board::GameCycle()
{
	bool quit = true;
	switch (gamestate)
	{
	case 1:
		quit = MenuEventHandler();
		Render();
		break;
	case 2:
		UpdateBoard();
		quit = EventHandler();
		Render();
		break;
	}
	return quit;
}

