#pragma once
#include <SDL.h>
#include <stdio.h>

class board
{
	int difficulty;
	int framecounter;
	int speed;
	int gamestate;
	unsigned int score;

	void RenderBoard();
	void RenderBlocks();
	void Render();
	bool LoadImages();

	void SpawnTopRow();
	void CleanBottomRow();
	void ClearBoard();
	void MovePlayer(int);
	void DecreaseBlocks();
	bool EventHandler();
	bool MenuEventHandler();
	void UpdateBoard();

	int player;
	int theboard[8][15];

public:


	board();
	bool GameCycle();
	SDL_Window* window;
	SDL_Surface* screensurface;
	SDL_Surface* block;
	SDL_Surface* imageplayer;
	SDL_Surface* boardsurface;
	SDL_Surface* mainmenu;
	SDL_Event eve;

	~board();
};