#include "Game.h"

bool board::EventHandler()
{

	while (SDL_PollEvent(&eve) != 0)
	{
		if (eve.type == SDL_QUIT)
			return false;
		else if (eve.type == SDL_KEYDOWN)
		{
			switch (eve.key.keysym.sym)
			{
			case SDLK_RIGHT:
			case SDLK_d:
				MovePlayer(1);
				break;
			case SDLK_LEFT:
			case SDLK_a:
				MovePlayer(2);
				break;
			case SDLK_UP:
				speed = speed + 10;
				break;
			case SDLK_DOWN:
				if (speed >= 10)
					speed = speed - 10;
				break;
			case SDLK_ESCAPE:
				gamestate = 1;
				break;
			}
		}
	}
	return true;
}

bool board::MenuEventHandler()
{

	while (SDL_PollEvent(&eve) != 0)
	{
		if (eve.type == SDL_QUIT)
			return false;
		else if (eve.type == SDL_MOUSEBUTTONDOWN)
		{
			int x, y;
			SDL_GetMouseState(&x, &y);
			
			if (y > 230)
			{
				return false;
			}
			else if (y < 230)
			{
				gamestate = 2;
			}
		}
	}
	return true;
}