#include "Game.h"
#include <cstdlib>

void swap(int &x, int &y)
{
	int temp = x;
	x = y;
	y = temp;
}

void randomshuffle(int x[])
{
	int z, y;
	for (int i = 0; i < 10; i++)
	{
		z = rand() % 8;
		y = rand() % 8;
		swap(x[z], x[y]);
	}
}

void board::UpdateBoard()
{
	CleanBottomRow();

	if (++framecounter == 30)
		framecounter = 0;
	if (framecounter % 5 == 0)
	{
		if (theboard[player][13] == 2)
		{
			ClearBoard();
		}
		DecreaseBlocks();
	}
	if (score > 100)
	{
		score = 0;
		difficulty++;
	}
	SDL_Delay(speed);
}
void board::CleanBottomRow()
{
	for (int i = 0; i < 8; i++)
	if (theboard[i][14] == 2)
	{
		theboard[i][14] = 0;
		score++;
	}
}

void board::DecreaseBlocks()
{
	for (int i = 14; i > 0; i--)
	{
		for (int n = 7; n >= 0; n--)
		{
			theboard[n][i] = theboard[n][i - 1];
			theboard[n][i - 1] = 0;
		}
	}
	theboard[player][14] = 1;
	if (framecounter % 10)
		SpawnTopRow();
}
void board::SpawnTopRow()
{
	int boardpositions[8] = { 0, 1, 2, 3, 4, 5, 6, 7 };
	randomshuffle(boardpositions);
	for (int i = 0; i < difficulty; i++)
		theboard[boardpositions[i]][0] = 2;

}

void board::MovePlayer(int x)
{
	switch (x)
	{
	case 1:
		if (player < 7)
		{
			theboard[player][14] = 0;
			theboard[++player][14] = 1;

		}
		break;
	case 2:
		if (player > 0)
		{
			theboard[player][14] = 0;
			theboard[--player][14] = 1;
		}
		break;
	}
}