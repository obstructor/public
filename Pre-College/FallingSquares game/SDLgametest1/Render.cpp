#include "Game.h"

void board::Render()
{
	switch (gamestate)
	{
	case 1:
		SDL_BlitSurface(mainmenu, NULL, screensurface, NULL);
		break;
	case 2:
		RenderBoard();
		RenderBlocks();
		break;
	}
	SDL_UpdateWindowSurface(window);
}

void board::RenderBoard()
{
	SDL_BlitSurface(boardsurface, NULL, screensurface, NULL);
}

void board::RenderBlocks()
{
	SDL_Rect srcrect, dstrect;
	dstrect.x = dstrect.y = srcrect.x = srcrect.y = 0;
	dstrect.w = srcrect.w = 20;
	dstrect.h = srcrect.h = 20;

	for (int i = 0; i < 8; i++)
	{
		for (int n = 0; n < 15; n++)
		{
			dstrect.x = i * 20 + 1;
			dstrect.y = n * 20 + 1;
			switch (theboard[i][n])
			{
			case 1:
				SDL_BlitSurface(imageplayer, &srcrect, screensurface, &dstrect);
				break;
			case 2:
				SDL_BlitSurface(block, &srcrect, screensurface, &dstrect);
				break;
			}
		}
	}
}

