// Game.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
const int Quit = 0;
const int Mainmenu = 1;
const int Running = 2;

int GameBoard[3][3] =
{
{10,10,10,},
{10,10,10,},
{10,10,10}
};
int Logic(int);
int Events(int);
int Render(int);
int Player = 1;
int Gamestate = 1;
int ScreenSetting = 0;
	SDL_Surface* O = NULL;
	SDL_Surface* X = NULL;
	SDL_Surface* Board = NULL;
	SDL_Surface* Screen = NULL;
	SDL_Surface* MainMenu = NULL;

	void apply_surface( int x, int y, SDL_Surface* source, SDL_Surface* destination )
{
    SDL_Rect offset;
		 offset.x = x;
		 offset.y = y;
    SDL_BlitSurface( source, NULL, destination, &offset );
}

SDL_Surface* load_image( std::string filename )
{

    SDL_Surface* loadedImage = NULL;
    SDL_Surface* optimizedImage = NULL;
    loadedImage = IMG_Load( filename.c_str() );
    if( loadedImage != NULL )
    {
        optimizedImage = SDL_DisplayFormat( loadedImage );
        SDL_FreeSurface( loadedImage );
    }

	if(loadedImage == NULL)
		return 0;

	if(optimizedImage != NULL)
	{
	 Uint32 colorkey = SDL_MapRGB( optimizedImage->format, 0, 0xFF, 0xFF );
	 SDL_SetColorKey( optimizedImage, SDL_SRCCOLORKEY, colorkey );
	}
    return optimizedImage;
}


int main( int argc, char* args[] ) 
{ 
	
	if(SDL_Init( SDL_INIT_EVERYTHING ) == - 1)
		{
			return false;
		}


		Screen = SDL_SetVideoMode( 200, 200, SCREEN_BPP, SDL_SWSURFACE );
		SDL_WM_SetCaption( "Game Test", NULL );

		Board = load_image ("Board.bmp");
		O = load_image ("O.bmp");
		X = load_image ("X.bmp");
		MainMenu= load_image ("MainMenu.bmp");

		

	while(Gamestate != 0)
    {
		 Render(Gamestate);
		 Gamestate = Logic(Gamestate);
		 Gamestate = Events(Gamestate);

    }

	SDL_FreeSurface(Board);
	SDL_FreeSurface(Screen);
	SDL_FreeSurface(O);
	SDL_FreeSurface(X);

	SDL_Quit(); 
	return 0;
}