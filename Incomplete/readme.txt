Project Colony:

I started this initially with a friend and spent several weeks working full-time on this project.  
My friend dropped the project after a few days and I continued to work on it.  
Although its incomplete, I still think it stands as a testament of my ability to work on a project.

It also shows my style of coding when working on larger projects, instead of small College Projects.