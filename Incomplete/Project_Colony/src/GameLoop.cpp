#include "../inc/all.h"
#include "../inc/GameLoop.h"
#include "../inc/UI_Engine.h"
#include "../inc/game_engine.h"
#include "../inc/event.h"
#include "../inc/Render.h"

void GameLoop()
{
	UI_Manager.quit = false;
	while (UI_Manager.quit == false)
	{
		Event_Manager.Gather_Input();
		UI_Manager.Update();
		Game_Engine.Update();
		Render_Manager.Render();
	}
}