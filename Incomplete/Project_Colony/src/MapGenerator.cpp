#include "../inc/MapGenerator.h"
#include "../inc/game_engine.h"

void MapGenerator::Generate_Map(std::fstream & file_map_gen, Map&map)
{
	Coordinate bounds;
	file_map_gen >> bounds.x;
	file_map_gen >> bounds.y;
	file_map_gen >> bounds.z;
	if(bounds.x > 0 && bounds.y > 0 && bounds.z>0)
		map.resize(bounds.x * bounds.y * bounds.z);

	map.map_chunk_xwidth = bounds.x;
	map.map_chunk_ywidth = bounds.y;
	map.map_chunk_zwidth = bounds.z;
}

void MapGenerator::Generate_Fill(Map& map)
{
	Coordinate position;
	for (int i = 0; i < generation_information[GENITEM_FILL].size(); ++i)
	{
		for (position.y = generation_information[GENITEM_FILL][i].fill_range_top; position.y < generation_information[GENITEM_FILL][i].fill_range_bot; ++position.y)
		{
			for (position.x = 0; position.x < map.map_chunk_xwidth * CHUNK_SIZE; ++position.x)
			{
				for (position.z = 0; position.z < map.map_chunk_zwidth * CHUNK_SIZE; ++position.z)
				{
					map[position] = generation_information[GENITEM_FILL][i].default_entity;
}	}	}	}	}

void MapGenerator::Generate_Layers(Map & map)
{
	Coordinate position;
	int width = 0;
	GenItem current_item;
	for (int i = 0; i < generation_information[GENITEM_LAYER].size(); ++i)
	{
		current_item = generation_information[GENITEM_LAYER][i];
		width = (current_item.layer_range_bot 
			- current_item.layer_range_top);

		for (position.x = 0; position.x < map.map_chunk_xwidth * CHUNK_SIZE; ++position.x)
		{
			for (position.z = 0; position.z < map.map_chunk_zwidth * CHUNK_SIZE; ++position.z)
			{
				double offset_y = noise_generator.GetValue(position.x, 0, position.z);
				position.y = (width / 2) + ((width / 2 ) * offset_y) + current_item.layer_range_top;
				for (int y = current_item.layer_range_top; y < current_item.layer_range_bot; ++y)
				{
					if (y <= position.y)
					{
						map[position] = current_item.default_entity;
					}
					else
					{
						map[position] = current_item.default_entity_bot;
}	}	}	}	}	}

void MapGenerator::Generate_Pockets(Map &map)
{
	Coordinate position;
	GenItem current_item;
	double value = 0;
	noise_generator.SetSeed(++seed);

	for (int i = 0; i < generation_information[GENITEM_POCKET].size(); ++i)
	{
		current_item = generation_information[GENITEM_POCKET][i];
		for (position.y = current_item.pocket_range_top; position.y < current_item.pocket_range_bot; ++position.y)
		{
			for (position.x = 0; position.x < map.map_chunk_xwidth * CHUNK_SIZE; ++position.x)
			{
				for (position.z = 0; position.z < map.map_chunk_zwidth * CHUNK_SIZE; ++position.z)
				{
					value = noise_generator.GetValue(position.x,position.y,position.z);
					if (value >= 0)
					{
						if (map[position].type == current_item.location.type || !current_item.location_bool || current_item.location_all)
						{
							map[position] = Generate_Pockets_Generate_MapEntity(current_item, value);
}	}	}	}	}	}	}

MapEntity MapGenerator::Generate_Pockets_Generate_MapEntity(GenItem& current_item, const int& perlin_value)
{
	if (current_item.default_entity.Flag(PROTO_FLAG_FLUID))
	{
		MapEntity current_entity = current_item.default_entity;
		current_entity.Set_Value(perlin_value * 10);
		return current_entity;
	}
	else if(current_item.default_entity.Flag(PROTO_FLAG_ORE))
	{
		MapEntity current_entity = current_item.default_entity;
		current_entity.Set_Value(perlin_value * 100);
		return current_entity;
	}
	else return current_item.default_entity;
}

void MapGenerator::Generate_Surface(Map & map)
{
	Coordinate co;
	std::vector<Coordinate> surface_map;
	surface_seed = split_layer_seed + 1;

	for (co.x = 0; co.x < map.map_chunk_xwidth * CHUNK_SIZE; ++co.x)
	{
		for (co.z = 0; co.z < map.map_chunk_zwidth * CHUNK_SIZE; ++co.z)
		{
			co.y = 0;
			for (bool done = false; done == false; ++co.y)
			{
				if (map[co].Flag(PROTO_FLAG_TRANSPARENT))
				{
					map[co].Flag(true, ENTITY_OUTSIDE);
				}
				else
				{
					surface_map.push_back(co);
					done = true;
				}
			}
		}
	}
	noise_generator.SetSeed(surface_seed);
	for (int k = 0; k < generation_information[GENITEM_SURFACE].size(); ++k)
	{
		for (int i = 0; i < surface_map.size(); ++i)
		{
			Coordinate cur = surface_map[i];
			double value = noise_generator.GetValue(cur.x, cur.y, cur.z);
			if (value > 0)
			{
				if (map[cur].type == generation_information[GENITEM_SURFACE][k].location.type)
				{
					map[cur] = generation_information[GENITEM_SURFACE][k].default_entity;
				}
				else if (generation_information[GENITEM_SURFACE][k].surface)
				{
					if (cur.y > 0)
					{
						cur.y--;
					}
					map[cur] = generation_information[GENITEM_SURFACE][k].default_entity;
				}
				
			}
		}
	}
	
}

void MapGenerator::Generate_Edges(Map & map)
{
	Coordinate co;
	//top
	co.y = 0;
	for (co.x = 0; co.x < map.map_chunk_xwidth * CHUNK_SIZE; ++co.x)
	{
		for (co.z = 0; co.z < map.map_chunk_zwidth * CHUNK_SIZE; ++co.z)
		{
			map[co].Flag(true, ENTITY_INACTIVE);
		}
	}
	//bottom
	co.y = map.map_chunk_ywidth * CHUNK_SIZE - 1;
	for (co.x = 0; co.x < map.map_chunk_xwidth * CHUNK_SIZE; ++co.x)
	{
		for (co.z = 0; co.z < map.map_chunk_zwidth * CHUNK_SIZE; ++co.z)
		{
			map[co].Flag(true, ENTITY_INACTIVE);
		}
	}

	//x = 0
	co.x = 0;
	for (co.y = 0; co.y < map.map_chunk_ywidth * CHUNK_SIZE; ++co.y)
	{
		for (co.z = 0; co.z < map.map_chunk_zwidth * CHUNK_SIZE; ++co.z)
		{
			map[co].Flag(true, ENTITY_INACTIVE);
		}
	}
	// x = max
	co.x = map.map_chunk_xwidth * CHUNK_SIZE - 1;
	for (co.y = 0; co.y < map.map_chunk_ywidth * CHUNK_SIZE; ++co.y)
	{
		for (co.z = 0; co.z < map.map_chunk_zwidth * CHUNK_SIZE; ++co.z)
		{
			map[co].Flag(true, ENTITY_INACTIVE);
		}
	}
	// z = 0
	co.z = 0;
	for (co.x = 0; co.x < map.map_chunk_xwidth * CHUNK_SIZE; ++co.x)
	{
		for (co.y = 0; co.y < map.map_chunk_ywidth * CHUNK_SIZE; ++co.y)
		{
			map[co].Flag(true, ENTITY_INACTIVE);
		}
	}
	// z = max
	co.z = map.map_chunk_zwidth * CHUNK_SIZE - 1;
	for (co.x = 0; co.x < map.map_chunk_xwidth * CHUNK_SIZE; ++co.x)
	{
		for (co.y = 0; co.y < map.map_chunk_ywidth * CHUNK_SIZE; ++co.y)
		{
			map[co].Flag(true, ENTITY_INACTIVE);
		}
	}
}

void MapGenerator::Parse_Name(GenItem &gen_item)
{
	std::string parser;
	int start = 0, end = 0;
	std::getline(file_map_gen,parser);
	if (parser == "" || parser.length() < 2)
	{
		std::getline(file_map_gen, parser);
	}
	while (parser[start] != '<' && start < parser.length())
	{
		end = ++start;
	}
	while (parser[end] != '>' && end < parser.length())
	{
		++end;
	}
	parser = parser.substr(start + 1, end - start - 1);
	gen_item.default_entity = Lookup_Entity_By_Name(parser);
}

void MapGenerator::Parse_Gen_Attributes(GenItem &item)
{
	std::string parser;
	ParsedString parsed_string;
	int start = 0, end = 0;
	std::getline(file_map_gen, parser);
	while (start < parser.length() && end < parser.length())
	{
		while (parser[start] != '[')
		{
			++start;
		}
		while (parser[end] != ']')
		{
			++end;
		}
		parsed_string.Parse(parser.substr(start + 1, end - start - 1));
		start = ++end;
		if (parsed_string.parsed_string == "pocket")
		{
			item.pocket = true;
			std::stringstream(parsed_string.parsed_value) >> item.pocket_range_top;
			std::stringstream(parsed_string.parsed_value_bot) >> item.pocket_range_bot;

		}
		if (parsed_string.parsed_string == "fill")
		{
			item.fill = true;
			std::stringstream(parsed_string.parsed_value) >> item.fill_range_top;
			std::stringstream(parsed_string.parsed_value_bot) >> item.fill_range_bot;
		}
		if (parsed_string.parsed_string == "location")
		{
			if (parsed_string.parsed_value == "all")
			{
				item.location_bool = true;
				item.location_all = true;
			}
			else if (parsed_string.parsed_value == "outside")
			{
				item.location_bool = true;
				item.surface = true;
			}
			else
			{
				item.location_bool = true;
				item.location = Lookup_Entity_By_Name(parsed_string.parsed_value);
			}
		}
		if (parsed_string.parsed_string == "surface")
		{
			if (parsed_string.parsed_value == "all")
			{
				item.location_bool = true;
				item.location_all = true;
			}
			else
			{
				item.location_bool = true;
				item.location = Lookup_Entity_By_Name(parsed_string.parsed_value);
			}
		}
		if (parsed_string.parsed_string == "frequency")
		{
			if (parsed_string.parsed_value == "high")
			{
				item.frequency = 3;
			}
			if (parsed_string.parsed_value == "medium")
			{
				item.frequency = 2;
			}
			if (parsed_string.parsed_value == "low")
			{
				item.frequency = 1;
			}
		}
		if (parsed_string.parsed_string == "size")
		{
			if (parsed_string.parsed_value == "large")
			{
				item.size = 3;
			}
			if (parsed_string.parsed_value == "medium")
			{
				item.size = 2;
			}
			if (parsed_string.parsed_value == "small")
			{
				item.size = 1;
			}
		}
	}
	
}

void MapGenerator::ParsedString::Parse(std::string parser)
{
	int start = 0, end = 0;
	while (parser[end] != '=' && end < parser.length())
	{
		++end;
	}
	parsed_string = parser.substr(start, end - start);
	start = end + 1;
	while (end < parser.length())
	{
		if (parser[end] == '-')
		{
			parsed_value = parser.substr(start, end - start);
			parsed_value_bot = parser.substr(end + 1, parser.length() - end);
		}
		++end;
	}
}

void MapGenerator::Push_GenItem(GenItem &item)
{
	if (item.fill)
	{
		generation_information[GENITEM_FILL].push_back(item);
	}
	if (item.pocket)
	{
		generation_information[GENITEM_POCKET].push_back(item);
	}
	if (item.surface)
	{
		generation_information[GENITEM_SURFACE].push_back(item);
	}
}

void MapGenerator::Parse_Split_Layers()
{
	for (int i = 0; i < generation_information[GENITEM_FILL].size();++i)
	{
		for (int k = i; k < generation_information[GENITEM_FILL].size(); ++k)
		{
			// Check to see if k's top fill is between i's top and bot or vice versa.	
			if(generation_information[GENITEM_FILL][i].fill_range_top >= generation_information[GENITEM_FILL][k].fill_range_top)
			{
				if (generation_information[GENITEM_FILL][i].fill_range_bot < generation_information[GENITEM_FILL][k].fill_range_top)
				{
					//k's top is within i
					generation_information[GENITEM_LAYER].push_back(GenItem_From_Split_Layers(generation_information[GENITEM_FILL][k], generation_information[GENITEM_FILL][i]));
				}
			}
			else if (generation_information[GENITEM_FILL][k].fill_range_top >= generation_information[GENITEM_FILL][i].fill_range_top)
			{
				if (generation_information[GENITEM_FILL][k].fill_range_bot < generation_information[GENITEM_FILL][i].fill_range_top)
				{
					//i's top is within k
					generation_information[GENITEM_LAYER].push_back(GenItem_From_Split_Layers(generation_information[GENITEM_FILL][k], generation_information[GENITEM_FILL][i]));
}	}	}	}	}

MapGenerator::GenItem MapGenerator::GenItem_From_Split_Layers(GenItem fill_x, GenItem fill_y)
{
	GenItem layer;
	//Find which fill reaches higher or if they are equal
	if (fill_x.fill_range_top > fill_y.fill_range_top)
	{
		layer.random_layer = false;
		layer.default_entity = fill_x.default_entity;
		layer.default_entity_bot = fill_y.default_entity;
		layer.layer_range_top = fill_y.fill_range_top; // y the lower top
	}
	else if (fill_x.fill_range_top < fill_y.fill_range_top)
	{
		layer.random_layer = false;
		layer.default_entity = fill_y.default_entity;
		layer.default_entity_bot = fill_x.default_entity;
		layer.layer_range_top = fill_x.fill_range_top; // x the lower top
	}
	else
	{
		layer.random_layer = false;
		layer.layer_range_top = fill_x.fill_range_top; // both tops are equal
		if (fill_x.fill_range_bot == fill_y.fill_range_bot)
		{
			layer.random_layer = true;
			layer.default_entity = fill_x.default_entity;
			layer.default_entity_bot = fill_y.default_entity; // y reaches further down
			layer.layer_range_top = fill_x.fill_range_top; // both tops are equal
			layer.layer_range_bot = fill_x.fill_range_bot; // both bottom are equal
		}
		else if (fill_x.fill_range_bot > fill_y.fill_range_bot)
		{
			layer.random_layer = false;
			layer.default_entity = fill_x.default_entity;
			layer.default_entity_bot = fill_y.default_entity; // y reaches further down
			layer.layer_range_bot = fill_x.fill_range_bot; // x is higher bottom
		}
		else if (fill_x.fill_range_bot < fill_y.fill_range_bot)
		{
			layer.random_layer = false;
			layer.default_entity = fill_y.default_entity;
			layer.default_entity_bot = fill_x.default_entity; // x reacher further down
			layer.layer_range_bot = fill_y.fill_range_bot; // y is higher bottom
		}
	}

	if (fill_x.fill_range_bot > fill_y.fill_range_bot)
	{
		layer.layer_range_bot = fill_x.fill_range_bot; // x is higher bottom
	}
	else
	{
		layer.layer_range_bot = fill_y.fill_range_bot; // y is higher bottom or they are equal
	}
	// Equal case is handled in first if block.

	return layer;
}

MapEntity MapGenerator::Lookup_Entity_By_Name(std::string name)
{
	for (int i = 0; i < Game_Engine.prototype_list.size(); ++i)
	{
		if (Game_Engine.prototype_list[i].name == name)
		{
			return Game_Engine.prototype_list[i].Default_MapEntity();
		}
	}
	return MapEntity();
}

Map MapGenerator::Generate_Map(int xseed)
{
	seed = xseed;
	noise_generator.SetSeed(seed);
	Map map;
	file_map_gen.open("../resources/settings/mapgeneration");
	generation_information.resize(0);
	generation_information.resize(GENITEM_END);

	if (file_map_gen.is_open())
	{
		Generate_Map(file_map_gen, map);
		while (!file_map_gen.eof())
		{
			GenItem current_item;
			Parse_Name(current_item);
			Parse_Gen_Attributes(current_item);
			Push_GenItem(current_item);
		}
		Parse_Split_Layers();
		
		Generate_Fill(map);
		Generate_Layers(map);
		Generate_Pockets(map);
		Generate_Surface(map);
		Generate_Edges(map);
	}
	return map;
}

MapGenerator::MapGenerator()
{
	seed = split_layer_seed = surface_seed = 1337;
}
