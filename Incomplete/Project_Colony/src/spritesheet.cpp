#include "../inc/Render.h"

SDL_Rect SpriteSheet::Get_Sprite_Rect(int sprite_number)
{
	SDL_Rect sprite;
	if (Valid())
	{
		sprite.w = square_width;
		sprite.h = square_height;

		sprite.y = ((sprite_number * square_width) / sheet_width) * square_height;
		sprite.x = (sprite_number * square_width) % sheet_width;
		if (sprite.y > sheet_height || sprite.x > sheet_width)
		{
			sprite.y = sprite.x = 0;
			Debug("Null sprite rendered", D_VIDEO);
		}
	}
	return sprite;
}

SpriteSheet::SpriteSheet(char * file)
{
	Load(file);
}

SpriteSheet::SpriteSheet(std::string file)
{
	Load(file);
}

SpriteSheet::SpriteSheet()
{
	sheet = NULL;
	valid = false;
	square_width = 0;
	square_height = 0;
	sheet_width = 0;
	sheet_height = 0;
}

bool SpriteSheet::Load(char * file)
{
	std::string filepath = file;
	return Load(filepath);
}

bool SpriteSheet::Load(std::string file)
{
	
	SDL_Surface * loaded = IMG_Load(file.c_str());
	if (loaded != NULL)
	{
		sheet_width = loaded->w;
		sheet_height = loaded->h;
		SDL_DestroyTexture(sheet);
		sheet = SDL_CreateTextureFromSurface(Render_Manager.game_renderer, loaded);
		SDL_FreeSurface(loaded);
		//The squares I drew were 20x20, this should be able to change and should be loaded via settings.
		//Until settings gets set up this magic number will work for now.
		square_width = 20;
		square_height = 20;
		Debug("loaded" + file, D_VIDEO);

		valid = true;
	}
	else
	{
		Debug(SDL_GetError(), D_VIDEO);
		Debug(IMG_GetError(), D_VIDEO);
		sheet = NULL;
		square_width = 0;
		square_height = 0;
		sheet_width = 0;
		sheet_height = 0;
		valid = false;
	}
	
	return valid;
}

bool SpriteSheet::Valid()
{
	return valid;
}
