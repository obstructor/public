#include "../inc/audio.h"

bool AudioManager::Initialize(){
	//Base sound output
	if(SDL_InitSubSystem(SDL_INIT_AUDIO) == -1)
	{
		Debug("SDL Audio failed to load",D_AUDIO);
		return false;
	}
	//Mixer
	if(Mix_OpenAudio( 44100, MIX_DEFAULT_FORMAT,2,1024) == -1){
		Debug("SDL Mixer failed to load", D_AUDIO);
		return false;
	}
	//Ogg decoding
	if(!Mix_Init( MIX_INIT_OGG ) ){
		Debug("SDL Mixer failed ogg loading", D_AUDIO);
		return false;
	}

	if (Initialize_All_Music() && Initialize_All_Sound())
		return true;
	else return false;
}

void AudioManager::Play_Sound(SoundID sound_to_play) {

	//Set volume. If <0, it's ignored.
	if (sound_to_play < loaded_sounds.size())
	{
		Mix_VolumeChunk(loaded_sounds.at(sound_to_play), int(.5*MIX_MAX_VOLUME));

		//Play in the next available (-1) channel
		Mix_PlayChannel(-1, loaded_sounds.at(sound_to_play), 0);
	}
	else return;
}

void AudioManager::Stop_Music(int ms=50){
	//Fade music out over ms milliseconds
	Mix_FadeOutMusic(ms);
}

void AudioManager::Play_Music(MusicID to_play)
{
	//Play music, looping infinite (-1) times
	if (to_play != current_music && to_play < loaded_music.size())
	{
		Mix_FadeInMusic(loaded_music.at(to_play), -1, 50);
		current_music = to_play;
	}
}

void AudioManager::Free(Sound sound){
	Mix_FreeChunk(sound);
}

void AudioManager::Free(Music music){
	Mix_FreeMusic(music);
}
