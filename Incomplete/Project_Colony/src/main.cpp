#include "../inc/all.h"
#include "../inc/InitializeClean.h"
#include "../inc/GameLoop.h"
#include "../inc/constants.h"
#include "../inc/screen.h"
#include "../inc/load.h"
#include "../inc/event.h"
#include "../inc/text.h"
#include "../inc/InitializeClean.h"
#include "../inc/audio.h"

#include "../inc/test.h"

int main(int argc, char* argv[])
{
	std::cout << argv[0] << std::endl;

	if (!Initialize())
	{
		Debug("Error Initializing");
		return 1;
	}


	GameLoop();
	
	Clean();
	return 0;
}