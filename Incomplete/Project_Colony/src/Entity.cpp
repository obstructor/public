#include "../inc/game_engine.h"

Entity::Entity()
{
	flags = 0;
	value = 0;
	type = 0;
}

Entity::Entity(int type_to_be)
{
	if (type_to_be < Game_Engine.prototype_list.size())
	{
		flags = Game_Engine.prototype_list.at(type_to_be).entity_flags_defaults;
		value = Game_Engine.prototype_list.at(type_to_be).value_default;
		type = type_to_be;
	}
}

int Entity::Hp()
{
	return value;
}

int Entity::Temperature()
{
	return temperature;
}

bool Entity::Actor()
{
	return flags.test(ENTITY_ACTOR);
}

bool Entity::Flag(EntityFlag flag)
{
	return flags.test(flag);
}

bool Entity::Flag(PrototypeFlag flag)
{
	return Game_Engine.prototype_list[type].proto_flags.test(flag);
}

bool Entity::Flag(bool flag_value, EntityFlag flag)
{
	return flags[flag] = flag_value;
}

bool Entity::Flag(bool flag_value, PrototypeFlag flag)
{
	// Please try to avoid using this function outside of debugging.
	// Prototypes shouldn't change in the middle of execution,from the entity especially.
	return Game_Engine.prototype_list[type].proto_flags[flag] = flag_value;
}

void Entity::Set_Type(int to_be_type)
{
	if (to_be_type < Game_Engine.prototype_list.size())
	{
		type = to_be_type;
		Set_Defaults();
	}
}

void Entity::Set_Value(int x)
{
	value = x;
}

void Entity::Decrease_Value(int x)
{
	if(!Flag(PROTO_FLAG_MULTIBLOCK))
		value -= x;
}

void Entity::Increase_Value(int x)
{
	if (!Flag(PROTO_FLAG_MULTIBLOCK))
		value += x;
}

void Entity::Set_Defaults()
{
	if (type < Game_Engine.prototype_list.size())
	{
		value = Game_Engine.prototype_list[type].value_default;
		flags = Game_Engine.prototype_list[type].entity_flags_defaults;
	}
}

int Entity::Sprite()
{
	return Game_Engine.prototype_list[type].sprite_number;
}

void Entity::Entity_Constructor()
{
	flags = 0;
	value = 0;
	type = 0;
}

void Entity::Entity_Constructor(int type_to_be)
{
	if (type_to_be < Game_Engine.prototype_list.size())
	{
		flags = Game_Engine.prototype_list.at(type_to_be).entity_flags_defaults;
		value = Game_Engine.prototype_list.at(type_to_be).value_default;
		type = type_to_be;
	}
}

bool Entity::Actor(bool flag)
{
	if (Flag(ENTITY_INACTIVE))
	{
		Flag(false, ENTITY_ACTOR);
		return false;
	}
	else return flags[ENTITY_ACTOR] = flag;
}

MapEntity::MapEntity()
{
	Entity_Constructor();
	container_inside = NULL;
	colonist_inside = NULL;
}

MapEntity::MapEntity(int type_to_be)
{
	Entity_Constructor(type_to_be);
	container_inside = NULL;
	colonist_inside = NULL;
}
