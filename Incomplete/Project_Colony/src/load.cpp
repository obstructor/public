#include "../inc/load.h"

Texture Load_Img(std::string path){
	Texture out;

	//Debug("Loading and converting image: " + path);
	path = "../resources/images/" + path;
	SDL_Surface * temp = IMG_Load(path.c_str() );
	out = SDL_CreateTextureFromSurface(game_renderer, temp);
	std::clog << SDL_GetError();
	SDL_FreeSurface(temp);

	return out;
}

//Load a 16x16 block font image
Font Load_Font(std::string path){
	Font out;
	//load the image
	out.glyphs = Load_Img("fonts/" + path);

	//use the size to determine the individual glyph size
	SDL_QueryTexture(out.glyphs, NULL, NULL, &out.width, &out.height);
	out.width /= 16;
	out.height /= 16;

	return out;
}

