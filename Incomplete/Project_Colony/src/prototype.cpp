#include "../inc/game_engine.h"

void Prototype::Reset()
{
	name = "";
	type = 0;
	sprite_number = 0;
	hardness = -1;
	permeability = -1;
	conductivity = -1;
	viscosity = -1;
	density = -1;
	proto_flags = 0;
	entity_flags_defaults = 0;
	value_default = 0;
}

void Prototype::LoadFlag(std::string flag_value)
{
	if (flag_value == "colonist")
	{
		proto_flags.set(PROTO_FLAG_COLONIST);
	}
	if (flag_value == "solid")
	{
		proto_flags.set(PROTO_FLAG_SOLID);
	}
	if (flag_value == "liquid")
	{
		proto_flags.set(PROTO_FLAG_LIQUID);
	}
	if (flag_value == "gas")
	{
		proto_flags.set(PROTO_FLAG_GAS);
	}
	if (flag_value == "walkable")
	{
		proto_flags.set(PROTO_FLAG_WALKABLE);
	}
	if (flag_value == "swimmable")
	{
		proto_flags.set(PROTO_FLAG_SWIMMABLE);
	}
	if (flag_value == "transparent")
	{
		proto_flags.set(PROTO_FLAG_TRANSPARENT);
	}
	if (flag_value == "piling")
	{
		proto_flags.set(PROTO_FLAG_PILING);
	}
	if (flag_value == "organic")
	{
		proto_flags.set(PROTO_FLAG_ORGANIC);
	}
	if (flag_value == "edible")
	{
		proto_flags.set(PROTO_FLAG_EDIBLE);
	}
	if (flag_value == "conductive")
	{
		proto_flags.set(PROTO_FLAG_CONDUCTIVE);
	}
	if (flag_value == "permeable")
	{
		proto_flags.set(PROTO_FLAG_PERMEABLE);
	}
	if (flag_value == "ore")
	{
		proto_flags.set(PROTO_FLAG_ORE);
	}
	if (flag_value == "metal")
	{
		proto_flags.set(PROTO_FLAG_METAL);
	}
	if (flag_value == "flameable")
	{
		proto_flags.set(PROTO_FLAG_FLAMEABLE);
	}
	if (flag_value == "multiblock")
	{
		proto_flags.set(PROTO_FLAG_MULTIBLOCK);
	}
}

void Prototype::LoadValue(std::string name, int value)
{
	if (name == "hp")
	{
		value_default = value;
	}
	if (name == "sprite")
	{
		sprite_number = value;
	}
	if (name == "hardness")
	{
		hardness = value;
	}
	if (name == "permeability")
	{
		permeability = value;
	}
	if (name == "conductivity")
	{
		conductivity = value;
	}
	if (name == "viscosity")
	{
		viscosity = value;
	}
	if (name == "density")
	{
		density = value;
	}
}

MapEntity Prototype::Default_MapEntity()
{
	MapEntity default_entity(type);
	return default_entity;
}

