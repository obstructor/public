#include "../inc/screen.h"
#include "../inc/game_engine.h"
#include "../inc/UI_Engine.h"
#include "../inc/text.h"
#include "../inc/load.h" // I am against all these headers I will simplify later.

int SCREEN_WIDTH;
int SCREEN_HEIGHT;
int SCREEN_SCALE;

SDL_Window * game_window;
SDL_Renderer * game_renderer;

//framerate limiter and measurer.
int Frame_Limiter(){
	static int oldtime = 0;
	static int newtime = 0;
	static double avg = 1/60.0;

	newtime = SDL_GetTicks();

	//update the average frame time, weighting the current frame at 1/10
	avg = (9*avg + (newtime - oldtime))/10.0;

	//update the time
	oldtime = newtime;

	//calculate the fps from the average frame time (ms)
	int fps = (int)(1000.0/avg + 0.5);

	//output FPS as debugging information every 5 seconds
	static int a=0;
	a++;
	if(a%300 == 0){
		Debug("FPS: "+std::to_string( fps ), D_VIDEO);
	}

	return fps;
}

SDL_Renderer * Open_Renderer(SDL_Window * window){

	SDL_Renderer * a = SDL_CreateRenderer(
		window,
		-1,
		SDL_RENDERER_PRESENTVSYNC
	);

	Debug(SDL_GetError());

	//SDL_RenderSetLogicalSize(a, SCREEN_WIDTH, SCREEN_HEIGHT);

	int x;
	int y;
	SDL_RenderGetLogicalSize(a, &x, &y);
	Debug("Logical size: " + std::to_string(x) + " " + std::to_string(y));


	return a;
}
SDL_Window * Open_Window(){
	return SDL_CreateWindow(
		TITLE,
		SDL_WINDOWPOS_UNDEFINED,
		SDL_WINDOWPOS_UNDEFINED,
		SCREEN_WIDTH,
		SCREEN_HEIGHT,
		SDL_WINDOW_RESIZABLE
	);
}

bool Init_Video() {

	SCREEN_WIDTH = 640;
	SCREEN_HEIGHT = 480;
	SCREEN_SCALE = 1;

	if( SDL_Init(SDL_INIT_VIDEO) < 0 ){
		std::cout << "SDL Init returned this error: " << SDL_GetError();
		return 0;
	}
	else {
		//Nearest Neighbor scaling, since this is a pixelly game.
	SDL_SetHint( SDL_HINT_RENDER_SCALE_QUALITY, "0");
	}
	IMG_Init(IMG_INIT_PNG);

	//set up the context
	game_window = Open_Window();
	game_renderer = Open_Renderer(game_window);


	Resize_Screen(SCREEN_WIDTH, SCREEN_HEIGHT, SCREEN_SCALE);

	SDL_SetRenderDrawColor( game_renderer, 0x88, 0x88, 0x88, 0x88 );
	return 0;
}

//Resize screen to w x h "real" pixels with a specified virtual-real scale
void Resize_Screen(int w, int h, int scale){

	//Force integer scaling
	w -= w % scale;
	h -= h % scale;

	SCREEN_SCALE = scale;
	SCREEN_WIDTH = w/scale;
	SCREEN_HEIGHT = h/scale;

	Debug(
				"Resizing Screen: " +
				std::to_string(SCREEN_WIDTH) + "x" + std::to_string(SCREEN_HEIGHT) + " internal, "
				+ std::to_string(w) + "x" + std::to_string(h) + " physical."
	);

	SDL_SetWindowSize(game_window, w, h);
	SDL_RenderSetLogicalSize(game_renderer, SCREEN_WIDTH, SCREEN_HEIGHT);
}

void Set_Scale(int scale){
	Debug("Setting scale to: "+std::to_string(scale) );
	if(SCREEN_SCALE != scale){
		SCREEN_WIDTH = (SCREEN_WIDTH * SCREEN_SCALE) / scale;
		SCREEN_HEIGHT = (SCREEN_HEIGHT * SCREEN_SCALE) / scale;
		SDL_RenderSetLogicalSize(game_renderer, SCREEN_WIDTH, SCREEN_HEIGHT);
		SCREEN_SCALE = scale;
	}
}

void Clear_Screen(){
	SDL_RenderClear(game_renderer);
}

void Update_Screen(){
	SDL_RenderPresent(game_renderer);
}

void Draw(Texture tex, Rect src, Rect dest, Draw_Hint hint){

	//test to center horizontally
	if( ! (hint & HINT_HORI) ){
		dest.x += (SCREEN_WIDTH - dest.x - dest.w) / 2;
	}

	if( ! (hint & HINT_VERT) ){
		dest.y += (SCREEN_HEIGHT - dest.y - dest.h) / 2;
	}

	if(hint & HINT_RIGHT){
		dest.x = SCREEN_WIDTH - dest.x - dest.w;
	}
	if(hint & HINT_BOTTOM){
		dest.y = SCREEN_HEIGHT - dest.y - dest.h;
	}

	SDL_RenderCopy(game_renderer, tex, &src, &dest);
}

void Draw(Texture tex, int x, int y, Draw_Hint hint, double scale){
	Rect src = Texture_Rect(tex);
	Rect dest = src;

	src.x = 0;
	src.y = 0;

	dest.x = x;
	dest.y = y;
	dest.w = src.w * scale;
	dest.h = src.h * scale;

	Draw(tex,src,dest,hint);

	//SDL_RenderCopy(game_renderer, tex, NULL, &dest);
}

//Return a Rect of the texture's size
Rect Texture_Rect(Texture tex){
	int w, h;
	SDL_ClearError();
	int success = SDL_QueryTexture(tex,NULL, NULL, &w, &h);
	if(success != 0) Debug(SDL_GetError());

	Rect out;
	out.x = 0;
	out.y = 0;
	out.w = w;
	out.h = h;


	return out;
}

//Render-to-texture functions. Call to change render modes.
void Render_To(Texture dest){
	int success = SDL_SetRenderTarget(game_renderer, dest);
	//Debug("Set render to texture.");
}
void Render_To_Screen(){
	SDL_SetRenderTarget(game_renderer, NULL);
	//Debug("Set render to screen.");
}

Texture New_Texture(int width, int height, bool transparent){
	auto out =  SDL_CreateTexture(game_renderer, SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_TARGET, width, height);
	if(transparent) {
		SDL_SetTextureBlendMode(out,SDL_BLENDMODE_BLEND);
	}
	return out;
}

void Free(Texture garbage){
	SDL_DestroyTexture(garbage);
}

#include "../inc/test.h"



