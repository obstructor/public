#include "../inc/all.h"
#include "../inc/constants.h"
#include "../inc/Render.h"
#include "../inc/InitializeClean.h"
#include "../inc/audio.h"
#include "../inc/event.h"
#include "../inc/game_engine.h"
#include "../inc/UI_Engine.h"
#include "../inc/JobHandler.h"

bool EventHandler::Initialize()
{
	SDLkeys = NULL;
	SDLkeys = SDL_GetKeyboardState(&number_of_keys);

	if (SDLkeys != NULL && number_of_keys > 0)
	{
		key_state = new KeyState[number_of_keys];
		for (int i = 0; i < number_of_keys; ++i)
		{
			key_state[i] = KEY_RELEASED;
		}
	}
	else return false;

	return true;
}

void EventHandler::Clean()
{
	delete[] key_state;
}

bool GameHandler::Initialize_Game_Engine()
{
	// Load Prototypes, should probably scan a folder and load all files in the folder.
	bool success;
	success = Parse_Prototypes();
	return success;
}

bool Debugger::Initialize_Debug()
{
	logfile.open("log.log");
	if (logfile.is_open())
	{
		std::clog << "logfile opened" << '\n';
	}
	else
	{
		std::clog << "logfile failed to open" << '\n';
		std::cin.get();
	}

	std::ifstream debug_settings("../resources/settings/debug");
	if (debug_settings.is_open())
	{
		int i = 0;
		for (i = 0; i < D_END && !debug_settings.eof(); ++i)
		{
			debug_settings >> enabled_messages[i];
		}
		if (debug_settings.eof())
		{
			Debug("Too few items in debug settings", D_DEBUG);
			for (i; i < D_END; ++i)
			{
				enabled_messages[i] = 1;
			}
		}
	}
	else return false;


	debug_settings.close();
	return true;
}

void Debugger::Clean_Debug()
{
	logfile.close();
}

bool RenderHandler::Initialize()
{
	screen_width = 640;
	screen_height = 480;
	screen_scale = 1;

	if (SDL_Init(SDL_INIT_VIDEO) < 0) {
		std::cout << "SDL Init returned this error: " << SDL_GetError();
		return 0;
	}
	else {
		//Nearest Neighbor scaling, since this is a pixelly game.
		SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "0");
	}
	IMG_Init(IMG_INIT_PNG);

	//set up the context
	game_window = SDL_CreateWindow(
		TITLE,
		SDL_WINDOWPOS_UNDEFINED,
		SDL_WINDOWPOS_UNDEFINED,
		screen_width,
		screen_height,
		SDL_WINDOW_RESIZABLE
		);

	game_renderer = SDL_CreateRenderer(
		game_window,
		-1,
		SDL_RENDERER_PRESENTVSYNC
		);

	blocks.Load("../resources/images/blocks.png");

	SDL_SetRenderDrawColor(game_renderer, 0x88, 0x88, 0x88, 0x88);
	return 0;
}

bool Initialize()
{
	Debug_Manager.Initialize_Debug();

	std::cout << TITLE << std::endl;
	std::cout << VERSION << std::endl;
	Debug("Initializing SDL");

	Game_Engine.Initialize_Game_Engine();
	Job_Manager.Initialize();
	UI_Manager.Initialize();
	Render_Manager.Initialize();
	Audio_Manager.Initialize();
	Event_Manager.Initialize();

	return true;
}

void Clean()
{
	//Free Surfaces
	//Free Other SDL Modules
	Event_Manager.Clean();
	Debug_Manager.Clean_Debug();
	IMG_Quit();
	SDL_Quit();
}