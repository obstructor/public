#include "../inc/audio.h"


bool AudioManager::Load_Sound(std::string path){

	Sound loaded;
	
	path = "../resources/sounds/" + path;
	
	loaded = Mix_LoadWAV( path.c_str() );
	if (loaded == NULL)
	{
		Debug(Mix_GetError(), D_AUDIO);
		return false;
	}
	else
	{
		Debug("Loaded" + path, D_AUDIO);
		loaded_sounds.push_back(loaded);
		return true;
	}
	return false;
}


bool AudioManager::Load_Music(std::string path){
	Music loaded = NULL;
	
	path = "../resources/music/" + path;
	
	loaded = Mix_LoadMUS( path.c_str() );
	if (loaded == NULL)
	{
		Debug(Mix_GetError(), D_AUDIO);
		return false;
	}
	else 
	{
		Debug("Loaded" + path, D_AUDIO);
		loaded_music.push_back(loaded);
		return true;
	}
	return false;
}

bool AudioManager::Initialize_All_Music()
{
	Load_Music("flowey.ogg"); // This was copyrighted music we were using to test.
	return Load_Music("flowey.ogg");

}

bool AudioManager::Initialize_All_Sound()
{
	Load_Sound("test.wav");
	return Load_Sound("test.wav");
}