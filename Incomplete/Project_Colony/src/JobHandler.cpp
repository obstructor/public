#include "../inc/JobHandler.h"
#include "../inc/game_engine.h"

bool JobHandler::Initialize()
{
	null_task.type = TASK_NULL;
	Coordinate n_cord; n_cord.y = n_cord.z = n_cord.x = 0;
	null_task.destination = n_cord;
	null_task.value = 0;
	null_task.task_ID = 0;
	null_task.claimant_ID = 0;

	task_queues[TASK_NULL].push(null_task);
	task_exists[TASK_NULL].push_back(true);

	return true;
}

Task JobHandler::Request_Task(unsigned int ID, TaskType type)
{
	Task requested_task;
	if (task_queues[type].empty())
	{
		requested_task = null_task;
	}
	else
	{
		bool task_found = false;
		while (!task_queues[type].empty() && task_found == false)
		{
			requested_task = task_queues[type].front();
			if (task_exists[type].at(requested_task.task_ID))
			{
				task_found = true;
				// Recycle task until it gets completed.
				task_queues[type].pop();
				task_queues[type].push(requested_task); 
			}
			else
			{
				task_found = false;
				//Selected task is completed pop from queue, do not recycle.
				task_queues[type].pop();
			}
		}
		
	}
	return requested_task;
}

void JobHandler::Task_Finished(Task finished_task)
{
	if (task_exists[finished_task.type].size() > finished_task.task_ID)
	{
		if (task_exists[finished_task.type][finished_task.task_ID] == true)
		{
			task_exists[finished_task.type][finished_task.task_ID] = false;
			task_ID_available[finished_task.type].push(finished_task.task_ID);
		}
	}
		
}

void JobHandler::Create_Task(TaskType type, Coordinate location)
{
	Task new_task = null_task;
	if (task_ID_available[type].empty())
	{
		new_task.task_ID = CreateID(type);
	}
	else
	{
		new_task.task_ID = task_ID_available[type].front();
		task_ID_available[type].pop();
	}

	if (task_exists[type].size() < new_task.task_ID)
	{
		task_exists[type].resize(new_task.task_ID + 1, false);
	}
	task_exists[type][new_task.task_ID] = true;

	task_queues[type].push(new_task);
}

unsigned int JobHandler::CreateID(TaskType type)
{
	unsigned int ID = task_exists[type].size();
	task_exists[type].push_back(false); //Just because we create an ID doesn't mean its task exists.
	return ID;
}