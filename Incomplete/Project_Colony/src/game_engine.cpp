#include "../inc/game_engine.h"
#include "../inc/UI_Engine.h"
#include "../inc/constants.h"
#include <ctime>

void GameHandler::Update()
{
	GameInstruction current_instruction;

	for (int i = UI_Manager.game_instruction_queue.size(); i > 0; --i)
	{
		current_instruction = UI_Manager.game_instruction_queue.front();
		UI_Manager.game_instruction_queue.pop();
		switch(current_instruction.type)
		{
		case GAME_UPDATE_PHYSICS:
			Physics_Update();
			break;
		case GAME_PLAYER_DIRECT:
			break;
		case GAME_DESIGNATION:
			break;
		}
	}
}

void GameHandler::Physics_Update()
{
	Actor current_actor;
	for (int i = actor_queue.size(); i > 0; --i)
	{
		current_actor = actor_queue.front();
		if (current_actor.type == PROTO_COLONIST)
		{
			Colonist_Update(current_actor);
		}
		else if (current_actor.type == PROTO_PLAYER)
		{
			Player_Update(current_actor);
		}
		else if (current_actor.type >= 0 && current_actor.type < Game_Engine.prototype_list.size())
		{
			Block_Update(current_actor);
		}
		else
		{
			Debug("Invalid actor type:" + current_actor.type, D_PHYSICS);
		}
		actor_queue.pop();
	}
	Physics_Push();
}

void GameHandler::Player_Update(Actor &current_actor)
{

}

void GameHandler::Colonist_Update(Actor &current_actor)
{

}

void GameHandler::Physics_Push()
{
	PhysicsEvent current;
	bool changed = false;
	while (event_queue.size() > 0)
	{
		changed = false;
		current = event_queue.front();
		switch (current.type)
		{
		case PHYSICS_EVENT_PRESSURE:
			/*  Pressure physics are being moved to multiblock structure.
			if (Bound_Check(current.source) && Bound_Check(current.destination))
			{
				if (map[(current.source)].Pressure() >= current.value && current.value > 0)
				{
					map[(current.source)].Decrease_Value(current.value);
					map[(current.destination)].Increase_Value(current.value);
					map[(current.destination)].type = map[(current.source)].type;
					changed = true;
				}
				if (map[(current.source)].Pressure() == 0)
				{
					map[(current.source)].type = PROTO_AIR;
				}
			}*/
			break;
		}

		if (changed)
		{
			Coordinate loader_up;
			Coordinate loader_side[4];
			Coordinate loader_down;
			loader_up = loader_down = loader_side[0] = loader_side[1] = loader_side[2] = loader_side[3] = current.source;


			loader_up.y -= 1;
			loader_down.y = current.source.y + 1;

			loader_side[0].z = current.source.z + 1;

			loader_side[1].z = current.source.z - 1;

			loader_side[2].x = current.source.x + 1;

			loader_side[3].x = current.source.x - 1;

			if (Bound_Check(loader_up))
				map[loader_up].Actor(true);
			if (Bound_Check(loader_down))
				map[loader_down].Actor(true);
			
			Actor neighbor;
			neighbor.type = PROTO_WATER;
			neighbor.position = loader_up;
			actor_queue.push(neighbor);
			for (int i = 0; i < 4; ++i)
			{
				if (Bound_Check(loader_side[i]))
				{
					map[loader_side[i]].Actor(true);
					neighbor.position = loader_side[i];
					actor_queue.push(neighbor);
				}
			}
			neighbor.position = loader_down;
			actor_queue.push(neighbor);
		}
		event_queue.pop();
	}
}


bool GameHandler::Bound_Check(const Coordinate & coord)
{
	return map.Bound_Check(coord);
}

bool GameHandler::Bound_Check(const int & x, const int & y, const int &z)
{
	return map.Bound_Check(x,y,z);
}

int GameHandler::Get_Sprite_Number(const Coordinate & coord)
{
	if (Bound_Check(coord))
	{
		return map[coord].Sprite();
	}
	else
	{
		return 0;
	}
}

int GameHandler::Get_Sprite_Number(const int & x, const int & y, const int & z)
{
	
	if (Bound_Check(x, y, z))
	{
		Coordinate temp;
		temp.x = x;
		temp.y = y;
		temp.z = z;
		return map[temp].Sprite();
	}
	else
	{
		return 0;
	}
}

void GameHandler::Generate_Map()
{
	MapGenerator map_generator;
	map = map_generator.Generate_Map(clock());
}

void GameHandler::Generate_Water_Test()
{
	/* Needs to be redone to work with chunk structure.
	MapEntity temp;
	temp.type = PROTO_DIRT;
	temp.Flag(PROTO_FLAG_SOLID);
	temp.Set_Value(1000);
	temp.Actor(false);

	std::vector <std::vector <MapEntity> > side;
	std::vector <MapEntity> row;

	map.resize(0);

	for (int x = 0; x < 10; ++x)
	{
		side.resize(0);
		for (int y = 0; y < 10; ++y)
		{
			row.resize(0);
			for (int z = 0; z < 10; ++z)
			{
				row.push_back(temp);
			}
			side.push_back(row);
		}
		map.push_back(side);
	}

	MapEntity water;
	water.type = PROTO_WATER;
	water.Flag(true, PROTO_FLAG_FLUID);
	water.Set_Value(10000);
	water.Actor(true);

	MapEntity magma;
	magma.type = PROTO_MAGMA;
	magma.Flag(true, PROTO_FLAG_FLUID);
	magma.Set_Value(10000);
	magma.Actor(true);

	MapEntity air;
	air.type = PROTO_AIR;
	air.Set_Value(0);
	air.Actor(false);
	air.Flag(true, PROTO_FLAG_WALKABLE);
	air.Flag(true, PROTO_FLAG_TRANSPARENT);

	for (int x = 1; x < 9; ++x)
	{
		for (int y = 1; y < 9; ++y)
		{
			for (int z = 1; z < 9; ++z)
			{
				map[x][y][z] = air;
			}
		}
	}

	map[1][1][1] = water;
	map[8][1][8] = magma;
	Actor water_actor;
	Coordinate water_test;
	water_test.x = water_test.z = water_test.y = 1;
	water_actor.position = water_test;
	water_actor.type = PROTO_WATER;

	actor_queue.push(water_actor);

	water_test.x = water_test.z = 8;
	water_actor.position = water_test;
	water_actor.type = PROTO_WATER;

	actor_queue.push(water_actor);*/
}

void GameHandler::Generate_Water_Test2()
{
	/*  Needs to be redone to work with chunk structure.
	MapEntity temp;
	temp.type = PROTO_DIRT;
	temp.Set_Value(1000);
	temp.Actor(false);

	std::vector <std::vector <MapEntity> > side;
	std::vector <MapEntity> row;

	map.resize(0);

	for (int x = 0; x < 10; ++x)
	{
		side.resize(0);
		for (int y = 0; y < 3; ++y)
		{
			row.resize(0);
			for (int z = 0; z < 10; ++z)
			{
				row.push_back(temp);
			}
			side.push_back(row);
		}
		map.push_back(side);
	}

	MapEntity water;
	water.type = PROTO_WATER;
	water.Flag(true, PROTO_FLAG_FLUID);
	water.Set_Value(10000);
	water.Actor(true);


	MapEntity air;
	air.type = PROTO_AIR;
	air.Set_Value(0);
	air.Actor(false);
	air.Flag(true, PROTO_FLAG_WALKABLE);
	air.Flag(true, PROTO_FLAG_TRANSPARENT);

	for (int x = 1; x < 9; ++x)
	{
		for (int y = 1; y < 2; ++y)
		{
			for (int z = 1; z < 9; ++z)
			{
				map[x][y][z] = air;
			}
		}
	}

	map[1][1][1] = water;

	Actor water_actor;
	Coordinate water_test;
	water_test.x = water_test.z = water_test.y = 1;
	water_actor.position = water_test;
	water_actor.type = PROTO_WATER;

	actor_queue.push(water_actor);
	*/
}
std::string Remove_White_Space(std::string parser)
{
	std::string parser_swap;
	for (int i = 0; i < parser.length(); ++i)
	{
		if (parser[i] != ' ' && parser[i] != '/t')
		{
			parser_swap + parser[i];
		}
	}
	return parser_swap;
}
bool GameHandler::Parse_Prototypes()
{
	bool success = true;
	std::fstream prototype_file("../resources/settings/prototypes");
	if (prototype_file.is_open())
	{
		std::string parser = "";
		std::getline(prototype_file, parser);
		bool found_type = false;
		while (found_type = false)
		{
			parser = Remove_White_Space(parser);
			if (parser == "solid")
			{

			}
			else if (parser == "gas")
			{

			}
			else if (parser == "liquid")
			{

			}
		}
		
		
		// read in block,gas,liquid throwaway whitespace and empty lines
		// find { and delete
		// read in line end if } found while deleting whitespace
		// parse up to = and after = pass read information to prototype.
	}
	else
		success = false;
	return success;
}


MapEntity Chunk::at(const Coordinate & coord)
{
	return chunk[(coord.z % CHUNK_SIZE) + CHUNK_SIZE * ((coord.y % CHUNK_SIZE) + (coord.x % CHUNK_SIZE) * CHUNK_SIZE)];
}

MapEntity Chunk::at(const int&x, const int&y, const int&z)
{
	return chunk[(z % CHUNK_SIZE) + CHUNK_SIZE * ((y % CHUNK_SIZE) + (x % CHUNK_SIZE) * CHUNK_SIZE)];
}