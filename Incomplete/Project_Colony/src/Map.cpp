#include "../inc/Entity.h"

unsigned int Map::size()
{
	return internal_map_storage.size();
}

void Map::resize(unsigned int size)
{
	internal_map_storage.resize(size);
}

void Map::resize(int x, int y, int z)
{
	resize(x * y * z);
}

MapEntity Map::Null()
{
	return Null_Entity;
}

bool Map::Bound_Check(const Coordinate & coord)
{
	if (Coord_to_Chunk(coord) >= 0 && Coord_to_Chunk(coord) < internal_map_storage.size())
	{
		if (Coord_to_Block(coord) < CHUNK_SIZE && Coord_to_Block(coord) >= 0)
		{
			if (coord.x >= 0 && coord.y >= 0 && coord.z >= 0)
				return true;
		}
	}
	return false;
}

bool Map::Bound_Check(const int & x, const int & y, const int &z)
{
	if (Coord_to_Chunk(x, y, z) >= 0 && Coord_to_Chunk(x, y, z) < internal_map_storage.size())
	{
		if (Coord_to_Block(x, y, z) < CHUNK_SIZE && Coord_to_Block(x, y, z) >= 0)
		{
			if (x >= 0 && y >= 0 && z >= 0)
				return true;
		}
	}
	return false;
}

int Map::Coord_to_Chunk(const Coordinate & coord)
{
	return (coord.x / CHUNK_SIZE) * map_chunk_ywidth * map_chunk_zwidth + (coord.y / CHUNK_SIZE) * map_chunk_zwidth + (coord.z / CHUNK_SIZE);
}

int Map::Coord_to_Chunk(const int & x, const int & y, const int & z)
{
	return (x / CHUNK_SIZE) * map_chunk_ywidth * map_chunk_zwidth + (y / CHUNK_SIZE) * map_chunk_zwidth + (z / CHUNK_SIZE);
}

int Map::Coord_to_Block(const Coordinate & coord)
{
	return (coord.x % CHUNK_SIZE) * CHUNK_SIZE * CHUNK_SIZE + (coord.y % CHUNK_SIZE) * CHUNK_SIZE + (coord.z % CHUNK_SIZE);
}

int Map::Coord_to_Block(const int &x, const int &y, const int &z)
{
	return (x % CHUNK_SIZE) * CHUNK_SIZE * CHUNK_SIZE + (y % CHUNK_SIZE) * CHUNK_SIZE + (z % CHUNK_SIZE);
}

int Map::X()
{
	return map_chunk_xwidth * CHUNK_SIZE;
}

int Map::Y()
{
	return map_chunk_ywidth * CHUNK_SIZE;
}

int Map::Z()
{
	return map_chunk_zwidth * CHUNK_SIZE;
}

MapEntity & Map::operator[](const Coordinate& coord)
{
	if (Bound_Check(coord))
	{
		return internal_map_storage[Coord_to_Chunk(coord)].chunk[Coord_to_Block(coord)];
	}
	else
	{
		return Null_Entity;
	}
}