#include "../inc/Render.h"
#include "../inc/game_engine.h"

void RenderHandler::Render_Square(const int &type, const SDL_Rect &renderer)
{
	SDL_RenderCopy(game_renderer, blocks.sheet, &blocks.Get_Sprite_Rect(type), &renderer);
}

void RenderHandler::Render_Layer()
{
	SDL_Rect renderer, src;

	renderer.y = renderer.x = 0;
	src.y = src.x = 0;

	src.w = renderer.w = 20;
	src.h = renderer.h = 20;
	/* No longer works after migrating to chunks, likely uneeded as viewport rendering exists.
	for (int x = 0; x < Game_Engine.map.size(); ++x)
	{
		for (int z = 0; z < Game_Engine.map[0][0].size(); ++z)
		{
			renderer.y = z * 20;
			renderer.x = x * 20;

			// Should probably do something with transparent blocks like air, but who knows.

			Render_Square(Game_Engine.map[x][UI_Manager.viewlevel][z].Sprite(), renderer);

		}
	}
	*/
}

void RenderHandler::Render_Viewport(ViewPort view)
{
	SDL_Rect renderer, src;
	//Based on type select spritesheet.
	//Currently only one type of viewport exists.
	SpriteSheet * current_sprite_sheet = &blocks;
	Coordinate crb = view.location; //current_render_block

	renderer.y = renderer.x = 0;
	src.y = src.x = 0;

	src.w = renderer.w = current_sprite_sheet->square_width;
	src.h = renderer.h = current_sprite_sheet->square_height;

	for (int x = 0; x < view.x_width / 20; ++x)
	{
		for (int z = 0; z < view.z_width / 20; ++z)
		{
			renderer.y = z * 20; // the renderer is x,y game is x,y,z we need to render z on the renderer y axis.
			renderer.x = x * 20;

			crb.x = view.location.x + x;
			crb.z = view.location.z + z;
			// Should probably do something with transparent blocks like air, but who knows.
			
			Render_Square(Game_Engine.Get_Sprite_Number(crb), renderer);
		}
	}
}

void RenderHandler::Clear_Screen()
{
	SDL_RenderClear(game_renderer);
}

void RenderHandler::Update_Screen()
{
	SDL_RenderPresent(game_renderer);
}

int RenderHandler::Frame_Limiter() {
	static int oldtime = 0;
	static int newtime = 0;
	static double avg = 1 / 60.0;

	newtime = SDL_GetTicks();

	//update the average frame time, weighting the current frame at 1/10
	avg = (9 * avg + (newtime - oldtime)) / 10.0;

	//update the time
	oldtime = newtime;

	//calculate the fps from the average frame time (ms)
	int fps = (int)(1000.0 / avg + 0.5);

	//output FPS as debugging information every 5 seconds
	static int a = 0;
	a++;
	if (a % 300 == 0) {
		Debug("FPS: " + std::to_string(fps), D_VIDEO);
	}

	return fps;
}

void RenderHandler::Render()
{
	Clear_Screen();
	//Render_Test_Font(0, 0);
	Render_Viewport(UI_Manager.viewport_list.at(0));
	Frame_Limiter();
	//placeholder
	Update_Screen();
}


