#include "../inc/game_engine.h"
int cycle = 0;
/*  Fluids are being converted to multiblock entities.
void GameHandler::Fluid_Update(Actor & current_actor, MapEntity current_entity)
{
	MapEntity upper;
	MapEntity side[4];
	MapEntity lower;
	Coordinate loader_up;
	Coordinate loader_side[4];
	Coordinate loader_down;
	loader_up = loader_down = loader_side[0] = loader_side[1] = loader_side[2] = loader_side[3] = current_actor.position;

	loader_up.y -= 1;
	upper = (Fetch_Entity(loader_up));
	loader_down.y = current_actor.position.y + 1;
	lower = Fetch_Entity(loader_down);

	loader_side[0] = current_actor.position;
	loader_side[0].z = current_actor.position.z + 1;
	side[0] = Fetch_Entity(loader_side[0]);

	loader_side[1] = current_actor.position;
	loader_side[1].z = current_actor.position.z - 1;
	side[1] = Fetch_Entity(loader_side[1]);

	loader_side[2] = current_actor.position;
	loader_side[2].x = current_actor.position.x + 1;
	side[2] = Fetch_Entity(loader_side[2]);

	loader_side[3] = current_actor.position;
	loader_side[3].x = current_actor.position.x - 1;
	side[3] = Fetch_Entity(loader_side[3]);

	int flow = 0;
	int self_weight = current_entity.Pressure();

	PhysicsEvent event_created;

	if (lower.Flag(PROTO_FLAG_FLUID) || lower.Flag(PROTO_FLAG_WALKABLE))
	{
		if (lower.type == current_entity.type || lower.type == PROTO_AIR)
		{
			if (self_weight + 10 > lower.Pressure())
			{
				flow = (self_weight + 10 - lower.Pressure()) / 2;
			}
			if (flow > self_weight)
			{
				flow = self_weight;
			}
		}
		//Else swap based on density, oil to water, etc.
		// Or combine as lava and water.
	}

	if (flow > 0)
	{
		event_created.type = PHYSICS_EVENT_PRESSURE;
		event_created.value = flow;
		event_created.source = current_actor.position;
		event_created.destination = loader_down;
		event_queue.push(event_created);
	}

	int side_count = 0;
	int side_flow_temp;
	int side_flow_difficulty[4] = {0,1,2,3};
	flow = 0;

	for (unsigned int i = 0; i < 4; ++i) //For some reason this breaks if i is signed
	{
		if (!(side[i].Flag(PROTO_FLAG_FLUID) || side[i].Flag(PROTO_FLAG_WALKABLE)))
			side_flow_difficulty[i] = -1;
		if (!(side[i].type == current_entity.type || side[i].type == PROTO_AIR))
			side_flow_difficulty[i] = -1;
			
	}

	bool sorted = false;
	int index = 0;
	while (!sorted)
	{
		sorted = true;
		for (int i = 0; i < 3; ++i)
		{
			if (side[i].Pressure() < side[i + 1].Pressure())
			{
				std::swap(side[i], side[i + 1]);
				std::swap(side_flow_difficulty[i], side_flow_difficulty[i + 1]);
				sorted = false;
			}
		}
	}

	int average = self_weight;
	int running_average = self_weight;
	int average_count = 1;

	for (int i = 0; i < 4; ++i)
	{
		if (side[i].Pressure() < running_average && side_flow_difficulty[i] >= 0)
		{
			average += side[i].Pressure();
			average_count += 1;
			running_average = average / average_count;
		}
	}

	for (int i = 0; i < 4; ++i)
	{	
		if (side_flow_difficulty[i] >= 0 && side[i].Pressure() < running_average)
		{
			flow = (running_average - side[i].Pressure());
			if (flow > 0)
			{
				self_weight -= flow;
				event_created.type = PHYSICS_EVENT_PRESSURE;
				event_created.value = flow;
				event_created.source = current_actor.position;
				event_created.destination = loader_side[side_flow_difficulty[i]];
				event_queue.push(event_created);
			}
		}
	}

	flow = 0;
	if (upper.Flag(PROTO_FLAG_FLUID) || upper.Flag(PROTO_FLAG_WALKABLE))
	{
		if (upper.Pressure() + 10 < self_weight)
		{
			flow = (self_weight - (upper.Pressure() + 10)) / 2;
		}
	}

	if (flow > 0)
	{
		self_weight -= flow;
		event_created.type = PHYSICS_EVENT_PRESSURE;
		event_created.value = flow;
		event_created.source = current_actor.position;
		event_created.destination = loader_up;
		event_queue.push(event_created);
	}
}
*/
void GameHandler::Solid_Update(Actor & current_actor, MapEntity current_entity)
{
	MapEntity upper;
	MapEntity side[4];
	MapEntity lower;
	Coordinate loader_up;
	Coordinate loader_side[4];
	Coordinate loader_down;
	loader_up = loader_down = loader_side[0] = loader_side[1] = loader_side[2] = loader_side[3] = current_actor.position;

	loader_up.y -= 1;
	upper = (Fetch_Entity(loader_up));
	loader_down.y = current_actor.position.y + 1;
	lower = Fetch_Entity(loader_down);

	loader_side[0].z = current_actor.position.z + 1;
	side[0] = Fetch_Entity(loader_side[0]);

	loader_side[1].z = current_actor.position.z - 1;
	side[1] = Fetch_Entity(loader_side[1]);

	loader_side[2].x = current_actor.position.x + 1;
	side[2] = Fetch_Entity(loader_side[2]);

	loader_side[3].x = current_actor.position.x - 1;
	side[3] = Fetch_Entity(loader_side[3]);

	PhysicsEvent event_created;
	/*
	if (lower.flags.test(ENTITY_SOLID))
	{
		if (lower.hp <= current_entity->hp)
		{
			if (lower.hp >= 0)
			{
				event_created.type = PHYSICS_EVENT_BLOCK_COMPRESS;
				event_created.source = current_actor.position;
				event_created.destination = loader_down;
				event_created.value = current_entity->hp;
				event_queue.push(event_created);
			}
			else
			{
				event_created.type = PHYSICS_EVENT_BLOCK_COMPRESS;
				event_created.source = current_actor.position;
				event_created.destination = loader_down;
				event_created.value = current_entity->hp;
				event_queue.push(event_created);
			}
		}
		else
		{
			
		}
	}
	*/

	if (upper.Flag(ENTITY_OUTSIDE))
	{
		if (current_entity.Flag(PROTO_FLAG_WALKABLE))
		{
			event_created.type = PHYSICS_EVENT_OUTSIDE;
			event_created.source = loader_up;
			event_created.destination = current_actor.position;
		}
		else
		{
			event_created.type = PHYSICS_EVENT_INSIDE;
			event_created.source = current_actor.position;
			event_created.destination = current_actor.position;
		}
	}
}

void GameHandler::Block_Update(Actor &current_actor)
{
	MapEntity current_entity = Fetch_Entity(current_actor.position);
	if (!current_entity.Flag(ENTITY_INACTIVE))
	{
		if (current_entity.Flag(PROTO_FLAG_FLUID) && current_entity.Actor())
		{
			current_entity.Actor(false);
			Fluid_Update(current_actor, current_entity);
		}
		else
		{
			current_entity.Actor(false);
			//Physics needs to be figured out before we can implement them.
			Solid_Update(current_actor, current_entity);
		}
	}
	else
	{
		current_entity.Actor(false);
	}
}

MapEntity GameHandler::Fetch_Entity(int& x, int& y, int& z)
{
	int chunkx = x / CHUNK_SIZE;
	int chunky = y / CHUNK_SIZE;
	int chunkz = z / CHUNK_SIZE;
	if (Bound_Check(x, y, z))
	{
		return map[Coordinate(x,y,z)];
	}
	else if (map.size() > 0)
	{
		//Debug("Out of Bounds:", D_PHYSICS);
		return MapEntity();
	}
	else
	{
		Debug("Map size < 1", D_PHYSICS);
		return MapEntity();
	}
}

MapEntity GameHandler::Fetch_Entity(Coordinate& coord)
{
	if (Bound_Check(coord.x, coord.y, coord.z))
	{
		return map[(coord)];
	}
	else if (map.size() > 0)
	{
		return map.Null();
	}
	else
	{
		Debug("Map size < 1", D_PHYSICS);
		return MapEntity();
	}
}