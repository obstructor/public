#include "../inc/event.h"

/*
**
**
*/


MouseButtonList EventHandler::Get_Mouse_Key()
{
	switch (sdl_event.button.button)
	{
		case SDL_BUTTON_LEFT:
			return MOUSE_LEFT;
			break;
		case SDL_BUTTON_RIGHT:
			return MOUSE_RIGHT;
			break;
		case SDL_BUTTON_MIDDLE:
			return MOUSE_MIDDLE;
			break;
		default:
			Debug("Invalid Mouse Button", D_INPUT);
			return MOUSE_END;
	}
}

void EventHandler::Gather_Input()
{
	InputEvent temp;
	

	while (SDL_PollEvent(&sdl_event))
	{
		temp.state = KEY_NULL;
		temp.type = EVENT_NONE;
		temp.key = 0;
		temp.x = 0; temp.y = 0;

		switch (sdl_event.type)
		{
		case SDL_QUIT:
			temp.state = KEY_PRESSED;
			temp.type = EVENT_EXIT;
			break;
		case SDL_KEYDOWN:
			if (key_state[sdl_event.key.keysym.scancode] == KEY_RELEASED)
			{
				temp.state = KEY_PRESSED;
				temp.type = EVENT_KEY;
				temp.key = sdl_event.key.keysym.scancode;
				key_state[sdl_event.key.keysym.scancode] = KEY_PRESSED;
			}
			break;
		case SDL_KEYUP:
			if (key_state[sdl_event.key.keysym.scancode] == KEY_PRESSED)
			{
				temp.state = KEY_RELEASED;
				temp.type = EVENT_KEY;
				temp.key = sdl_event.key.keysym.scancode;
				key_state[sdl_event.key.keysym.scancode] = KEY_RELEASED;
			}
			break;
		case SDL_MOUSEBUTTONDOWN:
			temp.key = Get_Mouse_Key();
			if (mouse_state[temp.key] == 1)
			{
				temp.type = EVENT_MOUSE;
				temp.state = KEY_PRESSED;
				SDL_GetMouseState(&temp.x, &temp.y);
			}
			else temp.key = KEY_NULL;
			break;
		case SDL_MOUSEBUTTONUP:
			temp.key = Get_Mouse_Key();
			if (mouse_state[temp.key] == 0)
			{
				temp.type = EVENT_MOUSE;
				temp.state = KEY_RELEASED;
				SDL_GetMouseState(&temp.x, &temp.y);
			}
			else temp.key = KEY_NULL;
			break; 
		case SDL_MOUSEMOTION:
			temp.type = EVENT_MOUSE_MOTION;
			temp.state = KEY_NULL;
			temp.key = 0;
			SDL_GetMouseState(&temp.x, &temp.y);
			break;
		}

		if (temp.type != EVENT_NONE)
		{
			std::string message = "Keypress: " + std::to_string(temp.key) + '\n'
				+ std::to_string(temp.state) + '\n' + std::to_string(temp.type);
			Debug(message, D_INPUT);
			input_queue.push(temp);
		}
	}
}
















/*

void Update_Mouse(InputEvent * event){

	int buttons = SDL_GetMouseState(0,0);

	//Left click check
	if(buttons & SDL_BUTTON(SDL_BUTTON_LEFT)){
		event->btnstate |= BTN_LCLICK;
	}
	else{
		event->btnstate &= ~BTN_LCLICK;
	}

	//Middle click check
	if(buttons & SDL_BUTTON(SDL_BUTTON_MIDDLE)){
		event->btnstate |= BTN_MCLICK;
	}
	else{
		event->btnstate &= ~BTN_MCLICK;
	}
	//Scrolling check
	if(buttons & SDL_BUTTON(SDL_BUTTON_X2)){
		event->btnstate |= BTN_SCRUP;
	}
	else{
		event->btnstate &= ~BTN_SCRUP;
	}
	//Scrolling check
	if(buttons & SDL_BUTTON(SDL_BUTTON_X1)){
		event->btnstate |= BTN_SCRDN;
	}
	else{
		event->btnstate &= ~BTN_SCRDN;
	}

	//Right click check
	if(buttons & SDL_BUTTON(SDL_BUTTON_RIGHT)){
		event->btnstate |= BTN_RCLICK;
	}
	else{
		event->btnstate &= ~BTN_RCLICK;
	}
}

InputEvent Poll_Input(Monitor& event_monitor){
	InputEvent out;

	out.type = E_NONE;
	out.btnstate = event_monitor.last_event.btnstate;
	out.x = event_monitor.last_event.x;
	out.y = event_monitor.last_event.x;

	//Poll all waiting events
	while(SDL_PollEvent(&(event_monitor.e))){

		switch (event_monitor.e.type) {
		case SDL_QUIT:
			out.type |= E_EXIT;
			break;

		case SDL_MOUSEMOTION:
			out.type |= E_MOUSE;
			out.x = event_monitor.e.motion.x;
			out.y = event_monitor.e.motion.y;
			break;

		case SDL_MOUSEWHEEL:
			if (event_monitor.e.wheel.y > 0) {

				if (SDL_MOUSEWHEEL_FLIPPED) {
					out.type |= E_SCRUP;
				}
				else {
					out.type |= E_SCRDN;
				}
			}
			if (event_monitor.e.wheel.y < 0) {
			
				if (SDL_MOUSEWHEEL_FLIPPED) {
					out.type |= E_SCRDN;
				}
				else {
					out.type |= E_SCRUP;
				}
			
			}
			break;
			
			case SDL_MOUSEBUTTONDOWN:
			case SDL_MOUSEBUTTONUP:
			if (event_monitor.e.button.button == SDL_BUTTON_RIGHT){
				out.type |= E_RCLICK;
			}
			if (event_monitor.e.button.button == SDL_BUTTON_LEFT){
				out.type |= E_LCLICK;
			}
			if (event_monitor.e.button.button == SDL_BUTTON_MIDDLE){
				out.type |= E_MCLICK;
			}
			
			break;


			case SDL_KEYDOWN:
			if(event_monitor.e.key.repeat == 0){
				switch(event_monitor.e.key.keysym.sym) {

					case SDLK_w:
					out.type |= E_UP;
					out.btnstate |= BTN_UP;
					break;

					case SDLK_a:
					out.type |= E_LEFT;
					out.btnstate |= BTN_LEFT;
					break;

					case SDLK_s:
					out.type |= E_DOWN;
					out.btnstate |= BTN_DOWN;
					break;

					case SDLK_d:
					out.type |= E_RIGHT;
					out.btnstate |= BTN_RIGHT;
					break;

					case SDLK_SPACE:
					out.type |= E_JUMP;
					out.btnstate |= BTN_JUMP;
					break;

					case SDLK_LSHIFT:
					out.type |= E_ROLL;
					out.btnstate |= BTN_ROLL;
					break;

					case SDLK_RETURN:
					out.type |= E_PAUSE;
					out.btnstate |= BTN_PAUSE;
					break;
				}
			}
			break;

			case SDL_KEYUP:
			switch(event_monitor.e.key.keysym.sym) {
				case SDLK_w:
				out.type |= E_UP;
				out.btnstate &= ~BTN_UP;
				break;

				case SDLK_a:
				out.type |= E_LEFT;
				out.btnstate &= ~BTN_LEFT;
				break;

				case SDLK_s:
				out.type |= E_DOWN;
				out.btnstate &= ~BTN_DOWN;
				break;

				case SDLK_d:
				out.type |= E_RIGHT;
				out.btnstate &= ~BTN_RIGHT;
				break;

				case SDLK_SPACE:
				out.type |= E_JUMP;
				out.btnstate &= ~BTN_JUMP;
				break;

				case SDLK_LSHIFT:
				out.type |= E_ROLL;
				out.btnstate &= ~BTN_ROLL;
				break;

				case SDLK_RETURN:
				out.type |= E_PAUSE;
				out.btnstate &= ~BTN_PAUSE;
				break;
			}
			break;

			case SDL_WINDOWEVENT:
				if(event_monitor.e.window.event == SDL_WINDOWEVENT_RESIZED){
					Debug("Resizing screen: " + std::to_string(event_monitor.e.window.data1) + "x" + std::to_string(event_monitor.e.window.data2), D_VIDEO);
					Resize_Screen(event_monitor.e.window.data1, event_monitor.e.window.data2,SCREEN_SCALE);
				}
			break;

			default:
			break;
		}
	}

	Update_Mouse(&out);

	if(out.type != E_NONE){
		Debug("Caught event of type: "+std::to_string(out.type),D_UI);
		Debug("Mouse X,Y: "+std::to_string(out.x)+" "+std::to_string(out.y), D_UI);
		Debug("Buttons: "+std::to_string(out.btnstate), D_UI);
	}

	event_monitor.last_event = out; //If they push multiple buttons only the last will show up.

	return out;
}

InputEvent Gather_Input()
{
	static Monitor input;
	input.last_event.btnstate = 0;

	InputEvent cur = Poll_Input(input);

	return cur;
}
*/

/* This code was found within the gather input function on migration
** to the new design.  I will find where it needs to go if it remains
** Although if you wish to move it somewhere or delete it go ahead.
** -Jeffrey
int a = 0;

int x = 0;
int y = 0;
double scale = 1;
std::string text = "";

if (cur.type == E_EXIT) {
return true;
}

if (cur.btnstate == 0) text = "---";

if (cur.btnstate & BTN_UP) {
text += "UP ";
y = (y - 1) % SCREEN_HEIGHT;
}

if (cur.btnstate & BTN_DOWN) {
text += "DOWN ";
y = (y + 1) % SCREEN_HEIGHT;
}

if (cur.btnstate & BTN_LEFT) {
text += "LEFT ";
x = (x - 1) % SCREEN_WIDTH;
}

if (cur.btnstate & BTN_RIGHT) {
text += "RIGHT ";
x = (x + 1) % SCREEN_WIDTH;
}

if (cur.btnstate & BTN_ROLL) {
text += "ROLL ";
}

if (cur.btnstate & BTN_JUMP) {
text += "JUMP ";
}
if (cur.btnstate & BTN_LCLICK) {
text += "LCLICK ";
x = cur.x;
y = cur.y;
}

if (cur.btnstate & BTN_RCLICK) {
text += "RCLICK ";
}
if (cur.btnstate & BTN_MCLICK) {
SCREEN_SCALE = 1;
}

//Event VS State
//Event = change. State = current position. So, it's easy to determine
//whether a button is being held, not being held, being clicked OR being released
//with just a little logic. This looks for the frame of the downward click.
if ((cur.type & E_LCLICK) && (cur.btnstate & BTN_LCLICK)) {
}


if (cur.type & E_SCRUP) {
Debug("SCRUP");
scale += 0.1;
//set_scale(scale);
}
if (cur.type & E_SCRDN) {
Debug("SCRDN");
scale -= 0.1;
if (scale < 0.1) scale = 0.1;
//set_scale(scale);
}
*/
