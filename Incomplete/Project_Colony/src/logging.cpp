#include "../inc/all.h"
#include "../inc/game_engine.h"
#include "../inc/UI_Engine.h"

void Debugger::Debug_SubRoutine(const int line, const char * file, std::string message, DebugType type)
{
	std::string complete_message(file);
	
	switch (type)
	{
		case D_NULL:
			complete_message += ": " + std::to_string(line) + " Msg: " + message + "\n";
			break;
		case D_VIDEO:
			complete_message += ": " + std::to_string(line) + " VMsg: " + message + "\n";
			break;
		case D_AUDIO:
			complete_message += ": " + std::to_string(line) + " AMsg: " + message + "\n";
			break;
		case D_PHYSICS:
			complete_message += ": " + std::to_string(line) + " PMsg: " + message + "\n";
			break;
		case D_UI:
			complete_message += ": " + std::to_string(line) + " UIMsg: " + message + "\n";
			break;
		case D_INPUT:
			complete_message += ": " + std::to_string(line) + " IMsg: " + message + "\n";
			break;
		case D_DEBUG:
			complete_message += ": " + std::to_string(line) + " DEBUG: " + message + "\n";
			std::clog << complete_message;
			break; 
		case D_PARSE:
			complete_message += ": " + std::to_string(line) + " PARSE: " + message + "\n";
			break;
		default:
			complete_message += ": " + std::to_string(line) + " Msg: " + message + "\n";
			type = D_NULL; //Prevent issues with unknown values being sent.
			break;
	}

	logfile << complete_message;

	if (enabled_messages[type] == true)
	{
		std::clog << complete_message;
	}
	else
	{
		std::clog << "Message disabled?";
	}


	return;
}

void Debugger::Generate_Test()
{
	std::string input;
	std::cin >> input;
	if (input == "map")
	{
		Game_Engine.Generate_Map();
	}
	if (input == "water")
	{
		Game_Engine.Generate_Water_Test();
	}
	if (input == "water2")
	{
		Game_Engine.Generate_Water_Test2();
	}
}

void Debugger::UI_Settings()
{
	std::string input;
	std::cin >> input;
	if (input == "update")
	{
		UI_Manager.Debug_Physics_Update();
	}
}

void Debugger::Console_Output_Pressure()
{
	/* 
	Needs to be fixed for chunk structure and multiblock structure.
	Although pressure should still be straightforward from this perspective.
	*/
}

void Debugger::Console_Output_Type()
{
	int bound_x;
	int bound_z;
	std::cin >> bound_x;
	std::cin >> bound_z;

	Coordinate co;
	for (co.y = 0; co.y < Game_Engine.map.Y(); ++co.y)
	{
		for (co.z = 0; co.z <  Game_Engine.map.Z() && co.z < bound_z; ++co.z)
		{
			for (co.x = 0; co.x < Game_Engine.map.X() && co.x < bound_x; ++co.x)
			{
			std::cout << Game_Engine.map[co].type;
			std::cout << " ";
			}
		std::cout << "\n";
		}
	std::cout << "\n\n" << co.y << "\n\n";
	}

}

void Debugger::Output()
{
	std::string input;
	std::cin >> input;
	if (input == "wl")
	{
		Console_Output_Pressure();
	}
	if (input == "type")
	{
		Console_Output_Type();
	}
}

void Debugger::Move_Viewport()
{
	int input;
	std::cin >> input;
	ViewPort view;
	Coordinate location;
	location.x = location.z = 0;
	location.y = input;
	view.location = location;
	UI_Manager.viewport_list[0] = view;
}

void Debugger::Debug_Commands()
{
	std::string input;
	bool pause = false;

	std::cin >> input;
	do 
	{
		if (input == "pause")
		{
			pause = true;
		}
		if (input == "resume")
		{
			pause = false;
		}
		if (input == "out")
		{
			Output();
		}
		if (input == "gen")
		{
			Generate_Test();
		}
		if (input == "UI")
		{
			UI_Settings();
		}
		if (input == "move")
		{
			Move_Viewport();
		}
		if (input == "exit")
		{
			UI_Manager.debug_mode = false;
		}
	} while (pause);
	
}