#include "../inc/all.h"
#include "../inc/constants.h"
#include "../inc/screen.h"
#include "../inc/load.h"
#include "../inc/event.h"
#include "../inc/text.h"
#include "../inc/audio.h"

void Render_Test_Font(int x, int y)
{
	//Debug("Rendering test word");

	Font smallfont = Load_Font("smalloutline.png");

	Texture title = Render("UI Test\nv1.0.01", smallfont, 7);
	Texture subtitle = Render("Bottom right", smallfont, 6);
	Texture leftcenter = Render("Left Middle", smallfont, 7);
	Texture word = Render("#-('w')", smallfont, 8);

	Clear_Screen();

	Draw(title, 0, 0, HINT_TOP, 3);
	Draw(word, x, y, HINT_TOPLEFT, (int)(1 + std::min(x, y)*0.01));
	Draw(leftcenter, 0, 0, HINT_LEFT, 3);
	Draw(subtitle, 0, 0, HINT_VERT);
	Update_Screen();

	Free(smallfont);
	Free(title);
	Free(subtitle);
	Free(leftcenter);
	Free(word);
}

void AudioManager::Test_Music()
{	
	Play_Music(MUSIC_TEST);
}

void AudioManager::Test_Sound()
{	
	Play_Sound(SOUND_TEST);
}
