#include "..\inc\audio.h"
#include "..\inc\event.h"
#include "..\inc\UI_Engine.h"
#include "..\inc\game_engine.h"
#include "..\inc\Render.h"
#include "../inc/JobHandler.h"

AudioManager Audio_Manager;
EventHandler Event_Manager;
UIHandler UI_Manager;
GameHandler Game_Engine;
Debugger Debug_Manager;
RenderHandler Render_Manager;
JobHandler Job_Manager;