#include "../inc/all.h"
#include "../inc/UI_Engine.h"
#include "../inc/game_engine.h"
#include "../inc/event.h"

void UIHandler::Update()
{
	InputEvent current_event;
	while (Event_Manager.input_queue.size() > 0)
	{
		current_event = Event_Manager.input_queue.front();

		if (current_event.type == EVENT_KEY && current_event.state == KEY_PRESSED)
		{
			std::string message = "Keypress: " + std::to_string(current_event.key);
			Debug(message, D_INPUT);
			if (current_event.key == SDL_SCANCODE_COMMA)
			{
				if (Game_Engine.Bound_Check(0, viewlevel + 1, 0))
				{
					viewport_list[0].location.y++; // This is a smidge hackey, but alright for now.  It should change based on gamestate, Menues don't have viewlevels.
				}
			}
			if (current_event.key == SDL_SCANCODE_PERIOD)
			{
				if (Game_Engine.Bound_Check(0, viewlevel - 1, 0))
				{
					viewport_list[0].location.y--;
				}
			}
			if (current_event.key == 53)
			{
				std::cout << "debug commands:\n";
				debug_mode = !debug_mode;
			}
		}
		if (current_event.type == EVENT_EXIT)
		{
			quit = true; // Should probably be the last one checked as it is normally not the event.
		}


		if (debug_mode == true)
		{
			Debug_Manager.Debug_Commands();
		}
		Event_Manager.input_queue.pop();
	}
	if (running)
	{
		GameInstruction physics_instruction;
		physics_instruction.type = GAME_UPDATE_PHYSICS;
		game_instruction_queue.push(physics_instruction);
	}
}

bool UIHandler::Initialize()
{
	ViewPort default_viewport;
	default_viewport.location.x = default_viewport.location.y = default_viewport.location.z = 0;
	default_viewport.x_width = 800;
	default_viewport.y_width = 1;
	default_viewport.z_width = 800;

	viewport_list.push_back(default_viewport);

	return true;
}

void UIHandler::Debug_Physics_Update()
{
	running = true;
}

UIHandler::UIHandler()
{
	debug_mode = true;
	running = false;
	quit = false;
	viewlevel = 1;
}

bool UIHandler::Running()
{
	return running;
}
