#include "../inc/text.h"

//wrap words into an appropriately-shaped paragraph block for rendering
std::string Word_Wrap(std::string words, int line_length){
	std::string out = "";
	std::string line = "";
	std::string partial = "";

	if( words[words.length()-1] != '\n') words += '\n';

	for(auto ch : words){

		//if the line has been finished, put it on the pile
		if(line.length() == line_length){
			out += line;
			line = "";
		}

		//space - end partial, add space if room.
		if(ch == ' '){
			line += partial;
			partial = "";

			if(line.length() < line_length){
				line += " ";
			}
			else{
				out += line;
				line = "";
			}
		}

		//tab - end partial, fill to next 4-stop
		else if(ch == '\t'){
			line += partial;
			partial = "";

			if( (4 - (line.length() + 4) % 4) + line.length() < line_length){
				line += std::string(4 - (line.length() + 4) % 4,' ');
			}
			else{
				//end of line, wrap to the next and put the tab there
				line += std::string(line_length - line.length(), ' ');
				out += line;
				line = "    ";
			}
		}

		//newline - end partial, fill to end of the line
		else if(ch == '\n'){
			line += partial;
			partial = "";

			//end of line, wrap to the next and put the tab there
			if(line_length-line.length() > 0)	line += std::string(line_length - line.length(), ' ');
			out += line;
			line = "";

		}

		//other valid character - add to partial.
		else {
			if(partial.length() + line.length() < line_length){
				partial += ch;
			}
			else{
				if(partial.length() < line_length){
					line += std::string(line_length - line.length(), ' ');
					out += line;
					line = "";

					partial += ch;
				}
				//too long to fit on a single line
				else{
					line += partial;
					partial = "";
					out += line;
					line = "";
				}
			}
		}

		//Debug(line + " " + partial + std::to_string(line_length));
	}

	//trim trailing whitespace.
	while( out.back() == ' ') out.pop_back();

	//make sure to grab the last partial string.
	return out;
}

// create a new sprite of the specified string, using the provided font, wrapping at the specified line length.
Texture Render(std::string words, Font font, int line_length){

	//Debug(words);
	words = Word_Wrap(words,line_length);
	//Debug(words);

	int lines = (words.length() / line_length) + 1;

	int width = line_length * font.width;
	int height = lines * font.height;



	//new texture with transparency
	Texture out = New_Texture(width,height,1);

	//for all characters in the string, draw the character
	Rect glyph;
	Rect pos;

	glyph.x = 0;
	glyph.y = 0;
	glyph.w = font.width;
	glyph.h = font.height;

	pos.x = 0;
	pos.y = 0;
	pos.w = font.width;
	pos.h = font.height;



	//render to the texture
	Render_To(out);



	for(auto ch : words){
		//move the rects to the correct glyph
		glyph.x = (ch & 0xf) * font.width;
		glyph.y = (ch >> 4) * font.height;



		//draw the glyph to its place on the texture
		Draw(font.glyphs,glyph,pos);


		//move the placement rect
		pos.x += font.width;
		if(pos.x / font.width >= line_length){
			pos.x = 0;
			pos.y += font.height;
		}
	}

	//render future draws to the screen
	Render_To_Screen();

	return out;
}

void Free(Font garbage){
	Free(garbage.glyphs);
	return;
}
