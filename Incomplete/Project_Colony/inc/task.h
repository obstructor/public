#pragma once

enum TaskType
{
	TASK_NULL,
	TASK_EAT,
	TASK_DRINK,
	TASK_SLEEP,
	TASK_MINE,
	TASK_CUT_WOOD,
	TASK_FIND_TOOL, // This should be generic, but perhaps specific to what the tool is used for.
	TASK_END
};

class Task
{
public:
	TaskType type;
	Coordinate destination;
	int value;
	unsigned int task_ID;
	unsigned int claimant_ID;
};