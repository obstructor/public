#pragma once

enum GameInstructionType
{
	GAME_DESIGNATION,
	GAME_PLAYER_DIRECT,
	GAME_UPDATE_PHYSICS
};

class ViewPort
{
public:
	Coordinate location;
	int x_width,y_width,z_width;
	int type;
};

class GameInstruction
{
public:
	GameInstructionType type;
	int x, y, z, w, h;
};

class UIHandler
{
private:
	void Debug_Physics_Update();

public:
	UIHandler();

	bool debug_mode;
	bool quit;
	bool running;
	
	bool Running();
	int viewlevel;

	std::queue <GameInstruction> game_instruction_queue;
	std::vector <ViewPort> viewport_list;
	void Update();
	bool Initialize();
	friend Debugger;
};

extern UIHandler UI_Manager;
