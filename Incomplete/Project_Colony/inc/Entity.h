#pragma once
#include "constants.h"
#include "Colonist.h"

enum PrototypeType
{
	PROTO_AIR,
	PROTO_DIRT,
	PROTO_GRASS,
	PROTO_ROCK,
	PROTO_STONE,
	PROTO_BEDROCK,
	PROTO_CLAY,
	PROTO_LIMESTONE,
	PROTO_GRAVEL,
	PROTO_IRON_ORE,
	PROTO_COPPER_ORE,
	PROTO_GOLD_ORE,
	PROTO_COAL_ORE,
	PROTO_IRON_BAR,
	PROTO_COPPER_BAR,
	PROTO_GOLD_BAR,
	PROTO_COAL_WASHED,
	PROTO_WOOD_RAW,
	PROTO_WOOD_CUT,
	PROTO_WATER,
	PROTO_MAGMA,
	PROTO_OIL,
	PROTO_GLASS,
	PROTO_COLONIST,
	PROTO_PLAYER,
	PROTO_END
};

enum PrototypeFlag
{
	PROTO_FLAG_COLONIST,
	PROTO_FLAG_SOLID,
	PROTO_FLAG_LIQUID,
	PROTO_FLAG_GAS,
	PROTO_FLAG_WALKABLE,
	PROTO_FLAG_SWIMMABLE,
	PROTO_FLAG_TRANSPARENT,
	PROTO_FLAG_PILING,
	PROTO_FLAG_ORGANIC,
	PROTO_FLAG_EDIBLE,
	PROTO_FLAG_CONDUCTIVE,
	PROTO_FLAG_PERMEABLE,
	PROTO_FLAG_ORE,
	PROTO_FLAG_METAL,
	PROTO_FLAG_FLAMEABLE,
	PROTO_FLAG_MULTIBLOCK,
	PROTO_FLAG_END
};

enum EntityFlag
{
	ENTITY_HOT,
	ENTITY_COLD,
	ENTITY_EXTREME_TEMPERATURE,
	ENTITY_OILY,
	ENTITY_WET,
	ENTITY_ACTOR,
	ENTITY_OUTSIDE,
	ENTITY_INACTIVE,
	ENTITY_END
};

enum ThoughtType
{

	THOUGHT_END
};

enum ColonistTrait
{
	TRAIT_QUICK,
	TRAIT_END
};

class Entity
{
	std::bitset < ENTITY_END > flags;
	int value;
	int temperature; // Can be combined into value if needed as neither should ever be particularly large.
public:
	Entity();
	Entity(int);

	int Hp();
	int Temperature();
	bool Actor();
	bool Actor(bool);

	bool Flag(EntityFlag);
	bool Flag(PrototypeFlag);
	bool Flag(bool flag_value, EntityFlag flag);
	bool Flag(bool flag_value, PrototypeFlag flag);

	void Set_Type(int);
	void Set_Value(int);
	void Decrease_Value(int);
	void Increase_Value(int);
	void Set_Defaults();

	int type;
	int Sprite();

	void Entity_Constructor();
	void Entity_Constructor(int);

	friend Debugger;
	//unsigned int vertical_support_received; //Until we figure out physics this is wasted.
	// Pressure for water
};

enum ColonistSkill
{
	SKILL_MINING,
	SKILL_MASON,
	SKILL_WOODCUTTING,
	SKILL_WOODWORKING,
	SKILL_END
};

class Colonist : public Entity
{
public:
	unsigned int ID;
	std::queue <int> path;
	Entity inventory[8];
	Task current_task;

	//Traits
	unsigned int skills[SKILL_END];
	ColonistSkill highest_skill_list[SKILL_END];
	unsigned int speed;
};

class Container : public Entity
{
public:
	Entity inventory[8];
};

class MapEntity : public Entity
{
public:
	MapEntity();
	MapEntity(int);
	Entity entity_inside;
	Container * container_inside;
	Colonist * colonist_inside; //Possibly an ID to the array.  But a pointer is less error prone?
};

class Player : public Entity
{

};

class Prototype
{
public:
	std::string name;
	int type;
	unsigned int sprite_number;
	unsigned int hardness;
	unsigned int permeability;
	unsigned int conductivity;
	unsigned int viscosity;
	unsigned int density;
	std::bitset < PROTO_FLAG_END > proto_flags;
	std::bitset < ENTITY_END > entity_flags_defaults;
	int value_default;

	void Reset();
	void LoadFlag(std::string);
	void LoadValue(std::string, int);
	Entity Default_Entity();
	MapEntity Default_MapEntity();
};

class Actor
{
public:
	int type;
	int ID;  //Colonist ID needs to match the coordinate.
	Coordinate position;
};

class Chunk
{
public:
	MapEntity at(const Coordinate& coord);
	MapEntity at(const int&, const int&, const int&);
	MapEntity chunk[CHUNK_SIZE*CHUNK_SIZE*CHUNK_SIZE];
};

class Map
{
	

	MapEntity Null_Entity;

	std::vector<Chunk> internal_map_storage;
public:
	int map_chunk_xwidth;
	int map_chunk_ywidth;
	int map_chunk_zwidth;

	unsigned int size();
	void resize(unsigned int size);
	void resize(int,int,int);

	MapEntity Null();
	bool Bound_Check(const Coordinate &);
	bool Bound_Check(const int&,const int&,const int&);
	MapEntity& operator[](const Coordinate &);

	int Coord_to_Chunk(const Coordinate&);
	int Coord_to_Chunk(const int&, const int&, const int&);

	int Coord_to_Block(const Coordinate&);
	int Coord_to_Block(const int&, const int&, const int&);

	int X();
	int Y();
	int Z();
};