#pragma once
#include "Entity.h"
#include "FastNoise.h"

class MapGenerator
{
private:
	enum GenItemType
	{
		GENITEM_FILL,
		GENITEM_LAYER,
		GENITEM_POCKET,
		GENITEM_SURFACE,
		GENITEM_END,
	};
	class ParsedString
	{
	public:
		std::string parsed_string;
		std::string parsed_value;
		std::string parsed_value_bot;
		void Parse(std::string);
	};
	class GenItem
	{
	public:
		MapEntity default_entity;
		MapEntity default_entity_bot;
		MapEntity location;
		int layer_range_top;
		int layer_range_bot;
		int fill_range_top;
		int fill_range_bot;
		int pocket_range_top;
		int pocket_range_bot;
		int frequency;
		int size;
		bool vertical;
		bool fill;
		bool pocket;
		bool surface;
		bool location_all;
		bool location_bool;
		bool random_layer;
	};
	int seed;
	int split_layer_seed;
	int surface_seed;

	

	FastNoise noise_generator;

	std::fstream file_map_gen;
	std::vector<std::vector<GenItem>> generation_information;

	void Generate_Map(std::fstream &, Map&);
	void Generate_Fill(Map&);
	void Generate_Layers(Map&);
	void Generate_Pockets(Map&);
	MapEntity Generate_Pockets_Generate_MapEntity(GenItem&, const int& perlin_value);
	void Generate_Surface(Map&);
	void Generate_Edges(Map&);

	void Parse_Name(GenItem&);
	void Parse_Gen_Attributes(GenItem&);
	void Push_GenItem(GenItem&);

	void Parse_Split_Layers();
	GenItem GenItem_From_Split_Layers(GenItem,GenItem);
	MapEntity Lookup_Entity_By_Name(std::string);
public:
	Map Generate_Map(int xseed);
	Map Debug_Generate_Map(int type);

	MapGenerator();
};