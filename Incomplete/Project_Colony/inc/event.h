#pragma once

#include "all.h"

enum KeyState
{
	KEY_UNPRESSED,
	KEY_HELD,
	KEY_PRESSED,
	KEY_RELEASED,
	KEY_NULL
};

enum EventType
{
	EVENT_KEY,
	EVENT_MOUSE,
	EVENT_MOUSE_MOTION,
	EVENT_EXIT,
	EVENT_NONE
};

enum MouseButtonList
{
	MOUSE_LEFT,
	MOUSE_RIGHT,
	MOUSE_MIDDLE,
	MOUSE_END
};

struct InputEvent
{
	EventType type;
	KeyState state;
	int key;
	int x;
	int y;
};

class EventHandler
{
private:
	const Uint8 * SDLkeys; //Organized by ScanCode.
	KeyState * key_state;
	int number_of_keys;
	SDL_Event sdl_event;

	KeyState mouse_state[MOUSE_END]; // LEFT 0, RIGHT 1, MIDDLE 2

	MouseButtonList Get_Mouse_Key();
public:

	std::queue <InputEvent> input_queue;

	bool Initialize();
	void Clean();
	void Gather_Input();
};

extern EventHandler Event_Manager;



/*
struct InputEvent {
	uint64_t type;
	int btnstate;
	int x;
	int y;
};



struct Monitor {

	SDL_Event e;

	InputEvent last_event;
};

enum Button {
	BTN_NONE = 0,
	BTN_LCLICK = 1,
	BTN_RCLICK = 2,
	BTN_UP = 4,
	BTN_LEFT = 8,
	BTN_DOWN = 16,
	BTN_RIGHT = 32,
	BTN_JUMP = 64,
	BTN_ROLL = 128,
	BTN_PAUSE = 256,
	BTN_MCLICK = 512,
	BTN_SCRUP = 1024,
	BTN_SCRDN = 2048
};

enum EventType {
	E_NONE = 0,
	E_LCLICK = 1,
	E_RCLICK = 2,
	E_UP = 4,
	E_LEFT = 8,
	E_DOWN = 16,
	E_RIGHT = 32,
	E_JUMP = 64,
	E_ROLL = 128,
	E_PAUSE = 256,

	E_MOUSE = 512,
	E_MCLICK = 1024,
	E_SCRUP = 2048,
	E_SCRDN = 4096,
	
	
	E_EXIT = 8096
};

InputEvent Poll_Event(Monitor *);
InputEvent Gather_Input();

*/
