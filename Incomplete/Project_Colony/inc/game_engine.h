#pragma once
#include "../inc/all.h"
#include "Entity.h"
#include "physics_event.h"
#include "Colonist.h"
#include "MapGenerator.h"

class GameHandler
{
private:

	class Thought
	{
	public:
		ThoughtType type;
		// source?
		// destination?
	};

	std::queue <Actor> actor_queue;
	std::queue <PhysicsEvent> event_queue;

	void Physics_Update();
	void Physics_Push();

	void Player_Update(Actor&);
	void Colonist_Update(Actor&);
	void Block_Update(Actor&);
	void Fluid_Update(Actor&, MapEntity);
	void Solid_Update(Actor & current_actor, MapEntity current_entity);

	MapEntity Fetch_Entity(int&,int&,int&);
	MapEntity Fetch_Entity(Coordinate&);

	Entity Get_Default_Entity();
	
	friend class Debugger;
	// Debug and Test Functions
	void Generate_Map();
	void Generate_Water_Test();
	void Generate_Water_Test2();

	bool Parse_Prototypes();

	Map map;
public:
	Coordinate bounds;
	int map_chunk_ywidth;
	int map_chunk_zwidth;
	
	bool Bound_Check(const int&, const int&, const int&);
	bool Bound_Check(const Coordinate&);

	std::vector <Prototype> prototype_list;

	int Get_Sprite_Number(const Coordinate&);
	int Get_Sprite_Number(const int&, const int&, const int&);

	bool Initialize_Game_Engine();
	void Update();
};

extern GameHandler Game_Engine;



/*

PROTO_NULL,
PROTO_AIR,
PROTO_DIRT,
PROTO_GRASS,
PROTO_ROCK,

PROTO_STONE,
PROTO_BEDROCK,
PROTO_CLAY,
PROTO_LIMESTONE,
PROTO_GRAVEL,

PROTO_IRON_ORE,
PROTO_COPPER_ORE,
PROTO_GOLD_ORE,
PROTO_COAL_ORE,
PROTO_IRON_BAR,

PROTO_COPPER_BAR,
PROTO_GOLD_BAR,
PROTO_COAL_WASHED,
PROTO_WOOD_RAW,
PROTO_WOOD_CUT,

PROTO_WATER,
PROTO_MAGMA,
PROTO_OIL,
PROTO_GLASS,
PROTO_COLONIST,
PROTO_PLAYER,
PROTO_END

*/