// all.h - include all external libraries used, but not internal code.

#pragma once

#include <iostream>
#include <fstream>
#include <string>
#include <cmath>
#include <cstdlib>
#include <vector>
#include <cstdint>
#include <iterator>
#include <algorithm>
#include <string>
#include <queue>
#include <bitset>
#include <sstream>

#include "logging.h"
#include "sdl.h"


class Coordinate
{
public:
	Coordinate()
	{
		x = y = z = 0;
	}
	Coordinate(int X, int Y, int Z)
	{
		x = X; y = Y; z = Z;
	}
	int x, y, z;
};