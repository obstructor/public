#ifndef SCREEN_H
#define SCREEN_H

/* Screen.h - Abstraction from internal to literal drawing
 *
 * This file should contain the base wrapping functions from SDL.
 * Rewriting it (and dependencies) for another library should be enough to
 * remove the SDL2 dependency.
 *
 */

#include "all.h"
#include "constants.h"

bool Init_Video();

//Log current fps (moving average on the base timer)
int Frame_Limiter();

extern SDL_Window * game_window;
extern SDL_Renderer * game_renderer;

extern int SCREEN_WIDTH;
extern int SCREEN_HEIGHT;
extern int SCREEN_SCALE;

//Drawing placement hints: Coordinates inward from specified location
enum Draw_Hint {
  HINT_CENTER       = 0x00,
  HINT_LEFT         = 0x10,
  HINT_RIGHT        = 0x20,
  HINT_TOP          = 0x01,
  HINT_BOTTOM       = 0x02,
  HINT_TOPLEFT      = 0x11,
  HINT_TOPRIGHT     = 0x21,
  HINT_BOTTOMLEFT   = 0x12,
  HINT_BOTTOMRIGHT  = 0x22,

  HINT_HORI         = 0xf0,
  HINT_VERT         = 0x0f
};

typedef SDL_Texture* Texture;
typedef SDL_Rect Rect;

//wrapper functions around SDL2
void Clear_Screen();
void Update_Screen();
void Resize_Screen(int w, int h, int scale);
void Set_Scale(int scale);
void Draw(Texture, Rect, Rect, Draw_Hint=HINT_TOPLEFT);
void Draw(Texture, int, int, Draw_Hint=HINT_TOPLEFT, double=1);

Rect Texture_Rect(Texture tex);

//Render-to-texture functions. Call to change modes.
void Render_To(Texture);
void Render_To_Screen();

void Free(Texture garbage);

Texture New_Texture(int width, int height, bool transparent=0);

void Render_Engine(); //Placeholder

#endif
