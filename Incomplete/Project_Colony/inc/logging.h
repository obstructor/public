#pragma once

enum DebugType
{
	D_NULL,
	D_VIDEO,
	D_PHYSICS,
	D_UI,
	D_AUDIO,
	D_INPUT,
	D_DEBUG,
	D_PARSE,
	D_END
};

class Debugger
{
	int counter;
	std::ofstream logfile;
	void Generate_Test();

	void Console_Output_Pressure();
	void Console_Output_Type();
	void Move_Viewport();
	void UI_Settings();
	void Output();

public:

	bool enabled_messages[D_END];

	void Debug_Commands();
	bool Initialize_Debug();
	void Clean_Debug();
	void Debug_SubRoutine(const int, const char*, std::string message = "", DebugType type = D_NULL);
};

extern Debugger Debug_Manager;

/*
** Appends the current line and file to Debug() calls.
** Using the ellipsis to designate all debug calls
** and the preprocessor directives line and file to insert the file and line
** VA_ARGS is a list of all the arguments caught by the ellipsis.
*/
#define Debug(...) Debug_Manager.Debug_SubRoutine(__LINE__,__FILE__,__VA_ARGS__)