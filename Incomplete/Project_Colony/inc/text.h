#ifndef TEXT_H
#define TEXT_H

//Text rendering

#include "all.h"
#include "screen.h"

struct Font {
	Texture glyphs;
	int width;
	int height;
};

Texture Render(std::string, Font, int);
void Free(Font garbage);


#endif
