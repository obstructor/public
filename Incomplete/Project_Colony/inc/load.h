#ifndef LOAD
#define LOAD

#include "all.h"
#include "text.h"
#include "screen.h"
#include "audio.h"

//Load a png into a texture
Texture Load_Texture(std::string path);
Font Load_Font(std::string);

#endif
