#pragma once
#include "all.h"
#include "Entity.h"
#include "UI_Engine.h"

class SpriteSheet
{
	
	int sheet_width, sheet_height;
	bool valid;
public:
	int square_width, square_height;

	SDL_Texture * sheet;
	SDL_Rect Get_Sprite_Rect(int);

	SpriteSheet(char*);
	SpriteSheet(std::string);
	SpriteSheet();

	bool Load(char*);
	bool Load(std::string);

	bool Valid();
};

class RenderHandler
{
	void Render_Layer();
	void Render_Viewport(ViewPort view);
	void Render_Square(const int&, const SDL_Rect&);

	void Clear_Screen();
	void Update_Screen();
	int Frame_Limiter();

	SDL_Renderer* game_renderer;
	SDL_Window * game_window;

	SpriteSheet blocks;

	int screen_width, screen_height, screen_scale;

public:

	void Render();
	bool Initialize();

	friend SpriteSheet;
};

#define TITLE "Project Albuquerque"

extern RenderHandler Render_Manager;