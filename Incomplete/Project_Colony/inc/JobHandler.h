#pragma once
#include "all.h"

enum TaskType
{
	TASK_NULL,
	TASK_EAT,
	TASK_DRINK,
	TASK_SLEEP,
	TASK_MINE,
	TASK_CUT_WOOD,
	TASK_FIND_TOOL, // This should be generic, but perhaps specific to what the tool is used for.
	TASK_END
};

class Task
{
public:
	TaskType type;
	Coordinate destination;
	int value;
	unsigned int task_ID;
	unsigned int claimant_ID;
};

class JobHandler
{
private:
	std::queue <Task> task_queues[TASK_END];
	std::vector <bool> task_exists[TASK_END];
	std::queue <unsigned int> task_ID_available[TASK_END];

	Task null_task;

	unsigned int CreateID(TaskType);
public:
	bool Initialize();
	Task Request_Task(unsigned int ID, TaskType type);
	void Task_Finished(Task finished_task);
	void Create_Task(TaskType type, Coordinate location);
};

extern JobHandler Job_Manager;