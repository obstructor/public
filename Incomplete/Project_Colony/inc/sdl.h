//sdlinc.h - appropriate include statements for SDL2 per-platform
#ifdef __linux__

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_mixer.h>

#elif _WIN32

#include <SDL.h>
#include <SDL_image.h>
#include <SDL_mixer.h>

#else
#error Platform not identified. This error triggered in src/inc/sdl.h
#endif