#pragma once

enum PhysicsEventType
{
	PHYSICS_EVENT_PRESSURE,
	PHYSICS_EVENT_COLLAPSE,
	PHYSICS_EVENT_BLOCK_UPDATE,
	PHYSICS_EVENT_BLOCK_COMPRESS,
	PHYSICS_EVENT_OUTSIDE,
	PHYSICS_EVENT_INSIDE
};

class PhysicsEvent
{
public:
	PhysicsEventType type;
	Coordinate source;
	Coordinate destination;
	int value;
};