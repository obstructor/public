#ifndef AUDIO_H
#define AUDIO_H

#include "all.h"

typedef Mix_Chunk* Sound;
typedef Mix_Music* Music;

enum SoundID
{
	SOUND_NULL,
	SOUND_TEST,
	SOUND_END
};

enum MusicID
{
	MUSIC_NULL,
	MUSIC_TEST,
	MUSIC_END
};

class AudioManager
{
private:
	std::vector < Sound > loaded_sounds;
	std::vector < Music > loaded_music;

	MusicID current_music;

	void Free(Sound);
	void Free(Music);
	
	bool Load_Music(std::string);
	bool Load_Sound(std::string);

	bool Initialize_All_Music();
	bool Initialize_All_Sound();
public:
	void Test_Music();
	void Play_Sound(SoundID);
	void Play_Music(MusicID);
	void Stop_Music(int);
	void Test_Sound();
	bool Initialize();
};

extern AudioManager Audio_Manager;

//Play Music and sound

#endif
