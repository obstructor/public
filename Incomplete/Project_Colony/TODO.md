By 10/8/2016:

#Debug

Create a basic configurable debug system involving a type, ID and an optional message.
Configured from and INI file.

#Restructure

Implement code into the gameloop and separate code based on role.
Apply code style to unstyled code.

#Audio

Implement basic OpenAL audio, allowing the playing of sounds and soundtrack.

#Game Engine

Implement the entity object involving:
basic tagging system
typing

#Organization

Create and update code structure diagrams and data structure diagrams to reflect current design plan.

